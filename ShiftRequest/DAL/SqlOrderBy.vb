﻿Public Class SqlOrderBy

    Public Property Column As SqlColumn
    Public Property Priority As Integer
    Public Property Order As orders

    Public Enum orders
        Ascending
        Descending
    End Enum

    Public Sub New(ByVal column As [Enum], ByVal priority As Integer, Optional ByVal order As orders = orders.Ascending)
        Me.Column = New SqlColumn(column)
        Me.Priority = priority
        Me.Order = order
    End Sub

    Public Function GetSql() As String
        Dim sqlString As String = Me.Column.Identifier

        If Me.Order = orders.Ascending Then sqlString += " ASC" Else sqlString += " DESC"

        Return sqlString
    End Function

End Class
