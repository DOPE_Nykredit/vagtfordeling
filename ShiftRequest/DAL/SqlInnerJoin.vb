﻿Public Class SqlInnerJoin

#Region "Variables"

    Private _Table As ShiftRequestTableDefinition
    Private _Columns As SqlColumn()
    Private _TableJoin As SqlInnerJoin
    Private _JoinConditions As SqlJoinCondition()
    Private _WhereClauses As ISqlWhereClause()

#End Region

#Region "Properties"

    Friend ReadOnly Property ColumnsWithTableAliases As List(Of String)
        Get
            Dim returnList As New List(Of String)

            If _Columns Is Nothing OrElse _Columns.Count = 0 OrElse _Columns.ToList.TrueForAll(Function(c) c Is Nothing) Then
                Dim columns As List(Of String) = _Table.GetAllColumnsWithIdentifier
                For i = 0 To columns.Count - 1
                    columns(i) = columns(i).Insert(0, _Table.Identifier & ".")
                Next

                returnList.AddRange(columns)
            Else
                _Columns.ToList.ForEach(Sub(c) returnList.Add(_Table.Identifier & "." & c.NameWithIdentifier))
            End If

            If _TableJoin IsNot Nothing Then returnList.AddRange(_TableJoin.ColumnsWithTableAliases)

            Return returnList
        End Get
    End Property
    Friend ReadOnly Property WhereClausesWithTableAliases As List(Of String)
        Get
            Dim returnList As New List(Of String)

            If _WhereClauses IsNot Nothing Then _WhereClauses.ToList.ForEach(Sub(w) If w IsNot Nothing Then returnList.Add(_Table.Identifier & "." & w.GetSqlQueryClause))
            If _TableJoin IsNot Nothing Then returnList.AddRange(_TableJoin.WhereClausesWithTableAliases)

            Return returnList
        End Get
    End Property
    Friend ReadOnly Property InnerJoinStatement As String
        Get
            Dim returnString As String = ""

            returnString = "INNER JOIN " & _Table.Name & " " & _Table.Identifier & " ON "

            For Each j In _JoinConditions
                returnString += j.JoinConditionSql & " AND "
            Next

            returnString = returnString.Remove(returnString.LastIndexOf(" AND"))

            If _TableJoin IsNot Nothing Then returnString += " " & _TableJoin.InnerJoinStatement

            Return returnString
        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal columns As SqlColumn(), ByVal tableJoin As SqlInnerJoin, ByVal joinConditions As SqlJoinCondition(), ByVal whereClauses As ISqlWhereClause())
        If table Is Nothing Then Throw New ArgumentNullException("Table cannot be nothing.")
        If columns IsNot Nothing Then columns.ToList.ForEach(Sub(c) If c IsNot Nothing AndAlso c.TableType.Equals(table) = False Then Throw New ArgumentException("Column " & c.Name & ", in select statement, does not belong to table " & table.Name & "."))
        If joinConditions Is Nothing OrElse joinConditions.ToList.Count = 0 Then Throw New ArgumentException("Join conditions cannot be nothing or empty")
        If whereClauses IsNot Nothing Then whereClauses.ToList.ForEach(Sub(w) If w IsNot Nothing AndAlso table.GetType.Equals(w.Column.TableType) = False Then Throw New ArgumentException("Column " & w.Column.Name & ", in where clauses, does not belong to table " & table.Name & "."))

        _Table = table
        _Columns = columns
        _TableJoin = tableJoin
        _JoinConditions = joinConditions
        _WhereClauses = whereClauses
    End Sub

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal columns As SqlColumn(), ByVal joinCondition As SqlJoinCondition, ByVal whereClause As ISqlWhereClause)
        Me.New(table,
               columns,
               Nothing,
               joinconditions:=IIf(joinCondition Is Nothing, Nothing, New List(Of SqlJoinCondition)({joinCondition})),
               whereclauses:=IIf(whereClause Is Nothing, Nothing, New List(Of ISqlWhereClause)({whereClause})))
    End Sub

#End Region

End Class
