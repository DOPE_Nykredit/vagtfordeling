﻿Public Class SqlDelete
    Inherits PLSqlStatement

    Private _WhereClause As SqlColumnValuePair()

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal whereClauses As SqlColumnValuePair())
        _Table = table
        _WhereClause = whereClauses
    End Sub

    Public Overrides Function GetSql() As String
        Dim sql = "DELETE FROM " & _Table.Name

        If _WhereClause IsNot Nothing Then sql += " WHERE "

        _WhereClause.ToList.ForEach(Sub(w) sql += w.SQLColumnValuePairString & " AND ")

        sql = sql.Remove(sql.LastIndexOf(" AND "))

        Return sql
    End Function
End Class
