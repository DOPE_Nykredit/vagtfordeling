﻿Public Class SqlStringColumnValuePair
    Inherits SqlColumnValuePair

    Public Enum Comparisons
        Equal
        NotEqual
        WildCard
    End Enum

    Private _Comparison As Comparisons

    Public Overrides ReadOnly Property ColumnValue As String
        Get
            If _ColumnValue IsNot Nothing AndAlso Not String.Equals(_ColumnValue, "NULL", StringComparison.OrdinalIgnoreCase) Then
                If Me.Comparison = Comparisons.WildCard Then
                    Return "'%" & _ColumnValue & "%'"
                Else
                    Return "'" & _ColumnValue & "'"
                End If
            Else
                Return "NULL"
            End If
        End Get
    End Property
    Public Overrides ReadOnly Property SQLColumnValuePairString As String
        Get
            Dim sqlOperator As String

            Select Case _Comparison
                Case Comparisons.Equal
                    sqlOperator = " = "
                Case Comparisons.NotEqual
                    sqlOperator = " != "
                Case Comparisons.WildCard
                    sqlOperator = " LIKE "
                Case Else
                    Throw New NotImplementedException("Enum value " & _Comparison.ToString & " is not implemented")
            End Select

            Return _Column.ToString & sqlOperator & Me.ColumnValue
        End Get
    End Property

    Public Property Comparison As Comparisons
        Get
            Return _Comparison
        End Get
        Set(ByVal value As Comparisons)
            If [Enum].IsDefined(GetType(SqlStringColumnValuePair.Comparisons), value) Then
                If Not value.Equals(Comparisons.Equal) And Not value.Equals(Comparisons.NotEqual) Then
                    If _ColumnValue Is Nothing Then Throw New ArgumentException("Comparison must be Equal or NotEqual when column value is null")
                End If

                _Comparison = value
            Else
                Throw New ArgumentOutOfRangeException("Value is not defined in enum Comparisons")
            End If
        End Set
    End Property

    Public Sub New(ByVal column As [Enum], ByVal columnValue As String, Optional ByVal comparison As Comparisons = Comparisons.Equal)
        MyBase.New(New SqlColumn(column), columnValue)
        Me.Comparison = comparison
    End Sub
End Class
