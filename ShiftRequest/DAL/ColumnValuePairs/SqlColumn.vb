﻿Imports System.Reflection
Imports System.ComponentModel

Public Class SqlColumn

    Public Property Column As [Enum]

    Public ReadOnly Property Name As String
        Get
            Return Column.ToString
        End Get
    End Property
    Public ReadOnly Property Identifier As String
        Get
            Dim fi As FieldInfo = Column.GetType().GetField(Column.ToString())
            Dim aattr() As DescriptionAttribute = DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

            If aattr.Length > 0 Then
                Return aattr(0).Description
            Else
                Return Column.ToString()
            End If
        End Get
    End Property

    Public ReadOnly Property NameWithIdentifier
        Get
            Return Me.Name & " AS " & Me.Identifier
        End Get
    End Property

    Public ReadOnly Property TableType As System.Type
        Get
            Return Column.GetType.DeclaringType
        End Get
    End Property

    Public Sub New(ByVal column As [Enum])
        If Not column.GetType.DeclaringType.BaseType.Equals(GetType(ShiftRequestTableDefinition)) Then Throw New ArgumentException("Column " & column.ToString & " is not defined in a SolutionsTableDefinition")
        If Not String.Equals(column.GetType.Name, "Columns", StringComparison.CurrentCultureIgnoreCase) Then Throw New ArgumentException("Enum is not a column")
        Me.Column = column
    End Sub

    Public Overrides Function ToString() As String
        Return Column.ToString
    End Function

    Public Shared Function GetIdentifierOfColumn(ByVal column As [Enum]) As String
        If Not column.GetType.DeclaringType.BaseType.Equals(GetType(ShiftRequestTableDefinition)) Then Throw New ArgumentException("Column " & column.ToString & " is not defined in a SolutionsTableDefinition")
        If Not String.Equals(column.GetType.Name, "Columns", StringComparison.CurrentCultureIgnoreCase) Then Throw New ArgumentException("Enum is not a column")

        Dim fi As FieldInfo = column.GetType().GetField(column.ToString())
        Dim aattr() As DescriptionAttribute = DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If aattr.Length > 0 Then
            Return aattr(0).Description
        Else
            Return column.ToString()
        End If
    End Function
    Public Shared Function ConvertToListOfSqlColumn(ByVal columns As [Enum]()) As SqlColumn()
        Dim sqlColumns As New List(Of SqlColumn)

        For Each c In columns
            sqlColumns.Add(New SqlColumn(c))
        Next

        Return sqlColumns.ToArray
    End Function
End Class
