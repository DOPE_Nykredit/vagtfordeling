﻿Public MustInherit Class SqlColumnValuePair
    Implements ISqlWhereClause

    Protected _Column As SqlColumn
    Protected _ColumnValue As Object

    Public ReadOnly Property Column As SqlColumn Implements ISqlWhereClause.Column
        Get
            Return _Column
        End Get
    End Property
    Public MustOverride ReadOnly Property ColumnValue As String

    Public MustOverride ReadOnly Property SQLColumnValuePairString As String

    Protected Sub New(ByVal column As SqlColumn, ByVal columnValue As Object)
        _Column = column
        _ColumnValue = columnValue
    End Sub

    Private Function GetSqlQueryClause() As String Implements ISqlWhereClause.GetSqlQueryClause
        Return Me.SQLColumnValuePairString
    End Function

End Class
