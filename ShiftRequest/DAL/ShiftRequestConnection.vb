﻿Public Class ShiftRequestConnection

#Region "Variables"

    Private _OraConn As New System.Data.OracleClient.OracleConnection("Data Source=CTIR;User Id=KS_DRIFT;Password=KS_DRIFT")
    Private _cmd As OracleClient.OracleCommand

#End Region

#Region "Properties"

    ''' <summary>Gets the connectionstate of the underlying Oracle Connection.</summary>
    Public ReadOnly Property ConnectionState As ConnectionState
        Get
            Return _OraConn.State
        End Get
    End Property

    ''' <summary>Gets a boolean value indication if a transaction has been started.</summary>
    Public ReadOnly Property TransactionInProgress As Boolean
        Get
            Return IIf(_cmd.Transaction Is Nothing, False, True)
        End Get
    End Property

#End Region

#Region "Events"

    ''' <summary>Raised when a transaction on the underlying Oracle Connection is commited.</summary>
    Public Event TransactionCommitted()
    ''' <summary>Raised when a transaction on the underlying Oracle Connection is rolled back.</summary>
    Public Event TransactionRolledBack()

#End Region

#Region "Methods"

    Public Sub New()
        _OraConn.Open()
        _cmd = _OraConn.CreateCommand
    End Sub

    ''' <summary>Closes the underlying Oracle Connection.</summary>
    Public Sub CloseConnection()
        _OraConn.Close()
    End Sub

    'SQL execution methods
    ''' <summary>Executes an SQL string against database. Used for updates and deletes.</summary>
    ''' <param name="statement">Verified SQL update/delete statement.</param>
    ''' <param name="returnId">Determines if an id is to be returned from database.</param>
    Public Function ExecuteSQLStatement(ByVal statement As PLSqlStatement, Optional ByVal returnId As Boolean = False) As Integer
        If Not Me._OraConn.State = ConnectionState.Open Then _OraConn.Open()

        _cmd.CommandText = statement.GetSql

        Console.WriteLine(_cmd.CommandText)

        If returnId Then
            _cmd.CommandText += " returning ID into :newId"
            Dim newIdParam As New OracleClient.OracleParameter("newId", OracleClient.OracleType.Number) With {.Direction = ParameterDirection.Output}
            _cmd.Parameters.Add(newIdParam)

            _cmd.ExecuteNonQuery()

            Dim newId = CType(newIdParam.Value, Integer)

            _cmd.Parameters.Clear()

            Return newId
        Else
            Return _cmd.ExecuteNonQuery()
        End If
    End Function
    Public Function ExecuteSqlStatement(ByVal statement As String) As Integer
        If Not Me._OraConn.State = ConnectionState.Open Then _OraConn.Open()

        Console.WriteLine(statement)
        _cmd.CommandText = statement

        Return _cmd.ExecuteNonQuery
    End Function

    ''' <summary>Executes a COUNT() query against database and returns the count.</summary>
    ''' <param name="scalarQuery">Verified SQL Count query.</param>
    Public Function ExecuteScalarQuery(ByVal scalarQuery As SqlScalarQuery) As Integer
        If Not Me._OraConn.State = ConnectionState.Open Then Me._OraConn.Open()

        _cmd.CommandText = scalarQuery.GetSql

        Return CType(_cmd.ExecuteScalar(), Integer)
    End Function

    ''' <summary>Executes a query against the database and return a datareader to retrieve the values.</summary>
    ''' <param name="query">Verified SQL Query.</param>
    Public Function ExecuteQuery(ByVal query As SqlQuery) As OracleClient.OracleDataReader
        If Not _OraConn.State = ConnectionState.Open Then _OraConn.Open()

        _cmd.CommandText = query.GetSql

        Return _cmd.ExecuteReader
    End Function
    Public Function ExecuteQuery(ByVal query As String) As OracleClient.OracleDataReader
        If Not _OraConn.State = ConnectionState.Open Then _OraConn.Open()

        _cmd.CommandText = query

        Return _cmd.ExecuteReader
    End Function

    Public Sub CancelQuery()
        _cmd.Cancel()
    End Sub

    'Transaction methods
    ''' <summary>Begins a new transaction on the underlying Oracle Connection.</summary>
    ''' <param name="isolationLevel">The isolation level to be used for the transaction.</param>
    Public Sub BeginTransaction(ByVal isolationLevel As IsolationLevel)
        If _cmd.Transaction IsNot Nothing Then Throw New InvalidOperationException("A transaction is already started")

        _cmd.Transaction = _OraConn.BeginTransaction(isolationLevel)
    End Sub

    ''' <summary>Commits a transaction on the underlying Oracle Connection.</summary>
    Public Sub CommitTransaction()
        If _cmd.Transaction Is Nothing Then Throw New InvalidOperationException("A transaction has not been started")

        _cmd.Transaction.Commit()
        _cmd.Transaction = Nothing
        RaiseEvent TransactionCommitted()
    End Sub

    ''' <summary>Rolles back a transaction on the underlying Oracle Connection.</summary>
    Public Sub RollBackTransaction()
        If _cmd.Transaction Is Nothing Then Throw New InvalidOperationException("A transaction has not been started")

        _cmd.Transaction.Rollback()
        _cmd.Transaction = Nothing
        RaiseEvent TransactionRolledBack()
    End Sub

#End Region

End Class
