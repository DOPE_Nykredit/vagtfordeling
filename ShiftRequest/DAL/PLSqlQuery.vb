﻿Public MustInherit Class PLSqlQuery

#Region "Variables"

    Protected _Table As ShiftRequestTableDefinition
    Protected _Columns As [Enum]()
    Protected _WhereClauses As ISqlWhereClause()

#End Region

#Region "Methods"

    Public MustOverride Function GetSql() As String

#End Region

End Class
