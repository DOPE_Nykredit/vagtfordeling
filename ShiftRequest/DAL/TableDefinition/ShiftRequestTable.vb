﻿Imports System.Reflection
Imports System.ComponentModel

Public Class ShiftRequestTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("ShiftRequestId")> Id
        <Description("ShiftRequestAgentId")> Agent_Id
        <Description("ShiftRequestPrimStart")> Primary_Start
        <Description("ShiftRequestPrimEnd")> Primary_End
        <Description("ShiftRequestSecStart")> Secondary_Start
        <Description("ShiftRequestSecEnd")> Secondary_end
        <Description("ShiftRequestStdShiftId")> Standard_Shift_Id
        <Description("ShiftRequestDay")> Day
        <Description("ShiftRequestSubmitted")> Submitted
        <Description("ShiftRequestCampId")> CampaignId
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_SHIFT_REQUEST"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "srq" & Chr(34)
        End Get
    End Property

End Class