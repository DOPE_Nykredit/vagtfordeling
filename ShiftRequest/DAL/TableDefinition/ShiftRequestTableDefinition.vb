﻿Public MustInherit Class ShiftRequestTableDefinition
    Public MustOverride ReadOnly Property Name As String

    Public MustOverride ReadOnly Property Identifier As String

    Public Function GetAllColumnsWithIdentifier() As List(Of String)
        Dim columns As New List(Of String)
        Dim columnValues() As Integer

        columnValues = [Enum].GetValues(Type.GetType(Me.GetType.FullName & "+Columns"))

        For Each v In columnValues
            Dim column As New SqlColumn([Enum].ToObject(Type.GetType(Me.GetType.FullName & "+Columns"), v))
            columns.Add(column.NameWithIdentifier)
        Next

        Return columns
    End Function

End Class