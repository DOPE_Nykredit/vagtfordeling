﻿Imports System.Reflection
Imports System.ComponentModel

Public Class TeamTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("TeamId")> Id
        <Description("TeamCode")> Team
        <Description("TeamName")> Team_Navn
        <Description("TeamGroup")> Team_Gruppe
        <Description("TeamLeader")> Team_Leder
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "AGENTER_TEAMS"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "tea" & Chr(34)
        End Get
    End Property

End Class
