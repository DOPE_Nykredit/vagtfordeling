﻿Imports System.ComponentModel

Public Class TeamsInCampaignTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("TeamCampCampaignId")> CampaignId
        <Description("TeamCampTeamId")> Team_Id
    End Enum

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return "teca"
        End Get
    End Property

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_CAMPAIGN_TEAMS"
        End Get
    End Property
End Class
