﻿Imports System.Reflection
Imports System.ComponentModel

Public Class StandardShiftTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("StandardShiftId")> ID
        <Description("StandardShiftStart")> ShiftStart
        <Description("StandardShiftEnd")> ShiftEnd
        <Description("StandardShiftCampId")> CampaignId
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_STANDARD_SHIFT"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "stsh" & Chr(34)
        End Get
    End Property

End Class