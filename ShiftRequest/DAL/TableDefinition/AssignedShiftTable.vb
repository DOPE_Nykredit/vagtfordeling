﻿Imports System.Reflection
Imports System.ComponentModel

Public Class AssignedShiftTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("AssignShiftID")> Id
        <Description("AssignShiftDay")> Day
        <Description("AssignShiftStart")> ShiftStart
        <Description("AssignShiftEnd")> ShiftEnd
        <Description("AssignShiftAgentId")> Agent_Id
        <Description("AssignShiftCampId")> CampaignId
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_ASSIGNED_SHIFT"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "ass" & Chr(34)
        End Get
    End Property

End Class
