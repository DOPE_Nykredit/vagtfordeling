﻿Imports System.ComponentModel
Imports System.Reflection

Public Class ShiftRequirementsTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("RequirementDay")> Day
        <Description("RequirementStart")> IntervalStart
        <Description("RequirementAgents")> AgentsRequired
        <Description("RequirementCampId")> CampaignId
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_REQUIREMENTS"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "req" & Chr(34)
        End Get
    End Property

End Class
