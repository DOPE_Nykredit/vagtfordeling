﻿Imports System.Reflection
Imports System.ComponentModel

Public Class StandardShiftOnDayTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("StaShiftDayStaShiId")> StandardShiftID
        <Description("StaShiftDayDay")> Day
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_STANDARD_SHIFT_DAYS"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "ssd" & Chr(34)
        End Get
    End Property

End Class
