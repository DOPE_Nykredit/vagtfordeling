﻿Imports System.Reflection
Imports System.ComponentModel

Public Class AgentTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("AgentId")> Id
        <Description("AgentInitials")> Initialer
        <Description("AgentFirstName")> Fornavn
        <Description("AgentLastName")> Efternavn
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "AGENTER_LISTE"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "age" & Chr(34)
        End Get
    End Property

End Class
