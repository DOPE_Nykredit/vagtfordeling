﻿Imports System.ComponentModel
Imports System.Reflection

Public Class WorkdayDefinitionTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("WorkdayDefinitionDay")> Day
        <Description("WorkdayDefinitionStart")> DayStart
        <Description("WorkdayDefinitionEnd")> DayEnd
        <Description("WorkdayDefinitionCampId")> CampaignId
        <Description("WorkdayDefinitionReqHours")> HoursRequired
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_WORKDAY_DEFINITION"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "wdd" & Chr(34)
        End Get
    End Property

End Class
