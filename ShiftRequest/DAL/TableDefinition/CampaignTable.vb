﻿Imports System.ComponentModel

Public Class CampaignTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("CampaignId")> Id
        <Description("CampaignFromDate")> FromDate
        <Description("CampaignToDate")> ToDate
        <Description("CampaignName")> Name
        <Description("CampaignCreated")> Created
        <Description("CampaignScheduleStart")> ScheduleStart
        <Description("CampaignShiftSelectionType")> ShiftSelectionType
    End Enum

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return "camp"
        End Get
    End Property

    Public Overrides ReadOnly Property Name As String
        Get
            Return "HRSR_CAMPAIGNS"
        End Get
    End Property
End Class
