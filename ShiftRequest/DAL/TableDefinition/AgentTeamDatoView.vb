﻿Imports System.ComponentModel

Public Class AgentTeamDatoView
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("VTTeamDate")> Dato
        <Description("VTAgentId")> Agent_Id
        <Description("VtTeamId")> Team_Id
    End Enum

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) + "vt" + Chr(34)
        End Get
    End Property

    Public Overrides ReadOnly Property Name As String
        Get
            Return "V_TEAM_DATO"
        End Get
    End Property
End Class
