﻿Imports System.Reflection
Imports System.ComponentModel

Public Class AgentAllocationTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("AgentAllocInitials")> Initialer
        <Description("AgentAllocTeam")> Team
        <Description("AgentAllocStart")> Start
        <Description("AgentAllocEnd")> Slut
    End Enum

    Public Overrides ReadOnly Property Name As String
        Get
            Return "AGENTER_LISTE"
        End Get
    End Property

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return Chr(34) & "aga" & Chr(34)
        End Get
    End Property

End Class
