﻿Imports System.ComponentModel

Public Class CampignAdditionTable
    Inherits ShiftRequestTableDefinition

    Public Enum Columns
        <Description("CampAdditionId")> Id
        <Description("CampAdditionCampaignId")> Campaign_Id
        <Description("CampAdditionAgentId")> Agent_Id
    End Enum

    Public Overrides ReadOnly Property Identifier As String
        Get
            Return "cadd"
        End Get
    End Property

    Public Overrides ReadOnly Property Name As String
        Get
            Return Chr(34) + "HRSR_CAMPAIGN_ADDITION" + Chr(34)
        End Get
    End Property
End Class
