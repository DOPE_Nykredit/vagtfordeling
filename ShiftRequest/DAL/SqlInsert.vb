﻿Public Class SqlInsert
    Inherits PLSqlStatement

    Private _Inserts As SqlColumnValuePair()

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal insert As SqlColumnValuePair)
        Me.New(table, {insert})
    End Sub

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal inserts As SqlColumnValuePair())
        If table Is Nothing Then Throw New ArgumentNullException("Table cannot be nothing.")
        If inserts Is Nothing OrElse inserts.Count = 0 Then Throw New ArgumentException("Inserts cannot be nothing or empty.")

        _Table = table
        _Inserts = inserts
    End Sub

    Public Overrides Function GetSql() As String
        Dim sql As String = ""

        'Start of INSERT statement
        sql += "INSERT INTO " & _Table.Name & " ("

        'Add column names to statement
        For Each i In _Inserts
            sql += i.Column.ToString & ", "
        Next

        'Removes the last comma
        sql = sql.Remove(sql.Length - 2)

        'Add value initiater
        sql += ") VALUES ("

        'Add column values to statement
        For Each i In _Inserts
            sql += i.ColumnValue & ", "
        Next

        'Removes the last comma
        sql = sql.Remove(sql.Length - 2)

        'End value initiater
        sql += ")"

        'Return SQL statement
        Return sql
    End Function
End Class
