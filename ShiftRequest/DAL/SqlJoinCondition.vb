﻿Public Class SqlJoinCondition

    Public Property Table1ToJoin As ShiftRequestTableDefinition
    Public Property Table2ToJoin As ShiftRequestTableDefinition

    Public Property Columns1ToJoinOn As List(Of SqlColumn)
    Public Property Columns2ToJoinOn As List(Of SqlColumn)

    Public ReadOnly Property JoinConditionSql As String
        Get
            Dim sql As String = ""

            For i = 0 To Columns1ToJoinOn.Count - 1
                sql += Table1ToJoin.Identifier & "." & Columns1ToJoinOn(i).Name & " = " & Table2ToJoin.Identifier & "." & Columns2ToJoinOn(i).Name & " AND "
            Next

            sql = sql.Remove(sql.LastIndexOf(" AND "))

            Return sql
        End Get
    End Property

    Public Sub New(ByVal table1ToJoin As ShiftRequestTableDefinition, ByVal column1ToJoinOn As SqlColumn, ByVal table2ToJoin As ShiftRequestTableDefinition, ByVal column2ToJoinOn As SqlColumn)
        Me.new(table1ToJoin, New List(Of SqlColumn)({column1ToJoinOn}), table2ToJoin, New List(Of SqlColumn)({column2ToJoinOn}))
    End Sub

    Public Sub New(ByVal table1ToJoin As ShiftRequestTableDefinition, ByVal columns1ToJoinOn As List(Of SqlColumn), ByVal table2ToJoin As ShiftRequestTableDefinition, ByVal columns2ToJoinOn As List(Of SqlColumn))
        If columns1ToJoinOn Is Nothing OrElse columns1ToJoinOn.Count = 0 Then Throw New ArgumentException("Columns from table 1 to join on cannot be nothing or empty.")
        If columns2ToJoinOn Is Nothing OrElse columns2ToJoinOn.Count = 0 Then Throw New ArgumentException("Columns from table 2 to join on cannot be nothing or empty.")

        If columns1ToJoinOn.Count <> columns2ToJoinOn.Count Then Throw New ArgumentException("Columns from table 1 to join must containt the same number of element as Columns from table 2 to join on.")

        columns1ToJoinOn.ForEach(Sub(c) If Not table1ToJoin.GetType.Equals(c.TableType) Then Throw New ArgumentException("Column " & c.Column.ToString & " does not belong to table " & table1ToJoin.Name & "."))
        columns2ToJoinOn.ForEach(Sub(c) If Not table2ToJoin.GetType.Equals(c.TableType) Then Throw New ArgumentException("Column " & c.Column.ToString & " does not belong to table " & table2ToJoin.Name & "."))

        Me.Table1ToJoin = table1ToJoin
        Me.Table2ToJoin = table2ToJoin

        Me.Columns1ToJoinOn = columns1ToJoinOn
        Me.Columns2ToJoinOn = columns2ToJoinOn
    End Sub
End Class