﻿Public MustInherit Class PLSqlStatement

#Region "Variables"

    Protected _Table As ShiftRequestTableDefinition
    Protected _ApplyValues As List(Of SqlColumnValuePair)

#End Region

#Region "Methods"

    Public MustOverride Function GetSql() As String

#End Region


End Class
