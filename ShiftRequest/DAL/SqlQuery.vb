﻿Public Class SqlQuery
    Inherits PLSqlQuery

    Private _TableJoin As SqlInnerJoin

    Private _OrderBy As SqlOrderBy()

    Public Property InnerJoin As SqlInnerJoin
        Get
            Return _TableJoin
        End Get
        Set(ByVal value As SqlInnerJoin)
            _TableJoin = value
        End Set
    End Property

    Protected ReadOnly Property ColumnsWithTableAlies As String
        Get
            Dim returnString As String = ""

            Dim columns As New List(Of String)

            If _Columns Is Nothing OrElse _Columns.Count = 0 OrElse _Columns.ToList.TrueForAll(Function(c) c Is Nothing) Then
                columns.AddRange(_Table.GetAllColumnsWithIdentifier)
            Else
                Array.ForEach(_Columns, Sub(c) columns.Add(New SqlColumn(c).NameWithIdentifier))
            End If

            If _TableJoin IsNot Nothing Then
                For i = 0 To columns.Count - 1
                    columns(i) = columns(i).Insert(0, _Table.Identifier & ".")
                Next

                columns.AddRange(_TableJoin.ColumnsWithTableAliases)
            End If

            For Each c In columns
                returnString += c & ", "
            Next

            returnString = returnString.Remove(returnString.LastIndexOf(", "))

            Return returnString
        End Get
    End Property
    Protected ReadOnly Property FromStatement As String
        Get
            Dim returnString As String = "FROM " & _Table.Name

            If _TableJoin IsNot Nothing Then
                returnString += " " & _Table.Identifier

                returnString += " " & _TableJoin.InnerJoinStatement
            End If

            Return returnString
        End Get
    End Property
    Protected ReadOnly Property WhereStatement As String
        Get
            Dim clauses As New List(Of String)

            If _WhereClauses IsNot Nothing Then _WhereClauses.ToList.ForEach(Sub(w) clauses.Add(w.GetSqlQueryClause))

            If _TableJoin IsNot Nothing Then
                If _WhereClauses IsNot Nothing Then
                    For i = 0 To clauses.Count - 1
                        clauses(i) = clauses(i).Insert(0, _Table.Identifier & ".")
                    Next
                End If

                clauses.AddRange(_TableJoin.WhereClausesWithTableAliases)
            End If

            If clauses.Count = 0 Then
                Return ""
            Else
                Dim returnString As String = "WHERE "

                For Each c In clauses
                    returnString += c & " AND "
                Next

                returnString = returnString.Remove(returnString.LastIndexOf(" AND "))

                Return returnString
            End If

        End Get
    End Property
    Protected ReadOnly Property OrderBys As String
        Get
            Dim orderBy As String = ""

            If _OrderBy IsNot Nothing Then
                orderBy += "ORDER BY "

                _OrderBy.OrderBy(Function(o) o.Priority)
                _OrderBy.ToList.ForEach(Sub(o) orderBy += o.GetSql & ", ")

                orderBy = orderBy.Remove(orderBy.LastIndexOf(", "))
            End If

            Return orderBy
        End Get
    End Property

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal columns As [Enum](), ByVal whereClauses As ISqlWhereClause(), ByVal tableJoin As SqlInnerJoin, Optional ByVal orderBys As SqlOrderBy() = Nothing)
        If table Is Nothing Then Throw New ArgumentNullException("Table cannot be nothing")
        If columns IsNot Nothing Then columns.ToList.ForEach(Sub(c) If c IsNot Nothing AndAlso table.GetType.Equals(New SqlColumn(c).TableType) = False Then Throw New ArgumentException("Column " & New SqlColumn(c).Column.ToString & ", in select statement, does not belong to table " & table.Name & "."))
        If whereClauses IsNot Nothing Then whereClauses.ToList.ForEach(Sub(w) If w IsNot Nothing AndAlso table.GetType.Equals(w.Column.TableType) = False Then Throw New ArgumentException("Column " & w.Column.Name & ", in where clauses, does not belong to table " & table.Name & "."))
        If orderBys IsNot Nothing Then orderBys.ToList.ForEach(Sub(o) If o IsNot Nothing AndAlso table.GetType.Equals(o.Column.TableType) = False Then Throw New ArgumentException("Column " & o.Column.Name & ", in order by statement, does not belong to table " & table.Name & "."))

        _Table = table
        _Columns = columns
        _TableJoin = tableJoin
        _WhereClauses = whereClauses
        _OrderBy = orderBys
    End Sub
    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal columns As [Enum](), ByVal whereClauses As ISqlWhereClause(), Optional ByVal orderBys As SqlOrderBy() = Nothing)
        Me.New(table, columns, whereClauses, Nothing, orderBys)
    End Sub

    Public Overrides Function GetSql() As String
        Dim sql As String = ""

        sql += "SELECT "

        sql += Me.ColumnsWithTableAlies & " "

        sql += Me.FromStatement & " "

        sql += Me.WhereStatement & " "

        sql += Me.OrderBys

        Return sql
    End Function

End Class

