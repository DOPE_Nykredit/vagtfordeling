﻿Public Class SqlUpdate
    Inherits PLSqlStatement

    Private _Updates As SqlColumnValuePair()
    Private _WhereClauses As SqlColumnValuePair()

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal updates As SqlColumnValuePair(), ByVal whereClauses As SqlColumnValuePair())
        If table Is Nothing Then Throw New ArgumentNullException("Table cannot be nothing")
        If updates Is Nothing OrElse updates.Count = 0 Then Throw New ArgumentException("Missing updates")
        If whereClauses Is Nothing OrElse whereClauses.Count = 0 Then Throw New ArgumentException("Missing where clauses")

        _Table = table
        _Updates = updates
        _WhereClauses = whereClauses
    End Sub

    Public Overrides Function GetSql() As String
        Dim sql As String = ""

        sql += "UPDATE " & _Table.Name
        sql += " SET "

        For Each u In _Updates
            sql += u.SQLColumnValuePairString & ", "
        Next

        sql = sql.Remove(sql.Length - 2)

        sql += " WHERE "

        For Each w In _WhereClauses
            sql += w.SQLColumnValuePairString & " AND "
        Next

        sql = sql.Remove(sql.LastIndexOf(" AND "))

        Return sql
    End Function
End Class
