﻿Public Class SqlScalarQuery
    Inherits PLSqlQuery

    Public Enum ScalarQueryTypes
        Count
        Max
        Min
    End Enum

    Private _CalculationColumn As SqlColumn
    Private _ScalarType As ScalarQueryTypes

    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal calculationColumn As SqlColumn, ByVal scalarType As ScalarQueryTypes, ByVal whereClause As SqlColumnValuePair)
        Me.New(table, calculationColumn, scalarType, {whereClause})
    End Sub
    Public Sub New(ByVal table As ShiftRequestTableDefinition, ByVal calculationColumn As SqlColumn, ByVal scalarType As ScalarQueryTypes, ByVal whereClauses As ISqlWhereClause())
        _Table = table
        _CalculationColumn = calculationColumn
        _ScalarType = scalarType
        _WhereClauses = whereClauses
    End Sub

    Public Overrides Function GetSql() As String
        Dim sql As String = ""

        sql += "SELECT " & _ScalarType.ToString & "("

        If _CalculationColumn Is Nothing Then sql += "*" Else sql += _CalculationColumn.Name

        sql += ") FROM " & _Table.Name & " WHERE "

        For Each w In _WhereClauses
            sql += w.GetSqlQueryClause & " AND "
        Next

        sql = sql.Remove(sql.LastIndexOf(" AND "))

        Return sql
    End Function

End Class
