﻿Public Class SqlStringInClause
    Implements ISqlWhereClause

    Protected _Column As SqlColumn
    Protected _ColumnValues As List(Of String)

    Public ReadOnly Property Column As SqlColumn Implements ISqlWhereClause.Column
        Get
            Return _Column
        End Get
    End Property
    Public ReadOnly Property ColumnValues As IEnumerable(Of String)
        Get
            Return _ColumnValues
        End Get
    End Property

    Public Sub New(ByVal column As [Enum], ByVal columnValues As List(Of String))
        If Me.ColumnValues Is Nothing OrElse Me.ColumnValues.Count = 0 Then Throw New ArgumentException("ColumnValues cannot be nothing or empty")

        _Column = New SqlColumn(column)
        _ColumnValues = columnValues
    End Sub

    Public Function GetSqlInClause() As String Implements ISqlWhereClause.GetSqlQueryClause
        Dim inClause As String = ""

        inClause += Me.Column.Name & " ("

        For Each v In Me.ColumnValues
            inClause += "'" & v & "', "
        Next

        inClause.Remove(inClause.LastIndexOf(", "))

        inClause += ")"

        Return inClause
    End Function

End Class
