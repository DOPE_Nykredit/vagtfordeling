﻿Public Class SqlDateBetweenClause
    Implements ISqlWhereClause

    Protected _Column As SqlColumn
    Protected _StartDate As Date
    Protected _EndDate As Date

    Public ReadOnly Property Column As SqlColumn Implements ISqlWhereClause.Column
        Get
            Return _Column
        End Get
    End Property

    Public Sub New(ByVal column As [Enum], ByVal startDate As Date, ByVal endDate As Date)
        If startDate > endDate Then Throw New ArgumentException("StartDate cannot be greater then EndDate.")

        _Column = New SqlColumn(column)
        _StartDate = startDate
        _EndDate = endDate
    End Sub

    Public Function GetSqlQueryClause() As String Implements ISqlWhereClause.GetSqlQueryClause
        Dim betweenClause As String = ""

        betweenClause += _Column.Name & " BETWEEN " & ConvertToOracleDateTime(_StartDate) & " AND " & ConvertToOracleDateTime(_EndDate)

        Return betweenClause
    End Function

    Private Function ConvertToOracleDateTime(ByVal DateTimeToConvert As Date) As String
        Return "'" & DateTimeToConvert.Year & "-" & DateTimeToConvert.Month & "-" & DateTimeToConvert.Day & "'"
    End Function
End Class
