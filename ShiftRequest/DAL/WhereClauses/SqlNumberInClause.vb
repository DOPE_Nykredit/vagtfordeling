﻿Public Class SqlNumberInClause
    Implements ISqlWhereClause

    Protected _Column As SqlColumn
    Protected _ColumnValues As List(Of Integer)

    Public ReadOnly Property Column As SqlColumn Implements ISqlWhereClause.Column
        Get
            Return _Column
        End Get
    End Property
    Public ReadOnly Property ColumnValues As IEnumerable(Of Integer)
        Get
            Return _ColumnValues
        End Get
    End Property

    Public Sub New(ByVal column As [Enum], ByVal columnValues As List(Of Integer))
        If columnValues Is Nothing OrElse columnValues.Count = 0 Then Throw New ArgumentException("ColumnValues cannot be nothing or empty")

        _Column = New SqlColumn(column)
        _ColumnValues = columnValues
    End Sub

    Public Function GetSqlInClause() As String Implements ISqlWhereClause.GetSqlQueryClause
        Dim inClause As String = ""

        inClause += Me.Column.Name & " IN("

        For Each v In Me.ColumnValues
            inClause += v & ", "
        Next

        inClause = inClause.Remove(inClause.LastIndexOf(", "))

        inClause += ")"

        Return inClause
    End Function
End Class
