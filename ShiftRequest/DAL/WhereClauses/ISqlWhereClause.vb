﻿Public Interface ISqlWhereClause

    ReadOnly Property Column As SqlColumn

    Function GetSqlQueryClause() As String

End Interface
