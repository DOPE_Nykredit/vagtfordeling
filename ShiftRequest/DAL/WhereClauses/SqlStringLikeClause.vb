﻿Public Class SqlStringLikeClause
    Implements ISqlWhereClause

    Public Enum LikeComparisons
        StartsWith
        EndsWith
        Contains
    End Enum

    Private _Column As SqlColumn
    Private _ColumnValue As String

    Public ReadOnly Property Column As SqlColumn Implements ISqlWhereClause.Column
        Get
            Return _Column
        End Get
    End Property

    Public Property Comparison As LikeComparisons


    Public Sub New(ByVal column As [Enum], ByVal columnValue As String, Optional ByVal comparison As LikeComparisons = LikeComparisons.Contains)
        _Column = New SqlColumn(column)
        _ColumnValue = columnValue
        Me.Comparison = comparison
    End Sub

    Public Function GetSqlQueryClause() As String Implements ISqlWhereClause.GetSqlQueryClause
        Dim likeString As String = ""

        likeString += _Column.Name & " LIKE "

        If Me.Comparison = LikeComparisons.StartsWith Then
            likeString += "'" & _ColumnValue & "%'"
        ElseIf Me.Comparison = LikeComparisons.EndsWith Then
            likeString += "'%" & _ColumnValue & "'"
        ElseIf Me.Comparison = LikeComparisons.Contains Then
            likeString += "'%" & _ColumnValue & "%'"
        End If

        Return likeString
    End Function
End Class
