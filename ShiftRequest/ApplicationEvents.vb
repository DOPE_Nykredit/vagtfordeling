﻿Namespace My

    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.

    Partial Friend Class MyApplication

        Private _Agent As Agent

        Private _AgentsInSelectedCampaign As New List(Of Agent)
        Private _WorkDayDefinitionsInSelectedCampaign As New List(Of WorkDayDefinition)
        Private _StandardShiftsInSelectedCampaign As New List(Of StandardShift)
        Private _ShiftRequestsInSelectedCampaign As New List(Of RequestedShift)
        Private _AssignedShiftsInSelectedCampaign As New List(Of AssignedShift)

        Public Property SelectedCampaign As Campaign

        Dim tempId As Integer = 10000

        Public Function GetTempId() As Integer
            tempId += 1

            Return tempId
        End Function

        Public ReadOnly Property Agent As Agent
            Get
                Return _Agent
            End Get
        End Property

        Public ReadOnly Property AgentsInSelectedCampaign As List(Of Agent)
            Get
                Return _AgentsInSelectedCampaign
            End Get
        End Property
        Public ReadOnly Property WorkDayDefinitionsInSelectedCampaign As List(Of WorkDayDefinition)
            Get
                Return _WorkDayDefinitionsInSelectedCampaign
            End Get
        End Property
        Public ReadOnly Property WorkDayDefinitionsWithSplitInSelectedCampaign As List(Of WorkDayDefinition)
            Get
                Dim workDayDefinitionsWithSplit As List(Of WorkDayDefinition) = Me.WorkdayDefinitionsAll

                workDayDefinitionsWithSplit.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday)
                workDayDefinitionsWithSplit.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday)

                Return WorkDayDefinitionsWithSplit
            End Get
        End Property
        Public ReadOnly Property WorkdayDefinitionsAll As List(Of WorkDayDefinition)
            Get
                Dim allWorkDayDefinitions As New List(Of WorkDayDefinition)(_WorkDayDefinitionsInSelectedCampaign)

                If (allWorkDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday)) Then
                    Dim saturdayStart As ShiftTime = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday).DayStart
                    Dim saturdayEnd As ShiftTime = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday).DayEnd
                    Dim saturdayRequiredHours As Decimal = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday).RequiredHours
                    Dim saturdayShiftCountRequirements  = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday).ShiftCountRequirements

                    allWorkDayDefinitions.Add(New WorkDayDefinition(ShiftDay.Days.SaturdayEven,
                                                                    saturdayStart,
                                                                    saturdayEnd,
                                                                    saturdayRequiredHours) With {.ShiftCountRequirements = saturdayShiftCountRequirements})

                    allWorkDayDefinitions.Add(New WorkDayDefinition(ShiftDay.Days.SaturdayOdd,
                                                                    saturdayStart,
                                                                    saturdayEnd,
                                                                    saturdayRequiredHours) With {.ShiftCountRequirements = saturdayShiftCountRequirements})


                End If

                If (allWorkDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday)) Then
                    Dim sundayStart As ShiftTime = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday).DayStart
                    Dim sundayEnd As ShiftTime = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday).DayEnd
                    Dim sundayRequiredHours As Decimal = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday).RequiredHours
                    Dim sundayShiftCountRequirements = allWorkDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday).ShiftCountRequirements

                    allWorkDayDefinitions.Add(New WorkDayDefinition(ShiftDay.Days.SundayEven,
                                                                    sundayStart,
                                                                    sundayEnd,
                                                                    sundayRequiredHours) With {.ShiftCountRequirements = sundayShiftCountRequirements})

                    allWorkDayDefinitions.Add(New WorkDayDefinition(ShiftDay.Days.SundayOdd,
                                                                    sundayStart,
                                                                    sundayEnd,
                                                                    sundayRequiredHours) With {.ShiftCountRequirements = sundayShiftCountRequirements})
                End If

                Return allWorkDayDefinitions
            End Get
        End Property
        Public ReadOnly Property StandardShiftsInSelectedCampaign As List(Of StandardShift)
            Get
                Return _StandardShiftsInSelectedCampaign
            End Get
        End Property
        Public ReadOnly Property ShiftRequestsInSelectedCampaign As List(Of RequestedShift)
            Get
                Return _ShiftRequestsInSelectedCampaign
            End Get
        End Property
        Public ReadOnly Property AssignedShiftsInSelectedCampaign As List(Of AssignedShift)
            Get
                Return _AssignedShiftsInSelectedCampaign
            End Get
        End Property

        Public Sub LoadCampaignData(ByVal conn As ShiftRequestConnection)
            _AgentsInSelectedCampaign = Agent.GetAgentsInCampaign(conn, Me.SelectedCampaign)
            _WorkDayDefinitionsInSelectedCampaign = WorkDayDefinition.GetWorkDayDefinitionsInCampaign(conn, Me.SelectedCampaign)
            _StandardShiftsInSelectedCampaign = StandardShift.GetStandardShiftsInCampaign(conn, Me.SelectedCampaign)
            _ShiftRequestsInSelectedCampaign = RequestedShift.getRequestedShiftsInCampaign(conn, Me.SelectedCampaign)
            _AssignedShiftsInSelectedCampaign = AssignedShift.GetAssignedShiftsInCampaign(conn, Me.SelectedCampaign)
        End Sub

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            Dim conn As New ShiftRequestConnection

            _Agent = Agent.getAgentFromEnvironment(conn)

            If _Agent Is Nothing Then
                MsgBox("Du er ikke oprettet som medarbejder i Servicecenter. Kontakt HRØS hvis du er ansat i Servicecenter men stadig får denne besked", MsgBoxStyle.OkOnly, "Manglende oprettelse")
                e.Cancel = True
                Exit Sub
            ElseIf _Agent.Team.Group = "Stab" Then
                My.Application.MainForm = My.Forms.SelectCampaignForm
            Else
                Dim sqlCampaignCheck As String = "SELECT " +
                                                     "cam.* " +
                                                 "FROM " +
                                                     "KS_DRIFT.V_TEAM_DATO vt1 " +
                                                 "INNER JOIN " +
                                                     "(SELECT " +
                                                         "AGENT_ID, " +
                                                         "MAX(DATO) as ""DATO"" " + "" +
                                                     "FROM " +
                                                         "KS_DRIFT.V_TEAM_DATO " +
                                                     "GROUP BY " +
                                                         "AGENT_ID) vt2 " +
                                                 "ON " +
                                                     "vt1.AGENT_ID = vt2.AGENT_ID AND vt1.DATO = vt2.DATO " +
                                                 "INNER JOIN " +
                                                     "KS_DRIFT.HRSR_CAMPAIGN_TEAMS ct " +
                                                 "ON " +
                                                     "vt1.TEAM_ID = ct.TEAM_ID " +
                                                 "INNER JOIN " +
                                                     "KS_DRIFT.HRSR_CAMPAIGNS cam " +
                                                 "ON " +
                                                     "ct.CAMPAIGNID = cam.ID AND vt2.DATO > cam.SCHEDULESTART " +
                                                 "WHERE " +
                                                     "cam.TODATE >= trunc(sysdate) AND vt1.AGENT_ID = " & _Agent.Id

                Dim reader = conn.ExecuteQuery(sqlCampaignCheck)

                If reader.Read Then
                    Me.SelectedCampaign = New Campaign(reader("ID"),
                                                       reader("FROMDATE"),
                                                       reader("TODATE"),
                                                       reader("NAME"),
                                                       reader("SCHEDULESTART"),
                                                       reader("SHIFTSELECTIONTYPE"))

                    Dim AgentRequestCount = conn.ExecuteScalarQuery(New SqlScalarQuery(New ShiftRequestTable,
                                                                                       Nothing,
                                                                                       SqlScalarQuery.ScalarQueryTypes.Count,
                                                                                       {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.CampaignId, Me.SelectedCampaign.Id),
                                                                                        New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Agent_Id, _Agent.Id)}))

                    If AgentRequestCount > 0 Then
                        MsgBox("Du har allerede indgivet vagtønsker. Kontakt HRØS hvis du har ændringer til disse.", MsgBoxStyle.OkOnly, "Bemærk")
                        e.Cancel = True
                        Exit Sub
                    End If

                    My.Application.LoadCampaignData(conn)

                    My.Application.MainForm = My.Forms.SubmitShiftRequestForm
                Else
                    MsgBox("Det er ikke muligt for dit team at indgive vagtønsker i øjeblikket.", MsgBoxStyle.OkOnly, "Bemærk")
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        End Sub
    End Class

End Namespace

