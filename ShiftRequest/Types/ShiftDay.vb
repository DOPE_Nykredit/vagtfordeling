﻿Imports System.ComponentModel
Imports System.Reflection

Public Class ShiftDay
    Implements IComparable

    Public Enum Days
        <Description("Mandag")> Monday = 0
        <Description("Tirsdag")> Tuesday = 1
        <Description("Onsdag")> Wednesday = 2
        <Description("Torsdag")> Thursday = 3
        <Description("Fredag")> Friday = 4
        <Description("Lørdag")> Saturday = 5
        <Description("Lørdag - Lige uger")> SaturdayEven = 9
        <Description("Lørdag - Ulige uger")> SaturdayOdd = 10
        <Description("Søndag")> Sunday = 6
        <Description("Søndag - Lige uger")> SundayEven = 7
        <Description("Søndag - Ulige uger")> SundayOdd = 8
    End Enum

    Private _Day As ShiftDay.Days

    Public Property Day As ShiftDay.Days
        Get
            Return _Day
        End Get
        Set(ByVal value As ShiftDay.Days)
            If _Day <> value Then
                _Day = value
                Me.OnAssignedDayChanged(value)
            End If
        End Set
    End Property
    Public ReadOnly Property DayDescription As String
        Get
            Return ShiftDay.GetShiftDayDescription(_Day)
        End Get
    End Property

    Public Event AssignedDayChanged(ByVal sender As Object, ByVal day As ShiftDay.Days)

    Protected Sub OnAssignedDayChanged(ByVal day As ShiftDay.Days)
        RaiseEvent AssignedDayChanged(Me, day)
    End Sub

    Public Sub New(ByVal day As ShiftDay.Days)
        _Day = day
    End Sub

    Public Overrides Function ToString() As String
        Return Me.DayDescription
    End Function
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then Return False
        If Not obj.GetType.Equals(Me.GetType) Then Return False
        Return ((CType(obj, ShiftDay).Day = _Day))
    End Function

    Public Shared Operator =(ByVal shiftDay1 As ShiftDay, ByVal shiftDay2 As ShiftDay) As Boolean
        If shiftDay1 Is Nothing Or shiftDay2 Is Nothing Then Return False

        Return shiftDay1.Day = shiftDay2.Day
    End Operator
    Public Shared Operator <>(ByVal shiftDay1 As ShiftDay, ByVal shiftDay2 As ShiftDay) As Boolean
        If shiftDay1 Is Nothing Or shiftDay2 Is Nothing Then Return False

        Return shiftDay1.Day <> shiftDay2.Day
    End Operator

    Public Shared Function GetShiftDayDescription(ByVal day As Days) As String
        Dim fi As FieldInfo = day.GetType().GetField(day.ToString())
        Dim aattr() As DescriptionAttribute = DirectCast(fi.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If aattr.Length > 0 Then
            Return aattr(0).Description
        Else
            Return day.ToString()
        End If
    End Function

    Public Shared Function GetAllShiftDays(ByVal splitDays As Boolean) As List(Of ShiftDay)
        Dim allShiftDays As New List(Of ShiftDay)

        For Each d In [Enum].GetValues(GetType(ShiftDay.Days))
            If splitDays = True Or (d.ToString.EndsWith("Odd") = False Or d.ToString.EndsWith("Even") = False) Then allShiftDays.Add(New ShiftDay(d))
        Next

        Return allShiftDays
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If (TypeOf obj Is ShiftDay) = False Then Throw New ArgumentException("Cannot compare an shiftday to an object of type " & obj.GetType.ToString)

        Return Me.GetSortOrder(_Day).CompareTo(Me.GetSortOrder(CType(obj, ShiftDay).Day))
    End Function

    Private Function GetSortOrder(ByVal day As Days) As Integer
        If day = Days.Monday Then Return 0
        If day = Days.Tuesday Then Return 1
        If day = Days.Wednesday Then Return 2
        If day = Days.Thursday Then Return 3
        If day = Days.Friday Then Return 4
        If day = Days.Saturday Then Return 5
        If day = Days.SaturdayEven Then Return 6
        If day = Days.SaturdayOdd Then Return 7
        If day = Days.Sunday Then Return 8
        If day = Days.SundayEven Then Return 9 Else Return 10
    End Function
End Class
