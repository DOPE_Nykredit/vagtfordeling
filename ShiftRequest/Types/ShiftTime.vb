﻿Public Class ShiftTime
    Implements IComparable

    Const _MinimumShiftHour As Byte = 6
    Const _MaximumShiftHour As Byte = 22

    Private _hours As Byte
    Private _minutes As Byte

    Public Property Hours As Byte
        Get
            Return _hours
        End Get
        Private Set(ByVal value As Byte)
            If Me.RestrictToMaxInterval AndAlso (value < _MinimumShiftHour Or value > _MaximumShiftHour) Then Throw New ArgumentException("Hours must be between 6 and 22.")
            _hours = value
        End Set
    End Property
    Public Property Minutes As Byte
        Get
            Return _minutes
        End Get
        Private Set(ByVal value As Byte)
            _minutes = value

            If _minutes >= 60 Then
                Me.Hours += _minutes / 60
                _minutes = _minutes Mod 60
            End If
        End Set
    End Property

    Public Property RestrictToMaxInterval As Boolean

    Public Shared ReadOnly Property MinTime As ShiftTime
        Get
            Return New ShiftTime(_MinimumShiftHour, 0)
        End Get
    End Property
    Public Shared ReadOnly Property MaxTime As ShiftTime
        Get
            Return New ShiftTime(_MaximumShiftHour, 0)
        End Get
    End Property

    Public ReadOnly Property AsDecimal As Decimal
        Get
            Dim time = DateTime.Parse(_hours.ToString & ":" & _minutes.ToString)
            Return time.TimeOfDay.TotalHours
        End Get
    End Property

    Public Sub New(ByVal hours As Byte, ByVal minutes As Byte, Optional ByVal restrictToMaxInterval As Boolean = True)
        Me.RestrictToMaxInterval = restrictToMaxInterval
        Me.Hours = hours
        Me.Minutes = minutes
    End Sub
    Public Sub New(ByVal time As Decimal, Optional ByVal restrictToMaxInterval As Boolean = True)
        Me.RestrictToMaxInterval = restrictToMaxInterval
        Me.Hours = Decimal.Truncate(time)
        Me.Minutes = (time - Me.Hours) * 60
    End Sub

    Public Function GetShiftTimeFromOffset(ByVal hours As Byte, ByVal minutes As Byte) As ShiftTime
        Return New ShiftTime(_hours + hours, _minutes + minutes)
    End Function

    Public Shared Function GetAllShiftTimes() As List(Of ShiftTime)
        Dim allShiftTimes As New List(Of ShiftTime)

        allShiftTimes.Add(New ShiftTime(_MinimumShiftHour, 0))

        Do
            Dim lastShiftTime As ShiftTime = allShiftTimes.Last

            If lastShiftTime.Hours = _MaximumShiftHour Then Exit Do

            allShiftTimes.Add(lastShiftTime.GetShiftTimeFromOffset(0, 15))
        Loop

        Return allShiftTimes
    End Function

    Public Overrides Function ToString() As String
        Dim hours As String = Me.Hours
        If hours.Length = 1 Then hours = "0" & _hours.ToString

        Dim minutes As String = IIf(_minutes.ToString.Length = 1, "0" & _minutes.ToString, _minutes.ToString)

        Return hours & ":" & minutes
    End Function
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then Return False
        If Not Me.GetType.Equals(obj.GetType) Then Return False
        Return (CType(obj, ShiftTime).Hours = Me.Hours And CType(obj, ShiftTime).Minutes = Me.Minutes)
    End Function

    Public Shared Operator +(ByVal shifttime1 As ShiftTime, ByVal shifttime2 As ShiftTime) As ShiftTime
        Return New ShiftTime(shifttime1.AsDecimal + shifttime2.AsDecimal, shifttime1.RestrictToMaxInterval)
    End Operator
    Public Shared Operator -(ByVal shiftTime1 As ShiftTime, ByVal shifttime2 As ShiftTime) As ShiftTime
        Return New ShiftTime(shiftTime1.AsDecimal - shifttime2.AsDecimal, shiftTime1.RestrictToMaxInterval)
    End Operator
    Public Shared Operator =(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) = 0
    End Operator
    Public Shared Operator <>(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) <> 0
    End Operator

    Public Shared Operator <(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) < 0
    End Operator
    Public Shared Operator >(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) > 0
    End Operator

    Public Shared Operator <=(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) <= 0
    End Operator
    Public Shared Operator >=(ByVal shiftTime1 As ShiftTime, ByVal shiftTime2 As ShiftTime) As Boolean
        Return shiftTime1.CompareTo(shiftTime2) >= 0
    End Operator

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If obj Is Nothing Then Throw New ArgumentException("Cannot compare to Nothing.")
        If Not Me.GetType.Equals(obj.GetType) Then Throw New ArgumentException("Obj is not of type " & Me.GetType.ToString)

        If Me.Hours.CompareTo(CType(obj, ShiftTime).Hours) = 0 Then Return Me.Minutes.CompareTo(CType(obj, ShiftTime).Minutes) Else Return Me.Hours.CompareTo(CType(obj, ShiftTime).Hours)
    End Function
End Class