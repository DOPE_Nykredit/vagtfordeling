﻿Public Class ShiftTimeSpan

    Private _StartTime As ShiftTime
    Private _EndTime As ShiftTime

    Public ReadOnly Property StartTime As ShiftTime
        Get
            Return _StartTime
        End Get
    End Property
    Public ReadOnly Property EndTime As ShiftTime
        Get
            Return _EndTime
        End Get
    End Property

    Public ReadOnly Property Duration As ShiftTime
        Get
            Return New ShiftTime(Me.EndTime.AsDecimal - Me.StartTime.AsDecimal, False)
        End Get
    End Property

    Public Sub New(ByVal startTime As ShiftTime, ByVal endTime As ShiftTime)
        If startTime Is Nothing Then Throw New ArgumentNullException("Starttime cannot be Nothing.")
        If endTime Is Nothing Then Throw New ArgumentNullException("Endtime cannot be Nothing.")

        _StartTime = startTime
        _EndTime = endTime
    End Sub

    Public Shared Function GetAllShiftIntervals() As List(Of ShiftTimeSpan)
        Dim shiftIntervalStartTimes As List(Of ShiftTime) = ShiftTime.GetAllShiftTimes
        Dim shiftIntervals As New List(Of ShiftTimeSpan)

        For Each t In shiftIntervalStartTimes
            shiftIntervals.Add(New ShiftTimeSpan(t, t.GetShiftTimeFromOffset(0, My.Settings.ShiftInterval)))
        Next

        Return shiftIntervals
    End Function

    Public Function GetAllIntervalsInSpan() As List(Of ShiftTimeSpan)
        Dim returnList As New List(Of ShiftTimeSpan)

        For Each i In ShiftTimeSpan.GetAllShiftIntervals
            If Me.IsInInterval(i) Then returnList.Add(i)
        Next

        Return returnList
    End Function

    Public Function IsInInterval(ByVal shiftTime As ShiftTime) As Boolean
        If shiftTime Is Nothing Then Return False

        Return _StartTime <= shiftTime And _EndTime > shiftTime
    End Function
    Public Function IsInInterval(ByVal shiftSpan As ShiftTimeSpan) As Boolean
        If shiftSpan Is Nothing Then Return False

        Return _StartTime <= shiftSpan.StartTime And _EndTime >= shiftSpan.EndTime

        'Return (Me.IsInInterval(shiftSpan.StartTime) And Me.IsInInterval(shiftSpan.EndTime))
    End Function

    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then Return False
        If Not obj.GetType.Equals(Me.GetType) Then Return False
        Return (Me.StartTime.Equals(CType(obj, ShiftTimeSpan).StartTime) And Me.EndTime.Equals(CType(obj, ShiftTimeSpan).EndTime))
    End Function

    Public Overrides Function ToString() As String
        Return _StartTime.ToString & " - " & _EndTime.ToString
    End Function

End Class
