﻿Public Class WorkDayDefinition
    Implements IComparable

    Private _ShiftDay As ShiftDay

    Public ReadOnly Property ShiftDay As ShiftDay
        Get
            Return _ShiftDay
        End Get
    End Property

    Public Property DayStart As ShiftTime
    Public Property DayEnd As ShiftTime

    Public Property RequiredHours As Decimal

    Public ReadOnly Property DaySpan As ShiftTimeSpan
        Get
            If Me.DayStart Is Nothing Or Me.DayEnd Is Nothing Then
                Return Nothing
            Else
                Return New ShiftTimeSpan(Me.DayStart, Me.DayEnd)
            End If
        End Get
    End Property

    Public Property ShiftCountRequirements As New List(Of ShiftIntervalCountRequirement)

    Public Sub New(ByVal day As ShiftDay.Days, ByVal dayStart As ShiftTime, ByVal dayEnd As ShiftTime, ByVal requiredHours As Decimal)
        _ShiftDay = New ShiftDay(day)
        Me.DayStart = dayStart
        Me.DayEnd = dayEnd
        Me.RequiredHours = requiredHours
    End Sub

    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader) As WorkDayDefinition
        Dim day As ShiftDay.Days, dayStart As ShiftTime = Nothing, dayEnd As ShiftTime = Nothing, requiredHours As Decimal

        day = reader.Item(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.Day))

        If reader(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.DayStart)) > 0 Then dayStart = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.DayStart)))
        If reader(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.DayEnd)) > 0 Then dayEnd = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.DayEnd)))

        requiredHours = reader(SqlColumn.GetIdentifierOfColumn(WorkdayDefinitionTable.Columns.HoursRequired))

        Return New WorkDayDefinition(day, dayStart, dayEnd, requiredHours)
    End Function

    Public Sub Save(ByVal conn As ShiftRequestConnection)
        Try
            Dim updates As New List(Of SqlColumnValuePair)

            updates.Add(New SqlNumberColumnValuePair(WorkdayDefinitionTable.Columns.CampaignId, My.Application.SelectedCampaign.Id))

            If Me.DayStart Is Nothing Then updates.Add(New SqlDecimalColumnValuePair(WorkdayDefinitionTable.Columns.DayStart, 0)) Else updates.Add(New SqlDecimalColumnValuePair(WorkdayDefinitionTable.Columns.DayStart, Me.DayStart.AsDecimal))
            If Me.DayEnd Is Nothing Then updates.Add(New SqlDecimalColumnValuePair(WorkdayDefinitionTable.Columns.DayEnd, 0)) Else updates.Add(New SqlDecimalColumnValuePair(WorkdayDefinitionTable.Columns.DayEnd, Me.DayEnd.AsDecimal))

            updates.Add(New SqlDecimalColumnValuePair(WorkdayDefinitionTable.Columns.HoursRequired, Me.RequiredHours))

            Dim updateCount = conn.ExecuteSQLStatement(New SqlUpdate(New WorkdayDefinitionTable,
                                                                     updates.ToArray,
                                                                     {New SqlNumberColumnValuePair(WorkdayDefinitionTable.Columns.Day, Me.ShiftDay.Day),
                                                                      New SqlNumberColumnValuePair(WorkdayDefinitionTable.Columns.CampaignId, My.Application.SelectedCampaign.Id)}))

            If updateCount = 0 Then
                updates.Add(New SqlNumberColumnValuePair(WorkdayDefinitionTable.Columns.Day, Me.ShiftDay.Day))

                conn.ExecuteSQLStatement(New SqlInsert(New WorkdayDefinitionTable,
                                                       updates.ToArray))
            End If
        Catch ex As OracleClient.OracleException
            Throw

        End Try
    End Sub

    Public Class ShiftIntervalCountRequirement

        Public Property Day As DayOfWeek
        Public Property IntervalStart As ShiftTime
        Public ReadOnly Property IntervalEnd As ShiftTime
            Get
                Return Me.IntervalStart.GetShiftTimeFromOffset(0, My.Settings.ShiftInterval)
            End Get
        End Property

        Public ReadOnly Property IntervalSpan As ShiftTimeSpan
            Get
                Return New ShiftTimeSpan(Me.IntervalStart, Me.IntervalEnd)
            End Get
        End Property
        Public Property RequiredAgents As Integer

        Public Sub New(ByVal day As DayOfWeek, ByVal intervalStart As ShiftTime, ByVal requiredAgents As Integer)
            Me.Day = day
            Me.IntervalStart = intervalStart
            Me.RequiredAgents = requiredAgents
        End Sub

        Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader) As ShiftIntervalCountRequirement
            Dim day As ShiftDay.Days, intervalStart As Decimal, agentsRequired As Integer

            day = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequirementsTable.Columns.Day))
            intervalStart = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequirementsTable.Columns.IntervalStart))
            agentsRequired = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequirementsTable.Columns.AgentsRequired))

            Return New ShiftIntervalCountRequirement(day, New ShiftTime(intervalStart), agentsRequired)
        End Function
    End Class

    Public Shared Function GetWorkDayDefinitionsInCampaign(ByVal conn As ShiftRequestConnection,
                                                           ByVal campaign As Campaign,
                                                           Optional ByVal splitSundays As Boolean = False) As List(Of WorkDayDefinition)
        Dim workDayDefinitions As New List(Of WorkDayDefinition)

        Dim reader = conn.ExecuteQuery(New SqlQuery(New WorkdayDefinitionTable,
                                                    Nothing,
                                                    {New SqlNumberColumnValuePair(WorkdayDefinitionTable.Columns.CampaignId, campaign.Id)},
                                                    {New SqlOrderBy(WorkdayDefinitionTable.Columns.Day, 1)}))

        While reader.Read
            workDayDefinitions.Add(WorkDayDefinition.InstantiateFromDB(reader))
        End While

        reader = conn.ExecuteQuery(New SqlQuery(New ShiftRequirementsTable,
                                                Nothing,
                                                {New SqlNumberColumnValuePair(ShiftRequirementsTable.Columns.CampaignId, campaign.Id)}))

        While reader.Read
            Dim intervalReq = WorkDayDefinition.ShiftIntervalCountRequirement.InstantiateFromDB(reader)

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = intervalReq.Day) Then workDayDefinitions.First(Function(d) d.ShiftDay.Day = intervalReq.Day).ShiftCountRequirements.Add(intervalReq)
        End While

        If splitSundays AndAlso workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftRequest.ShiftDay.Days.Sunday) Then
            Dim sundayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftRequest.ShiftDay.Days.Sunday)

            workDayDefinitions.Add(New WorkDayDefinition(ShiftRequest.ShiftDay.Days.SundayEven,
                                                         sundayDefinition.DayStart,
                                                         sundayDefinition.DayEnd,
                                                         sundayDefinition.RequiredHours) With {.ShiftCountRequirements = sundayDefinition.ShiftCountRequirements})

            workDayDefinitions.Add(New WorkDayDefinition(ShiftRequest.ShiftDay.Days.SundayOdd,
                                                         sundayDefinition.DayStart,
                                                         sundayDefinition.DayEnd,
                                                         sundayDefinition.RequiredHours) With {.ShiftCountRequirements = sundayDefinition.ShiftCountRequirements})

            workDayDefinitions.RemoveAll(Function(d) d.ShiftDay.Day = ShiftRequest.ShiftDay.Days.Sunday)
        End If

        Return workDayDefinitions
    End Function

    Public Shared Sub DeleteWorkDayDefinitionInSelectedCampaign(ByVal conn As ShiftRequestConnection, ByVal day As ShiftDay.Days)
        Dim dayNumber As Integer = day

        Dim sql As String = "DELETE FROM " +
                                "KS_DRIFT.HRSR_WORKDAY_DEFINITION " +
                            "WHERE " +
                                "CAMPAIGNID = " + My.Application.SelectedCampaign.Id.ToString + " AND " +
                                "DAY = " + dayNumber.ToString

        conn.ExecuteSQLStatement(Sql)
    End Sub

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If (TypeOf obj Is WorkDayDefinition) = False Then Throw New ArgumentException("Cannot compare a workdaydefinition to an object of type " & obj.GetType.ToString)

        Return _ShiftDay.CompareTo(CType(obj, WorkDayDefinition).ShiftDay)
    End Function
End Class

