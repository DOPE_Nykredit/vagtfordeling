﻿Public Class RequestedShift
    Implements IComparable

    Private _Id As Integer

    Private _AgentId As Integer
    Private _Day As ShiftDay

    Private _StandardShift As StandardShift

    Private _PrimaryRequest As ShiftTimeSpan
    Private _SecondaryRequest As ShiftTimeSpan

    Private _Submitted As Date?

    Public ReadOnly Property Id As Integer
        Get
            Return _Id
        End Get
    End Property

    Public ReadOnly Property AgentId As Integer
        Get
            Return _AgentId
        End Get
    End Property
    Public ReadOnly Property Day As ShiftDay
        Get
            Return _Day
        End Get
    End Property

    Public Property StandardShift As StandardShift
        Get
            Return _StandardShift
        End Get
        Set(ByVal value As StandardShift)
            _StandardShift = value
            _PrimaryRequest = Nothing
        End Set
    End Property

    Public Property PrimaryRequest As ShiftTimeSpan
        Get
            If _StandardShift Is Nothing Then
                Return _PrimaryRequest
            Else
                Return _StandardShift.TimeSpan
            End If
        End Get
        Set(ByVal value As ShiftTimeSpan)
            _PrimaryRequest = value
            _StandardShift = Nothing
        End Set
    End Property
    Public Property SecondaryRequest As ShiftTimeSpan
        Get
            Return _SecondaryRequest
        End Get
        Set(ByVal value As ShiftTimeSpan)
            _SecondaryRequest = value
        End Set
    End Property

    Public ReadOnly Property Submitted As Date?
        Get
            Return _Submitted
        End Get
    End Property

    Public ReadOnly Property MaxTimeSpan As ShiftTimeSpan
        Get
            Dim minTime As ShiftTime = Nothing
            Dim maxTime As ShiftTime = Nothing

            If Me.PrimaryRequest IsNot Nothing Then
                minTime = Me.PrimaryRequest.StartTime
                maxTime = Me.PrimaryRequest.EndTime
            End If

            If _SecondaryRequest IsNot Nothing Then
                minTime = _SecondaryRequest.StartTime
                maxTime = _SecondaryRequest.EndTime
            End If

            Return New ShiftTimeSpan(minTime, maxTime)
        End Get
    End Property

    Public Event Saved()
    Public Event Deleted()

    Public Sub New(ByVal primary As ShiftTimeSpan, ByVal secondary As ShiftTimeSpan, ByVal agentId As Integer, ByVal day As ShiftDay)
        _PrimaryRequest = primary
        _SecondaryRequest = secondary
        _Day = day
        _AgentId = agentId
    End Sub
    Protected Sub New(ByVal id As Integer, ByVal primary As ShiftTimeSpan, ByVal secondary As ShiftTimeSpan, ByVal agentId As Integer, ByVal day As ShiftDay, ByVal submitted As Date)
        Me.New(primary, secondary, agentId, day)
        _Submitted = submitted
        _Id = id
    End Sub
    Public Sub New(ByVal shift As StandardShift, ByVal secondary As ShiftTimeSpan, ByVal agentId As Integer, ByVal day As ShiftDay)
        _StandardShift = shift
        _SecondaryRequest = secondary
        _AgentId = agentId
        _Day = day
    End Sub
    Public Sub New(ByVal id As Integer, ByVal shift As StandardShift, ByVal secondary As ShiftTimeSpan, ByVal agentId As Integer, ByVal day As ShiftDay, ByVal submitted As Date)
        Me.New(shift, secondary, agentId, day)
        _Submitted = submitted
        _Id = id
    End Sub

    Public Sub save(ByVal conn As ShiftRequestConnection)
        If _Id = 0 Then
            Dim checkQuery As New SqlScalarQuery(New ShiftRequestTable,
                                                 New SqlColumn(ShiftRequestTable.Columns.Agent_Id),
                                                 SqlScalarQuery.ScalarQueryTypes.Count,
                                                 {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.CampaignId, My.Application.SelectedCampaign.Id),
                                                  New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Day, _Day.Day),
                                                  New SqlStringColumnValuePair(ShiftRequestTable.Columns.Agent_Id, _AgentId)})

            Dim shiftRequestCountOnDay = conn.ExecuteScalarQuery(checkQuery)

            If shiftRequestCountOnDay > 0 Then
                MsgBox("Der findes allerede et vagtønske for " & ShiftDay.GetShiftDayDescription(Me.Day.Day) & ". Dette skal slettes før et nyt kan oprettes", MsgBoxStyle.OkOnly, "Bemærk")
            End If

            Dim inserts As New List(Of SqlColumnValuePair)

            If Me.PrimaryRequest IsNot Nothing Then
                If _StandardShift IsNot Nothing Then inserts.Add(New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Standard_Shift_Id, _StandardShift.ID))

                inserts.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_Start, Me.PrimaryRequest.StartTime.AsDecimal))
                inserts.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_End, Me.PrimaryRequest.EndTime.AsDecimal))
            End If

            If _SecondaryRequest IsNot Nothing Then
                inserts.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_Start, _SecondaryRequest.StartTime.AsDecimal))
                inserts.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_end, _SecondaryRequest.EndTime.AsDecimal))
            End If

            inserts.Add(New SqlStringColumnValuePair(ShiftRequestTable.Columns.Agent_Id, _AgentId))
            inserts.Add(New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Day, _Day.Day))
            inserts.Add(New SqlDateColumnValuePair(ShiftRequestTable.Columns.Submitted, If(_Submitted Is Nothing, Now, _Submitted)))
            inserts.Add(New SqlNumberColumnValuePair(ShiftRequestTable.Columns.CampaignId, My.Application.SelectedCampaign.Id))

            Dim insert As New SqlInsert(New ShiftRequestTable, inserts.ToArray)

            _Id = conn.ExecuteSQLStatement(insert, True)

            If _Submitted Is Nothing Then _Submitted = Now
        Else
            Dim updates As New List(Of SqlColumnValuePair)

            If Me.PrimaryRequest IsNot Nothing Then
                If _StandardShift IsNot Nothing Then updates.Add(New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Standard_Shift_Id, _StandardShift.ID))

                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_Start, Me.PrimaryRequest.StartTime.AsDecimal))
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_End, Me.PrimaryRequest.EndTime.AsDecimal))
            Else
                updates.Add(New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Standard_Shift_Id, Nothing))

                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_Start, Nothing))
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Primary_End, Nothing))
            End If

            If _SecondaryRequest IsNot Nothing Then
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_Start, _SecondaryRequest.StartTime.AsDecimal))
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_end, _SecondaryRequest.EndTime.AsDecimal))
            Else
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_Start, Nothing))
                updates.Add(New SqlDecimalColumnValuePair(ShiftRequestTable.Columns.Secondary_end, Nothing))
            End If

            conn.ExecuteSQLStatement(New SqlUpdate(New ShiftRequestTable,
                                                   updates.ToArray,
                                                   {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Id, _Id)}))
        End If

        RaiseEvent Saved()
    End Sub
    Public Sub Delete(ByVal conn As ShiftRequestConnection)
        If _Id > 0 Then
            Dim delete As New SqlDelete(New ShiftRequestTable,
                                        {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.Id, _Id)})

            conn.ExecuteSQLStatement(delete)
        End If

        RaiseEvent Deleted()
    End Sub

    Public Function FlexibilityEqualsShift() As Boolean
        If (_StandardShift Is Nothing) <> (_SecondaryRequest Is Nothing) Then
            Return False
        Else
            If _StandardShift IsNot Nothing Then
                Return (_StandardShift.TimeStart.Equals(_SecondaryRequest.StartTime) And _StandardShift.TimeEnd.Equals(_SecondaryRequest.EndTime))
            Else
                Return False
            End If
        End If
    End Function

    Public Shared Function getRequestedShiftsInCampaign(ByVal conn As ShiftRequestConnection, ByVal campaign As Campaign) As List(Of RequestedShift)
        Dim requestedShifts As New List(Of RequestedShift)

        Dim agentIdsInCampaign = (From a In My.Application.AgentsInSelectedCampaign Select a.Id).ToList

        Dim reader = conn.ExecuteQuery(New SqlQuery(New ShiftRequestTable,
                                                    Nothing,
                                                    {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.CampaignId, campaign.Id),
                                                     New SqlNumberInClause(ShiftRequestTable.Columns.Agent_Id, agentIdsInCampaign)}))

        While reader.Read
            requestedShifts.Add(RequestedShift.InstantiateFromDB(reader))
        End While

        requestedShifts.Sort()

        Return requestedShifts
    End Function
    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader) As RequestedShift
        Dim id As Integer, primaryStart As ShiftTime = Nothing, primaryEnd As ShiftTime = Nothing, shiftId As Integer?, secondaryStart As ShiftTime = Nothing, secondaryEnd As ShiftTime = Nothing, agentId As Integer, day As Integer, submitted As Date

        Dim nullCheck As Object

        id = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Id))

        nullCheck = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Standard_Shift_Id))
        If Not IsDBNull(nullCheck) Then
            shiftId = Convert.ToInt32(nullCheck)

            primaryStart = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Primary_Start)))
            primaryEnd = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Primary_End)))
        Else
            nullCheck = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Primary_Start))

            If Not IsDBNull(nullCheck) Then
                primaryStart = New ShiftTime(nullCheck)
                primaryEnd = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Primary_End)))
            End If
        End If

        nullCheck = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Secondary_Start))
        If Not IsDBNull(nullCheck) Then
            secondaryStart = New ShiftTime(nullCheck)
            secondaryEnd = New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Secondary_end)))
        End If

        day = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Day))
        agentId = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Agent_Id))
        submitted = reader(SqlColumn.GetIdentifierOfColumn(ShiftRequestTable.Columns.Submitted))

        Dim primarySpan As ShiftTimeSpan = Nothing
        Dim secondarySpan As ShiftTimeSpan = Nothing

        If primaryStart IsNot Nothing Then primarySpan = New ShiftTimeSpan(primaryStart,
                                                                           primaryEnd)

        If secondaryStart IsNot Nothing Then secondarySpan = New ShiftTimeSpan(secondaryStart,
                                                                               secondaryEnd)

        If shiftId IsNot Nothing Then
            Return New RequestedShift(id,
                                      New StandardShift(shiftId, primarySpan),
                                      secondarySpan,
                                      agentId,
                                      New ShiftDay(day),
                                      submitted)
        Else
            Return New RequestedShift(id,
                                      primarySpan,
                                      secondarySpan,
                                      agentId,
                                      New ShiftDay(day),
                                      submitted)
        End If
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If obj Is Nothing Then Return 0
        If Not TypeOf obj Is RequestedShift Then Throw New ArgumentException("Cannot compare to object of type " & obj.GetType.ToString & ".")

        Dim shiftRequestToCompare = CType(obj, RequestedShift)

        If _Day.CompareTo(shiftRequestToCompare.Day) = 0 Then
            Return Me.MaxTimeSpan.StartTime.CompareTo(shiftRequestToCompare.MaxTimeSpan.StartTime)
        Else
            Return _Day.CompareTo(shiftRequestToCompare.Day)
        End If
    End Function
End Class
