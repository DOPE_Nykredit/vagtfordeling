﻿Public Class Campaign

    Public Enum ShiftSelectionType
        StandardShifts
        AgentDefined
    End Enum

    Private _Id As Integer

    Private _FromDate As Date
    Private _ToDate As Date

    Private _SelectionType As ShiftSelectionType

    Public ReadOnly Property Id As Integer
        Get
            Return _Id
        End Get
    End Property

    Public ReadOnly Property FromDate As Date
        Get
            Return _FromDate
        End Get
    End Property
    Public ReadOnly Property ToDate As Date
        Get
            Return _ToDate
        End Get
    End Property

    Public Property AssignedTeams As New List(Of Team)

    Public Property Name As String
    Public Property StartOfSchedule As Date

    Public ReadOnly Property SelectionType As ShiftSelectionType
        Get
            Return _SelectionType
        End Get
    End Property

    Public Event Saved()
    Public Event Deleted()

    Public Sub New(ByVal fromDate As Date, ByVal toDate As Date, ByVal name As String, ByVal campaignStart As Date, ByVal shiftSelectionType As ShiftSelectionType)
        Me.New(0, fromDate, toDate, name, campaignStart, shiftSelectionType)
    End Sub
    Public Sub New(ByVal id As Integer, ByVal fromDate As Date, ByVal toDate As Date, ByVal name As String, ByVal startOfSchedule As Date, ByVal shiftSelectionType As ShiftSelectionType)
        _Id = id
        _FromDate = fromDate
        _ToDate = toDate
        Me.Name = name
        Me.StartOfSchedule = startOfSchedule
        _SelectionType = shiftSelectionType
    End Sub

    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader)
        Dim id As Integer, fromDate As Date, toDate As Date, name As String, startOfSchedule As Date, shiftSelectionType As ShiftSelectionType

        id = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.Id))
        fromDate = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.FromDate))
        toDate = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.ToDate))
        name = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.Name))
        startOfSchedule = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.ScheduleStart))
        shiftSelectionType = reader(SqlColumn.GetIdentifierOfColumn(CampaignTable.Columns.ShiftSelectionType))

        Return New Campaign(id, fromDate, toDate, name, startOfSchedule, shiftSelectionType)
    End Function

    Public Sub Save(ByVal conn As ShiftRequestConnection)
        Try
            If _Id = 0 Then
                Dim insert As New SqlInsert(New CampaignTable,
                                            {New SqlDateColumnValuePair(CampaignTable.Columns.FromDate, _FromDate),
                                             New SqlDateColumnValuePair(CampaignTable.Columns.ToDate, _ToDate),
                                             New SqlStringColumnValuePair(CampaignTable.Columns.Name, Me.Name),
                                             New SqlDateColumnValuePair(CampaignTable.Columns.ScheduleStart, Me.StartOfSchedule),
                                             New SqlNumberColumnValuePair(CampaignTable.Columns.ShiftSelectionType, Me.SelectionType)})

                _Id = conn.ExecuteSQLStatement(insert, True)

                For Each t In Me.AssignedTeams
                    insert = New SqlInsert(New TeamsInCampaignTable,
                                           {New SqlNumberColumnValuePair(TeamsInCampaignTable.Columns.CampaignId, _Id),
                                            New SqlStringColumnValuePair(TeamsInCampaignTable.Columns.Team_Id, t.Id)})

                    conn.ExecuteSQLStatement(insert)
                Next
            Else
                Dim update As New SqlUpdate(New CampaignTable,
                                            {New SqlStringColumnValuePair(CampaignTable.Columns.Name, Me.Name),
                                             New SqlDateColumnValuePair(CampaignTable.Columns.ScheduleStart, Me.StartOfSchedule),
                                             New SqlNumberColumnValuePair(CampaignTable.Columns.ShiftSelectionType, Me.SelectionType)},
                                            {New SqlNumberColumnValuePair(CampaignTable.Columns.Id, _Id)})

                conn.ExecuteSQLStatement(update)
            End If

            RaiseEvent Saved()
        Catch ex As OracleClient.OracleException
            Throw
        End Try
    End Sub
    Public Sub Delete()
        Dim conn As New ShiftRequestConnection

        Try
            If _Id > 0 Then
                Dim delete As New SqlDelete(New CampaignTable,
                                            {New SqlNumberColumnValuePair(CampaignTable.Columns.Id, _Id)})

                conn.ExecuteSQLStatement(delete)
            End If

            RaiseEvent Deleted()
        Catch ex As OracleClient.OracleException
            Throw
        Finally
            conn.CloseConnection()
        End Try
    End Sub

    Public Shared Function GetAllCampaigns() As List(Of Campaign)
        Dim db As New ShiftRequestConnection

        Dim query As New SqlQuery(New CampaignTable,
                                  Nothing,
                                  Nothing,
                                  {New SqlOrderBy(CampaignTable.Columns.Created, 1, SqlOrderBy.orders.Descending)})

        Dim reader = db.ExecuteQuery(query)

        Dim campaigns As New List(Of Campaign)

        While reader.Read
            campaigns.Add(Campaign.InstantiateFromDB(reader))
        End While

        query = New SqlQuery(New TeamsInCampaignTable,
                             Nothing,
                             Nothing,
                             New SqlInnerJoin(New TeamTable,
                                              Nothing,
                                              Nothing,
                                              {New SqlJoinCondition(New TeamsInCampaignTable, New SqlColumn(TeamsInCampaignTable.Columns.Team_Id), New TeamTable, New SqlColumn(TeamTable.Columns.Id))},
                                              Nothing))

        reader = db.ExecuteQuery(query)

        While reader.Read
            campaigns.Find(Function(c) c.Id = reader(SqlColumn.GetIdentifierOfColumn(TeamsInCampaignTable.Columns.CampaignId))).AssignedTeams.Add(Team.InstantiateFromDB(reader))
        End While

        db.CloseConnection()

        Return campaigns
    End Function

    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing OrElse (TypeOf obj Is Campaign) = False Then Return False
        If CType(obj, Campaign).Id = 0 Then Return (Me Is obj)
        Return CType(obj, Campaign).Id = _Id
    End Function

End Class
