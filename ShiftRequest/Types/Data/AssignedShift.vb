﻿Public Class AssignedShift
    Implements IComparable

    Private _Id As Integer
    Private _AgentId As Integer
    Private _Day As ShiftDay
    Private _ShiftSpan As ShiftTimeSpan

    Public ReadOnly Property Id As Integer
        Get
            Return _Id
        End Get
    End Property
    Public ReadOnly Property AgentId As Integer
        Get
            Return _AgentId
        End Get
    End Property

    Public Property Day As ShiftDay
        Get
            Return _Day
        End Get
        Set(ByVal value As ShiftDay)
            If value Is Nothing Then Throw New ArgumentNullException("Day cannot be nothing.")

            _Day = value
        End Set
    End Property

    Public Property ShiftSpan As ShiftTimeSpan
        Get
            Return _ShiftSpan
        End Get
        Set(ByVal value As ShiftTimeSpan)
            If value Is Nothing Then Throw New ArgumentNullException("ShiftSpan cannot be nothing.")

            _ShiftSpan = value
        End Set
    End Property

    Public Event Saved()
    Public Event Deleted()

    Public Sub New(ByVal agentId As Integer, ByVal day As ShiftDay.Days, ByVal shiftSpan As ShiftTimeSpan)
        Me.new(0, agentId, day, shiftSpan)
    End Sub
    Protected Sub New(ByVal id As Integer, ByVal agentId As Integer, ByVal day As ShiftDay.Days, ByVal shiftSpan As ShiftTimeSpan)
        _Id = id
        _AgentId = agentId
        Me.Day = New ShiftDay(day)
        _ShiftSpan = shiftSpan
    End Sub

    Public Sub save(ByVal conn As ShiftRequestConnection)
        If conn Is Nothing Then Throw New ArgumentNullException("Connection cannot be nothing.")

        If _Id = 0 Then
            Dim check = conn.ExecuteScalarQuery(New SqlScalarQuery(New AssignedShiftTable,
                                                                   Nothing,
                                                                   SqlScalarQuery.ScalarQueryTypes.Count,
                                                                   {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.CampaignId, My.Application.SelectedCampaign.Id),
                                                                    New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Agent_Id, _AgentId),
                                                                    New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Day, _Day.Day)}))
            If check > 0 Then
                Throw New InvalidOperationException("Agenten er allerede tildelt en vagt " + Me.Day.DayDescription)
            Else
                Dim insert As New SqlInsert(New AssignedShiftTable,
                                            {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Day, _Day.Day),
                                             New SqlDecimalColumnValuePair(AssignedShiftTable.Columns.ShiftStart, _ShiftSpan.StartTime.AsDecimal),
                                             New SqlDecimalColumnValuePair(AssignedShiftTable.Columns.ShiftEnd, _ShiftSpan.EndTime.AsDecimal),
                                             New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Agent_Id, _AgentId),
                                             New SqlNumberColumnValuePair(AssignedShiftTable.Columns.CampaignId, My.Application.SelectedCampaign.Id)})

                _Id = conn.ExecuteSQLStatement(insert, True)
            End If
        Else
            Dim update As New SqlUpdate(New AssignedShiftTable,
                                        {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Day, _Day.Day),
                                         New SqlDecimalColumnValuePair(AssignedShiftTable.Columns.ShiftStart, _ShiftSpan.StartTime.AsDecimal),
                                         New SqlDecimalColumnValuePair(AssignedShiftTable.Columns.ShiftEnd, _ShiftSpan.EndTime.AsDecimal)},
                                        {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Id, _Id)})

            conn.ExecuteSQLStatement(update)
        End If

        RaiseEvent Saved()
    End Sub
    Public Sub delete(ByVal conn As ShiftRequestConnection)
        If conn Is Nothing Then Throw New ArgumentNullException("Connection cannot be nothing.")

        If _Id > 0 Then
            Dim delete As New SqlDelete(New AssignedShiftTable,
                                        {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.Id, _Id)})

            conn.ExecuteSQLStatement(delete)
        End If

        RaiseEvent Deleted()
    End Sub

    Public Shared Function GetAssignedShiftsInCampaign(ByVal conn As ShiftRequestConnection, ByVal campaign As Campaign) As List(Of AssignedShift)
        Dim assignedShifts As New List(Of AssignedShift)

        Dim agentIdsInCampaign = (From a In My.Application.AgentsInSelectedCampaign Select a.Id).ToList

        Dim reader = conn.ExecuteQuery(New SqlQuery(New AssignedShiftTable,
                                                    Nothing,
                                                    {New SqlNumberColumnValuePair(AssignedShiftTable.Columns.CampaignId, campaign.Id),
                                                     New SqlNumberInClause(AssignedShiftTable.Columns.Agent_Id, agentIdsInCampaign)}))

        While reader.Read
            assignedShifts.Add(AssignedShift.InstantiateFromDB(reader))
        End While

        Return assignedShifts
    End Function
    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader)
        Dim id As Integer, agentId As Integer, day As Integer, startTime As Decimal, endTime As Decimal

        id = reader(SqlColumn.GetIdentifierOfColumn(AssignedShiftTable.Columns.Id))
        agentId = reader(SqlColumn.GetIdentifierOfColumn(AssignedShiftTable.Columns.Agent_Id))
        day = reader(SqlColumn.GetIdentifierOfColumn(AssignedShiftTable.Columns.Day))
        startTime = reader(SqlColumn.GetIdentifierOfColumn(AssignedShiftTable.Columns.ShiftStart))
        endTime = reader(SqlColumn.GetIdentifierOfColumn(AssignedShiftTable.Columns.ShiftEnd))

        Return New AssignedShift(id, agentId, day, New ShiftTimeSpan(New ShiftTime(startTime),
                                                                     New ShiftTime(endTime)))
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        If obj Is Nothing Then Return 0
        If Not TypeOf obj Is AssignedShift Then Throw New ArgumentException("Cannot compare to object of type " & obj.GetType.ToString & ".")

        Dim assignedShiftToCompare = CType(obj, AssignedShift)

        If _Day.CompareTo(assignedShiftToCompare.Day) = 0 Then
            Return Me.ShiftSpan.StartTime.CompareTo(assignedShiftToCompare.ShiftSpan.StartTime)
        Else
            Return _Day.CompareTo(assignedShiftToCompare.Day)
        End If
    End Function
End Class
