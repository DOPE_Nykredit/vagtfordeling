﻿Public Class Agent

    Private _Id As Integer
    Private _Initials As String
    Private _FirstName As String
    Private _LastName As String
    Private _Team As Team

    Public ReadOnly Property Id As Integer
        Get
            Return _Id
        End Get
    End Property
    Public ReadOnly Property Initials As String
        Get
            Return _Initials
        End Get
    End Property
    Public ReadOnly Property FirstName As String
        Get
            Return _FirstName
        End Get
    End Property
    Public ReadOnly Property LastName As String
        Get
            Return _LastName
        End Get
    End Property
    Public ReadOnly Property FullName As String
        Get
            Return _FirstName + " " + _LastName
        End Get
    End Property

    Public ReadOnly Property Team As Team
        Get
            Return _Team
        End Get
    End Property

    Public Sub New(ByVal id As Integer, ByVal initials As String, ByVal firstName As String, ByVal lastName As String, ByVal team As Team)
        _Id = id
        _Initials = initials
        _FirstName = firstName
        _LastName = lastName
        _Team = team
    End Sub

    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then Return False
        If Not obj.GetType.Equals(Me.GetType) Then Return False
        Return String.Equals(CType(obj, Agent).Initials.ToUpper, Me.Initials.ToUpper)
    End Function

    Public Shared Function GetAgentsInCampaign(ByVal conn As ShiftRequestConnection, ByVal campaign As Campaign)
        Dim agents As New List(Of Agent)

        Dim query As String = "SELECT " +
                                  "DISTINCT(age.ID) AS " + Chr(34) + "AGENT_ID" + Chr(34) + ", " +
                                  "age.INITIALER, " +
                                  "age.FORNAVN, " +
                                  "age.EFTERNAVN, " +
                                  "tea.ID AS " + Chr(34) + "TEAM_ID" + Chr(34) + ", " +
                                  "tea.TEAM, " +
                                  "tea.TEAM_NAVN, " +
                                  "tea.TEAM_GRUPPE " +
                              "FROM " +
                                  "KS_DRIFT.AGENTER_LISTE age " +
                              "INNER JOIN " +
                                  "KS_DRIFT.V_TEAM_DATO vt " +
                              "ON " +
                                  "age.ID = vt.AGENT_ID " +
                              "INNER JOIN " +
                                  "KS_DRIFT.AGENTER_TEAMS tea " +
                              "ON " +
                                  "vt.TEAM_ID = tea.ID " +
                              "INNER JOIN " +
                                  "KS_DRIFT.HRSR_CAMPAIGN_TEAMS cat " +
                              "ON " +
                                  "tea.ID = cat.TEAM_ID " +
                              "WHERE " +
                                  "vt.DATO BETWEEN " + SqlDateColumnValuePair.ConvertToOracleDateTime(campaign.StartOfSchedule) + " AND " + SqlDateColumnValuePair.ConvertToOracleDateTime(campaign.StartOfSchedule.AddMonths(2)) + " AND cat.CAMPAIGNID = " & My.Application.SelectedCampaign.Id

        Dim reader As OracleClient.OracleDataReader = conn.ExecuteQuery(query)

        While reader.Read
            Dim team As New Team(reader("TEAM_ID"),
                                 reader("TEAM"),
                                 reader("TEAM_NAVN"),
                                 reader("TEAM_GRUPPE"))

            agents.Add(New Agent(reader("AGENT_ID"),
                                 reader("INITIALER"),
                                 reader("FORNAVN"),
                                 reader("EFTERNAVN"),
                                 team))
        End While

        Dim x = agents.OrderBy(Function(a) a.Initials)

        Return x.ToList
    End Function

    Public Shared Function getAgentFromEnvironment(ByVal conn As ShiftRequestConnection) As Agent
        Dim reader = conn.ExecuteQuery("SELECT " +
                                           "age.ID as AGENT_ID, " +
                                           "age.INITIALER, " +
                                           "age.FORNAVN, " +
                                           "age.EFTERNAVN, " +
                                           "tea.ID AS TEAM_ID, " +
                                           "tea.TEAM, " +
                                           "tea.TEAM_NAVN, " +
                                           "tea.TEAM_GRUPPE " +
                                       "FROM " +
                                           "KS_DRIFT.AGENTER_LISTE age " +
                                       "INNER JOIN " +
                                           "KS_DRIFT.V_TEAM_DATO vt " +
                                       "ON " +
                                           "age.ID = vt.AGENT_ID " +
                                       "INNER JOIN " +
                                           "KS_DRIFT.AGENTER_TEAMS tea " +
                                       "ON " +
                                           "vt.TEAM_ID = tea.ID " +
                                       "WHERE " +
                                           "DATO = trunc(sysdate) AND age.INITIALER = '" + Environment.UserName.ToUpper + "'")

        Dim agent As Agent = Nothing

        If reader.Read Then
            agent = New Agent(reader("AGENT_ID"),
                              reader("INITIALER"),
                              reader("FORNAVN"),
                              reader("EFTERNAVN"),
                              New Team(reader("TEAM_ID"),
                                       reader("TEAM"),
                                       reader("TEAM_NAVN"),
                                       reader("TEAM_GRUPPE")))
        End If

        Return agent
    End Function
    Public Shared Function getAllAgents(ByVal conn As ShiftRequestConnection) As List(Of Agent)
        Dim agents As New List(Of Agent)

        Dim reader = conn.ExecuteQuery("SELECT DISTINCT(age.ID) as AGENT_ID, age.INITIALER, age.FORNAVN, age.EFTERNAVN, tea.ID AS TEAM_ID, tea.TEAM, tea.TEAM_NAVN, tea.TEAM_GRUPPE FROM KS_DRIFT.AGENTER_LISTE age INNER JOIN KS_DRIFT.V_TEAM_DATO vt ON age.ID = vt.AGENT_ID INNER JOIN KS_DRIFT.AGENTER_TEAMS tea ON vt.TEAM_ID = tea.ID WHERE DATO BETWEEN trunc(sysdate) AND trunc(ADD_MONTHS(sysdate, 1))")

        While reader.Read
            agents.Add(New Agent(reader("AGENT_ID"),
                                 reader("INITIALER"),
                                 reader("FORNAVN"),
                                 reader("EFTERNAVN"),
                                 New Team(reader("TEAM_ID"),
                                          reader("TEAM"),
                                          reader("TEAM_NAVN"),
                                          reader("TEAM_GRUPPE"))))
        End While

        agents.OrderBy(Function(a) a.Initials)

        Return agents
    End Function
    Public Shared Function getAllAgents(ByVal conn As ShiftRequestConnection, ByVal fromDate As Date, ByVal toDate As Date) As List(Of Agent)
        Dim agents As New List(Of Agent)

        Dim reader = conn.ExecuteQuery("SELECT DISTINCT(age.ID) as AGENT_ID, age.INITIALER, age.FORNAVN, age.EFTERNAVN, tea.ID AS TEAM_ID, tea.TEAM, tea.TEAM_NAVN, tea.TEAM_GRUPPE FROM KS_DRIFT.AGENTER_LISTE age INNER JOIN KS_DRIFT.V_TEAM_DATO vt ON age.ID = vt.AGENT_ID INNER JOIN KS_DRIFT.AGENTER_TEAMS tea ON vt.TEAM_ID = tea.ID WHERE DATO BETWEEN '" + fromDate.ToString("YYYY-MM-DD") + "' AND '" + toDate.ToString("YYYY-MM-DD") + "'")

        While reader.read
            agents.Add(New Agent(reader("AGENT_ID"),
                     reader("INITIALER"),
                     reader("FORNAVN"),
                     reader("EFTERNAVN"),
                     New Team(reader("TEAM_ID"),
                              reader("TEAM"),
                              reader("TEAM_NAVN"),
                              reader("TEAM_GRUPPE"))))
        End While

        Return agents
    End Function

    Public Overrides Function ToString() As String
        Return _Initials + " - " + Me.FullName
    End Function
End Class