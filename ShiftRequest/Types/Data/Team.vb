﻿Public Class Team

    Private _Id As Integer
    Private _Code As String
    Private _Name As String
    Private _Group As String

    Public ReadOnly Property Id As Integer
        Get
            Return _Id
        End Get
    End Property
    Public ReadOnly Property Code As String
        Get
            Return _Code
        End Get
    End Property
    Public ReadOnly Property Name As String
        Get
            Return _Name
        End Get
    End Property
    Public ReadOnly Property Group As String
        Get
            Return _Group
        End Get
    End Property

    Public Sub New(ByVal id As Integer, ByVal code As String, ByVal name As String, ByVal group As String)
        _Id = id
        _Code = code
        _Name = name
        _Group = group
    End Sub

    Public Shared Function GetTeamsInCampaign(ByVal conn As ShiftRequestConnection, ByVal campaign As Campaign)
        Dim teams As New List(Of Team)

        Dim reader = conn.ExecuteQuery(New SqlQuery(New TeamsInCampaignTable,
                                                    {TeamTable.Columns.Id, TeamTable.Columns.Team, TeamTable.Columns.Team_Navn, TeamTable.Columns.Team_Gruppe},
                                                    {New SqlNumberColumnValuePair(TeamTable.Columns.Id, campaign.Id)}))

        While reader.Read
            teams.Add(Team.InstantiateFromDB(reader))
        End While

        Return teams
    End Function
    Public Shared Function GetAllTeams(ByVal conn As ShiftRequestConnection)
        Dim teams As New List(Of Team)

        Dim reader = conn.ExecuteQuery(New SqlQuery(New TeamTable,
                                                    Nothing,
                                                    Nothing))

        While reader.Read
            teams.Add(Team.InstantiateFromDB(reader))
        End While

        Return teams
    End Function
    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader) As Team
        Dim id As Integer, code As String, name As String, group As String

        id = reader(SqlColumn.GetIdentifierOfColumn(TeamTable.Columns.Id))
        code = reader(SqlColumn.GetIdentifierOfColumn(TeamTable.Columns.Team))
        name = reader(SqlColumn.GetIdentifierOfColumn(TeamTable.Columns.Team_Navn))
        group = reader(SqlColumn.GetIdentifierOfColumn(TeamTable.Columns.Team_Gruppe))

        Return New Team(id, code, name, group)
    End Function

    Public Overrides Function ToString() As String
        Return _Name
    End Function

End Class
