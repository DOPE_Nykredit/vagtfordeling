﻿Public Class StandardShift

    Private _ID As Integer

    Private _Shift As ShiftTimeSpan

    Public ReadOnly Property ID As Integer
        Get
            Return _ID
        End Get
    End Property

    Public ReadOnly Property TimeStart As ShiftTime
        Get
            If _Shift Is Nothing Then Return Nothing Else Return _Shift.StartTime
        End Get
    End Property
    Public ReadOnly Property TimeEnd As ShiftTime
        Get
            If _Shift Is Nothing Then Return Nothing Else Return _Shift.EndTime
        End Get
    End Property
    Public Property TimeSpan As ShiftTimeSpan
        Get
            Return _Shift
        End Get
        Set(ByVal value As ShiftTimeSpan)
            _Shift = value
        End Set
    End Property

    Public Property AppliedDays As New List(Of ShiftDay)

    Public Event Saved()
    Public Event Deleted()

    Public Sub New(ByVal shiftSpan As ShiftTimeSpan)
        Me.New(0, shiftSpan)
    End Sub
    Public Sub New(ByVal id As Integer, ByVal shiftSpan As ShiftTimeSpan)
        _ID = id
        _Shift = shiftSpan
    End Sub

    Public Sub Save(ByVal conn As ShiftRequestConnection)
        If _ID = 0 Then
            Dim insert As New SqlInsert(New StandardShiftTable,
                                        {New SqlDecimalColumnValuePair(StandardShiftTable.Columns.ShiftStart, _Shift.StartTime.AsDecimal),
                                         New SqlDecimalColumnValuePair(StandardShiftTable.Columns.ShiftEnd, _Shift.EndTime.AsDecimal),
                                         New SqlNumberColumnValuePair(StandardShiftTable.Columns.CampaignId, My.Application.SelectedCampaign.Id)})

            _ID = conn.ExecuteSQLStatement(insert, True)

            For Each d In Me.AppliedDays
                conn.ExecuteSQLStatement(New SqlInsert(New StandardShiftOnDayTable,
                                                       {New SqlNumberColumnValuePair(StandardShiftOnDayTable.Columns.StandardShiftID, _ID),
                                                        New SqlNumberColumnValuePair(StandardShiftOnDayTable.Columns.Day, d.Day)}))
            Next

            Me.OnSaved()
        Else
            Dim update As New SqlUpdate(New StandardShiftTable,
                                        {New SqlDecimalColumnValuePair(StandardShiftTable.Columns.ShiftStart, _Shift.StartTime.AsDecimal),
                                         New SqlDecimalColumnValuePair(StandardShiftTable.Columns.ShiftEnd, _Shift.EndTime.AsDecimal)},
                                        {New SqlNumberColumnValuePair(StandardShiftTable.Columns.ID, _ID)})

            conn.ExecuteSQLStatement(update)

            Dim delete As New SqlDelete(New StandardShiftOnDayTable,
                                        {New SqlNumberColumnValuePair(StandardShiftOnDayTable.Columns.StandardShiftID, _ID)})

            conn.ExecuteSQLStatement(delete)

            For Each d In Me.AppliedDays
                conn.ExecuteSQLStatement(New SqlInsert(New StandardShiftOnDayTable,
                                                       {New SqlNumberColumnValuePair(StandardShiftOnDayTable.Columns.StandardShiftID, _ID),
                                                        New SqlNumberColumnValuePair(StandardShiftOnDayTable.Columns.Day, d.Day)}))
            Next

            'Throw New InvalidOperationException("Cannot update StandardShifts.")
        End If
    End Sub

    Public Sub Delete()
        Dim db As New ShiftRequestConnection

        If _ID > 0 Then
            Try
                db.ExecuteSQLStatement(New SqlDelete(New StandardShiftTable,
                                                     {New SqlNumberColumnValuePair(StandardShiftTable.Columns.ID, _ID)}))

                Me.OnDeleted()
            Catch ex As Exception
                Throw
            Finally
                db.CloseConnection()
            End Try
        End If
    End Sub

    Public Shared Function GetStandardShiftsInCampaign(ByVal conn As ShiftRequestConnection, ByVal campaign As Campaign) As List(Of StandardShift)
        Dim standardShifts As New List(Of StandardShift)

        Dim reader = conn.ExecuteQuery(New SqlQuery(New StandardShiftTable,
                                                    Nothing,
                                                    {New SqlNumberColumnValuePair(StandardShiftTable.Columns.CampaignId, campaign.Id)}))

        While reader.Read
            standardShifts.Add(InstantiateFromDB(reader))
        End While

        If standardShifts.Count > 0 Then
            Dim standardShiftIds As New List(Of Integer)

            standardShifts.ForEach(Sub(s) standardShiftIds.Add(s.ID))

            reader = conn.ExecuteQuery(New SqlQuery(New StandardShiftOnDayTable,
                                                    Nothing,
                                                    {New SqlNumberInClause(StandardShiftOnDayTable.Columns.StandardShiftID, standardShiftIds)}))

            While reader.Read
                standardShifts.First(Function(s) s.ID = reader(SqlColumn.GetIdentifierOfColumn(StandardShiftOnDayTable.Columns.StandardShiftID))).AppliedDays.Add(New ShiftDay(reader(SqlColumn.GetIdentifierOfColumn(StandardShiftOnDayTable.Columns.Day))))
            End While
        End If

        Dim x = standardShifts.OrderBy(Function(s) s.TimeStart)

        Return x.ToList
    End Function
    Public Shared Function InstantiateFromDB(ByVal reader As OracleClient.OracleDataReader) As StandardShift
        Dim id As Integer, shiftSpan As ShiftTimeSpan

        id = reader(SqlColumn.GetIdentifierOfColumn(StandardShiftTable.Columns.ID))
        shiftSpan = New ShiftTimeSpan(New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(StandardShiftTable.Columns.ShiftStart))),
                                  New ShiftTime(reader(SqlColumn.GetIdentifierOfColumn(StandardShiftTable.Columns.ShiftEnd))))

        Return New StandardShift(id, shiftSpan)
    End Function

    Protected Sub OnSaved()
        RaiseEvent Saved()
    End Sub
    Protected Sub OnDeleted()
        RaiseEvent Deleted()
    End Sub

    Public Overrides Function ToString() As String
        Return _Shift.ToString
    End Function

    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj Is Nothing Then Return False
        If Not obj.GetType.Equals(Me.GetType) Then Return False
        Return _ID = CType(obj, StandardShift).ID
    End Function
End Class
