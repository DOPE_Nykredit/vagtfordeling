﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditShiftRequestsFrom
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dayDescriptionLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.grpPrimaryRequests = New System.Windows.Forms.GroupBox()
        Me.primaryShiftSelection = New ShiftRequest.ShiftSelection()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.grpSecondaryRequests = New System.Windows.Forms.GroupBox()
        Me.secondaryShiftSelection = New ShiftRequest.ShiftSelection()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.generalLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdGetAgentShiftRequests = New System.Windows.Forms.Button()
        Me.cboAgents = New ShiftRequest.AgentComboBox()
        Me.grpPrimaryRequests.SuspendLayout()
        Me.grpSecondaryRequests.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.generalLayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'dayDescriptionLayoutPanel
        '
        Me.dayDescriptionLayoutPanel.AutoSize = True
        Me.dayDescriptionLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.dayDescriptionLayoutPanel.ColumnCount = 1
        Me.dayDescriptionLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.dayDescriptionLayoutPanel.Location = New System.Drawing.Point(3, 31)
        Me.dayDescriptionLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 31, 3, 3)
        Me.dayDescriptionLayoutPanel.Name = "dayDescriptionLayoutPanel"
        Me.dayDescriptionLayoutPanel.RowCount = 1
        Me.dayDescriptionLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.dayDescriptionLayoutPanel.Size = New System.Drawing.Size(0, 0)
        Me.dayDescriptionLayoutPanel.TabIndex = 3
        '
        'grpPrimaryRequests
        '
        Me.grpPrimaryRequests.AutoSize = True
        Me.grpPrimaryRequests.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpPrimaryRequests.Controls.Add(Me.primaryShiftSelection)
        Me.grpPrimaryRequests.Location = New System.Drawing.Point(16, 10)
        Me.grpPrimaryRequests.Margin = New System.Windows.Forms.Padding(10)
        Me.grpPrimaryRequests.Name = "grpPrimaryRequests"
        Me.grpPrimaryRequests.Size = New System.Drawing.Size(131, 38)
        Me.grpPrimaryRequests.TabIndex = 4
        Me.grpPrimaryRequests.TabStop = False
        Me.grpPrimaryRequests.Text = "-"
        '
        'primaryShiftSelection
        '
        Me.primaryShiftSelection.AddShiftTimeMarginToComboBoxes = True
        Me.primaryShiftSelection.AutoSize = True
        Me.primaryShiftSelection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.primaryShiftSelection.DisplayDaysMode = ShiftRequest.ShiftSelection.DisplayDays.StandardShifts
        Me.primaryShiftSelection.Enabled = False
        Me.primaryShiftSelection.Location = New System.Drawing.Point(3, 17)
        Me.primaryShiftSelection.Margin = New System.Windows.Forms.Padding(0)
        Me.primaryShiftSelection.MinimumSize = New System.Drawing.Size(125, 0)
        Me.primaryShiftSelection.Name = "primaryShiftSelection"
        Me.primaryShiftSelection.Size = New System.Drawing.Size(125, 3)
        Me.primaryShiftSelection.SplitDays = False
        Me.primaryShiftSelection.TabIndex = 0
        '
        'cmdClose
        '
        Me.cmdClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdClose.Location = New System.Drawing.Point(555, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(100, 23)
        Me.cmdClose.TabIndex = 5
        Me.cmdClose.Text = "Luk"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSave.Enabled = False
        Me.cmdSave.Location = New System.Drawing.Point(449, 3)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(100, 23)
        Me.cmdSave.TabIndex = 6
        Me.cmdSave.Text = "Gem"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'grpSecondaryRequests
        '
        Me.grpSecondaryRequests.AutoSize = True
        Me.grpSecondaryRequests.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpSecondaryRequests.Controls.Add(Me.secondaryShiftSelection)
        Me.grpSecondaryRequests.Location = New System.Drawing.Point(167, 10)
        Me.grpSecondaryRequests.Margin = New System.Windows.Forms.Padding(10)
        Me.grpSecondaryRequests.Name = "grpSecondaryRequests"
        Me.grpSecondaryRequests.Size = New System.Drawing.Size(236, 38)
        Me.grpSecondaryRequests.TabIndex = 7
        Me.grpSecondaryRequests.TabStop = False
        Me.grpSecondaryRequests.Text = "-"
        '
        'secondaryShiftSelection
        '
        Me.secondaryShiftSelection.AddShiftTimeMarginToComboBoxes = False
        Me.secondaryShiftSelection.AutoSize = True
        Me.secondaryShiftSelection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.secondaryShiftSelection.DisplayDaysMode = ShiftRequest.ShiftSelection.DisplayDays.FreeInterval
        Me.secondaryShiftSelection.Enabled = False
        Me.secondaryShiftSelection.Location = New System.Drawing.Point(3, 17)
        Me.secondaryShiftSelection.Margin = New System.Windows.Forms.Padding(0)
        Me.secondaryShiftSelection.MinimumSize = New System.Drawing.Size(230, 0)
        Me.secondaryShiftSelection.Name = "secondaryShiftSelection"
        Me.secondaryShiftSelection.Size = New System.Drawing.Size(230, 3)
        Me.secondaryShiftSelection.SplitDays = False
        Me.secondaryShiftSelection.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdClose)
        Me.Panel1.Controls.Add(Me.cmdSave)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 104)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 40, 3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(667, 34)
        Me.Panel1.TabIndex = 8
        '
        'generalLayoutPanel
        '
        Me.generalLayoutPanel.AutoSize = True
        Me.generalLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.generalLayoutPanel.ColumnCount = 3
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.Controls.Add(Me.dayDescriptionLayoutPanel, 0, 0)
        Me.generalLayoutPanel.Controls.Add(Me.grpPrimaryRequests, 1, 0)
        Me.generalLayoutPanel.Controls.Add(Me.grpSecondaryRequests, 2, 0)
        Me.generalLayoutPanel.Location = New System.Drawing.Point(12, 40)
        Me.generalLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 3, 3, 40)
        Me.generalLayoutPanel.Name = "generalLayoutPanel"
        Me.generalLayoutPanel.RowCount = 1
        Me.generalLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.generalLayoutPanel.Size = New System.Drawing.Size(413, 58)
        Me.generalLayoutPanel.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 14)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Vælg medarbejder:"
        '
        'cmdGetAgentShiftRequests
        '
        Me.cmdGetAgentShiftRequests.Enabled = False
        Me.cmdGetAgentShiftRequests.Location = New System.Drawing.Point(401, 5)
        Me.cmdGetAgentShiftRequests.Name = "cmdGetAgentShiftRequests"
        Me.cmdGetAgentShiftRequests.Size = New System.Drawing.Size(100, 23)
        Me.cmdGetAgentShiftRequests.TabIndex = 12
        Me.cmdGetAgentShiftRequests.Text = "Hent"
        Me.cmdGetAgentShiftRequests.UseVisualStyleBackColor = True
        '
        'cboAgents
        '
        Me.cboAgents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgents.FormattingEnabled = True
        Me.cboAgents.Location = New System.Drawing.Point(140, 6)
        Me.cboAgents.MaxDropDownItems = 14
        Me.cboAgents.Name = "cboAgents"
        Me.cboAgents.SelectedAgent = Nothing
        Me.cboAgents.Size = New System.Drawing.Size(255, 22)
        Me.cboAgents.TabIndex = 11
        '
        'EditShiftRequestsFrom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(667, 138)
        Me.Controls.Add(Me.cmdGetAgentShiftRequests)
        Me.Controls.Add(Me.cboAgents)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.generalLayoutPanel)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EditShiftRequestsFrom"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Rediger vagtønsker"
        Me.TopMost = True
        Me.grpPrimaryRequests.ResumeLayout(False)
        Me.grpPrimaryRequests.PerformLayout()
        Me.grpSecondaryRequests.ResumeLayout(False)
        Me.grpSecondaryRequests.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.generalLayoutPanel.ResumeLayout(False)
        Me.generalLayoutPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dayDescriptionLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents grpPrimaryRequests As System.Windows.Forms.GroupBox
    Friend WithEvents primaryShiftSelection As ShiftRequest.ShiftSelection
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents grpSecondaryRequests As System.Windows.Forms.GroupBox
    Friend WithEvents secondaryShiftSelection As ShiftRequest.ShiftSelection
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents generalLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAgents As ShiftRequest.AgentComboBox
    Friend WithEvents cmdGetAgentShiftRequests As System.Windows.Forms.Button
End Class
