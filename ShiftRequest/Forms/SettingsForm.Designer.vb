﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SettingsTabs = New System.Windows.Forms.TabControl()
        Me.tabWorkdayDefinition = New System.Windows.Forms.TabPage()
        Me.EditWorkDayDefinitions = New ShiftRequest.EditWorkDayDefinitions()
        Me.tabStandardShift = New System.Windows.Forms.TabPage()
        Me.ManageStandardShifts = New ShiftRequest.ManageStandardShifts()
        Me.tabShiftRequirements = New System.Windows.Forms.TabPage()
        Me.grpAutoFill = New System.Windows.Forms.GroupBox()
        Me.rdbAutoFillUp = New System.Windows.Forms.RadioButton()
        Me.rdbAutoFillDown = New System.Windows.Forms.RadioButton()
        Me.rdbAutoFillOff = New System.Windows.Forms.RadioButton()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.ShiftRequirementsGrid = New ShiftRequest.ShiftRequirementsGrid()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.SettingsTabs.SuspendLayout()
        Me.tabWorkdayDefinition.SuspendLayout()
        Me.tabStandardShift.SuspendLayout()
        Me.tabShiftRequirements.SuspendLayout()
        Me.grpAutoFill.SuspendLayout()
        CType(Me.ShiftRequirementsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SettingsTabs
        '
        Me.SettingsTabs.Controls.Add(Me.tabWorkdayDefinition)
        Me.SettingsTabs.Controls.Add(Me.tabStandardShift)
        Me.SettingsTabs.Controls.Add(Me.tabShiftRequirements)
        Me.SettingsTabs.ItemSize = New System.Drawing.Size(150, 18)
        Me.SettingsTabs.Location = New System.Drawing.Point(3, 3)
        Me.SettingsTabs.Name = "SettingsTabs"
        Me.SettingsTabs.SelectedIndex = 0
        Me.SettingsTabs.Size = New System.Drawing.Size(752, 593)
        Me.SettingsTabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.SettingsTabs.TabIndex = 0
        '
        'tabWorkdayDefinition
        '
        Me.tabWorkdayDefinition.Controls.Add(Me.EditWorkDayDefinitions)
        Me.tabWorkdayDefinition.Location = New System.Drawing.Point(4, 22)
        Me.tabWorkdayDefinition.Name = "tabWorkdayDefinition"
        Me.tabWorkdayDefinition.Padding = New System.Windows.Forms.Padding(3)
        Me.tabWorkdayDefinition.Size = New System.Drawing.Size(744, 567)
        Me.tabWorkdayDefinition.TabIndex = 0
        Me.tabWorkdayDefinition.Text = "Definer arbejdsdag"
        Me.tabWorkdayDefinition.UseVisualStyleBackColor = True
        '
        'EditWorkDayDefinitions
        '
        Me.EditWorkDayDefinitions.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditWorkDayDefinitions.Location = New System.Drawing.Point(16, 18)
        Me.EditWorkDayDefinitions.Name = "EditWorkDayDefinitions"
        Me.EditWorkDayDefinitions.Size = New System.Drawing.Size(535, 334)
        Me.EditWorkDayDefinitions.TabIndex = 0
        '
        'tabStandardShift
        '
        Me.tabStandardShift.Controls.Add(Me.ManageStandardShifts)
        Me.tabStandardShift.Location = New System.Drawing.Point(4, 22)
        Me.tabStandardShift.Name = "tabStandardShift"
        Me.tabStandardShift.Padding = New System.Windows.Forms.Padding(3)
        Me.tabStandardShift.Size = New System.Drawing.Size(744, 567)
        Me.tabStandardShift.TabIndex = 1
        Me.tabStandardShift.Text = "Standardvagter"
        Me.tabStandardShift.UseVisualStyleBackColor = True
        '
        'ManageStandardShifts
        '
        Me.ManageStandardShifts.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ManageStandardShifts.Location = New System.Drawing.Point(20, 20)
        Me.ManageStandardShifts.Name = "ManageStandardShifts"
        Me.ManageStandardShifts.Size = New System.Drawing.Size(479, 458)
        Me.ManageStandardShifts.TabIndex = 0
        '
        'tabShiftRequirements
        '
        Me.tabShiftRequirements.Controls.Add(Me.grpAutoFill)
        Me.tabShiftRequirements.Controls.Add(Me.cmdSave)
        Me.tabShiftRequirements.Controls.Add(Me.ShiftRequirementsGrid)
        Me.tabShiftRequirements.Location = New System.Drawing.Point(4, 22)
        Me.tabShiftRequirements.Name = "tabShiftRequirements"
        Me.tabShiftRequirements.Padding = New System.Windows.Forms.Padding(3)
        Me.tabShiftRequirements.Size = New System.Drawing.Size(744, 567)
        Me.tabShiftRequirements.TabIndex = 2
        Me.tabShiftRequirements.Text = "Krav"
        Me.tabShiftRequirements.UseVisualStyleBackColor = True
        '
        'grpAutoFill
        '
        Me.grpAutoFill.Controls.Add(Me.rdbAutoFillUp)
        Me.grpAutoFill.Controls.Add(Me.rdbAutoFillDown)
        Me.grpAutoFill.Controls.Add(Me.rdbAutoFillOff)
        Me.grpAutoFill.Location = New System.Drawing.Point(582, 6)
        Me.grpAutoFill.Name = "grpAutoFill"
        Me.grpAutoFill.Size = New System.Drawing.Size(138, 103)
        Me.grpAutoFill.TabIndex = 2
        Me.grpAutoFill.TabStop = False
        Me.grpAutoFill.Text = "Autoudfyldning"
        '
        'rdbAutoFillUp
        '
        Me.rdbAutoFillUp.AutoSize = True
        Me.rdbAutoFillUp.Location = New System.Drawing.Point(19, 72)
        Me.rdbAutoFillUp.Name = "rdbAutoFillUp"
        Me.rdbAutoFillUp.Size = New System.Drawing.Size(39, 17)
        Me.rdbAutoFillUp.TabIndex = 2
        Me.rdbAutoFillUp.Text = "Op"
        Me.rdbAutoFillUp.UseVisualStyleBackColor = True
        '
        'rdbAutoFillDown
        '
        Me.rdbAutoFillDown.AutoSize = True
        Me.rdbAutoFillDown.Location = New System.Drawing.Point(19, 48)
        Me.rdbAutoFillDown.Name = "rdbAutoFillDown"
        Me.rdbAutoFillDown.Size = New System.Drawing.Size(45, 17)
        Me.rdbAutoFillDown.TabIndex = 1
        Me.rdbAutoFillDown.Text = "Ned"
        Me.rdbAutoFillDown.UseVisualStyleBackColor = True
        '
        'rdbAutoFillOff
        '
        Me.rdbAutoFillOff.AutoSize = True
        Me.rdbAutoFillOff.Checked = True
        Me.rdbAutoFillOff.Location = New System.Drawing.Point(19, 24)
        Me.rdbAutoFillOff.Name = "rdbAutoFillOff"
        Me.rdbAutoFillOff.Size = New System.Drawing.Size(40, 17)
        Me.rdbAutoFillOff.TabIndex = 0
        Me.rdbAutoFillOff.TabStop = True
        Me.rdbAutoFillOff.Text = "Fra"
        Me.rdbAutoFillOff.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(567, 536)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(100, 25)
        Me.cmdSave.TabIndex = 1
        Me.cmdSave.Text = "Gem"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'ShiftRequirementsGrid
        '
        Me.ShiftRequirementsGrid.AllowUserToAddRows = False
        Me.ShiftRequirementsGrid.AllowUserToDeleteRows = False
        Me.ShiftRequirementsGrid.AllowUserToResizeColumns = False
        Me.ShiftRequirementsGrid.AllowUserToResizeRows = False
        Me.ShiftRequirementsGrid.AutoFill = ShiftRequest.ShiftRequirementsGrid.AutoFills.None
        Me.ShiftRequirementsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ShiftRequirementsGrid.DefaultCellStyle = DataGridViewCellStyle1
        Me.ShiftRequirementsGrid.Location = New System.Drawing.Point(7, 6)
        Me.ShiftRequirementsGrid.MultiSelect = False
        Me.ShiftRequirementsGrid.Name = "ShiftRequirementsGrid"
        Me.ShiftRequirementsGrid.Size = New System.Drawing.Size(554, 555)
        Me.ShiftRequirementsGrid.TabIndex = 0
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(638, 602)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(117, 25)
        Me.cmdClose.TabIndex = 1
        Me.cmdClose.Text = "Luk"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'SettingsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(762, 634)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.SettingsTabs)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "SettingsForm"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Indstillinger"
        Me.TopMost = True
        Me.SettingsTabs.ResumeLayout(False)
        Me.tabWorkdayDefinition.ResumeLayout(False)
        Me.tabStandardShift.ResumeLayout(False)
        Me.tabShiftRequirements.ResumeLayout(False)
        Me.grpAutoFill.ResumeLayout(False)
        Me.grpAutoFill.PerformLayout()
        CType(Me.ShiftRequirementsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SettingsTabs As System.Windows.Forms.TabControl
    Friend WithEvents tabWorkdayDefinition As System.Windows.Forms.TabPage
    Friend WithEvents tabStandardShift As System.Windows.Forms.TabPage
    Friend WithEvents tabShiftRequirements As System.Windows.Forms.TabPage
    Friend WithEvents ShiftRequirementsGrid As ShiftRequest.ShiftRequirementsGrid
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents grpAutoFill As System.Windows.Forms.GroupBox
    Friend WithEvents rdbAutoFillUp As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAutoFillDown As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAutoFillOff As System.Windows.Forms.RadioButton
    Friend WithEvents ManageStandardShifts As ShiftRequest.ManageStandardShifts
    Friend WithEvents EditWorkDayDefinitions As ShiftRequest.EditWorkDayDefinitions
End Class
