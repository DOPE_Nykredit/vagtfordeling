﻿Public Class ManageShiftRequestsForm

    Private Sub ManageShiftRequestsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text += " - " & My.Application.SelectedCampaign.Name()
    End Sub

    Private Sub cmdSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSettings.Click
        Me.ReviewShiftRequests.ClearSelections()

        Dim settings As New SettingsForm
        settings.ShowDialog()

        Me.ReviewShiftRequests.UpdateRequestData()
    End Sub
    Private Sub cmdEditShiftRequests_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditShiftRequests.Click
        Me.ReviewShiftRequests.ClearSelections()

        Dim edit As New EditShiftRequestsFrom
        edit.ShowDialog()

        Me.ReviewShiftRequests.UpdateRequestData()
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
    Private Sub ManageShiftRequestsForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub tabManageShifts_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabManageShifts.Enter
        If System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Runtime Then
            Me.ShiftManagementGrid.ClearSelection()
            Me.ShiftManagementGrid.Enabled = False
            Me.ShiftManagementGrid.RefreshShiftCoverage()
            Me.ShiftManagementGrid.Enabled = True
        End If
    End Sub

    Private Sub ShiftManagementGrid_IntervalSelected(ByVal day As ShiftDay, ByVal interval As ShiftTimeSpan, ByVal requiredAgents As Integer) Handles ShiftManagementGrid.IntervalSelected
        Me.grpDetails.SuspendLayout()
        Me.txtDetailsDay.Text = day.ToString
        Me.txtDetailsInterval.Text = interval.ToString

        Me.cmdDeleteAssignedShift.Enabled = False
        Me.AssignShift.Clear()

        Me.lstShiftRequests.ShowRequestedShifts(day, interval)
        Me.lstAssignedShifts.ShowAssignedShifts(day, interval)
    End Sub

    Private Sub AssignShift_NewShiftCreated(ByVal shift As AssignedShift) Handles AssignShift.NewShiftAssigned
        My.Application.AssignedShiftsInSelectedCampaign.Add(shift)

        Me.ShiftManagementGrid.RefreshShiftCoverage()
        Me.lstShiftRequests.ClearSelection()
        Me.lstShiftRequests.UpdateItems()
        Me.lstAssignedShifts.UpdateList()
    End Sub
    Private Sub AssignShift_AssignedShiftUpdated(ByVal assignedShift As AssignedShift) Handles AssignShift.AssignedShiftUpdated
        Me.ShiftManagementGrid.RefreshShiftCoverage()
        Me.lstAssignedShifts.UpdateList()
        Me.lstShiftRequests.UpdateItems()

        Me.cmdDeleteAssignedShift.Enabled = False
    End Sub

    Private Sub lstShiftRequests_RequestedShiftSelected(ByVal requestedShift As RequestedShift) Handles lstShiftRequests.RequestedShiftSelected
        Me.AssignShift.DisplayRequestedShift(requestedShift)

        Me.lstAssignedShifts.ClearSelection()

        Me.cmdDeleteAssignedShift.Enabled = False
    End Sub
    Private Sub lstAssignedShifts_AssignedShiftSelected(ByVal assignedShift As AssignedShift) Handles lstAssignedShifts.AssignedShiftSelected
        Me.AssignShift.DisplayAssignedShift(assignedShift)

        Me.lstShiftRequests.ClearSelection()

        Me.cmdDeleteAssignedShift.Enabled = True
    End Sub

    Private Sub tabReviewShiftRequests_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabReviewShiftRequests.Leave
        Me.ReviewShiftRequests.ClearSelections()
    End Sub

    Private Sub cmdDeleteAssignedShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteAssignedShift.Click
        Try
            Dim conn As New ShiftRequestConnection

            Dim shiftToDelete As AssignedShift = Me.lstAssignedShifts.SelectedShift

            My.Application.AssignedShiftsInSelectedCampaign.Remove(shiftToDelete)

            shiftToDelete.delete(conn)

            Me.ShiftManagementGrid.RefreshShiftCoverage()
            Me.lstShiftRequests.UpdateItems()
        Catch ex As Exception
            MsgBox("Der skete en fejl ved sletning af tildelt vagt. Det anbefales at du genstarter programmet." + vbCrLf + vbCrLf +
                   "Detaljer: " + ex.Message + vbCrLf + ex.StackTrace, vbOKOnly, "Fejl")
        Finally
            Me.cmdDeleteAssignedShift.Enabled = False
        End Try
    End Sub
End Class