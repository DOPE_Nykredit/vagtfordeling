﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManageShiftRequestsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManageShiftRequestsForm))
        Me.MainTabControl = New System.Windows.Forms.TabControl()
        Me.tabReviewShiftRequests = New System.Windows.Forms.TabPage()
        Me.ReviewShiftRequests = New ShiftRequest.ReviewShiftRequests()
        Me.tabManageShifts = New System.Windows.Forms.TabPage()
        Me.grpDetails = New System.Windows.Forms.GroupBox()
        Me.cmdDeleteAssignedShift = New System.Windows.Forms.Button()
        Me.lstAssignedShifts = New ShiftRequest.AssignedShiftListView()
        Me.lstShiftRequests = New ShiftRequest.ShiftRequestsListView()
        Me.AssignShift = New ShiftRequest.AssignShift()
        Me.lblDetailsRequestedShifts = New System.Windows.Forms.Label()
        Me.lblDetailsAssignedShifts = New System.Windows.Forms.Label()
        Me.txtDetailsInterval = New System.Windows.Forms.Label()
        Me.lblDetailsInterval = New System.Windows.Forms.Label()
        Me.lblDetailsDay = New System.Windows.Forms.Label()
        Me.txtDetailsDay = New System.Windows.Forms.Label()
        Me.ShiftManagementGrid = New ShiftRequest.ShiftManagementGrid()
        Me.cmdSettings = New System.Windows.Forms.Button()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.cmdEditShiftRequests = New System.Windows.Forms.Button()
        Me.MainTabControl.SuspendLayout()
        Me.tabReviewShiftRequests.SuspendLayout()
        Me.tabManageShifts.SuspendLayout()
        Me.grpDetails.SuspendLayout()
        CType(Me.ShiftManagementGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainTabControl
        '
        Me.MainTabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainTabControl.Controls.Add(Me.tabReviewShiftRequests)
        Me.MainTabControl.Controls.Add(Me.tabManageShifts)
        Me.MainTabControl.ItemSize = New System.Drawing.Size(200, 20)
        Me.MainTabControl.Location = New System.Drawing.Point(0, 0)
        Me.MainTabControl.Name = "MainTabControl"
        Me.MainTabControl.SelectedIndex = 0
        Me.MainTabControl.Size = New System.Drawing.Size(1042, 716)
        Me.MainTabControl.TabIndex = 2
        '
        'tabReviewShiftRequests
        '
        Me.tabReviewShiftRequests.Controls.Add(Me.ReviewShiftRequests)
        Me.tabReviewShiftRequests.Location = New System.Drawing.Point(4, 24)
        Me.tabReviewShiftRequests.Name = "tabReviewShiftRequests"
        Me.tabReviewShiftRequests.Padding = New System.Windows.Forms.Padding(3)
        Me.tabReviewShiftRequests.Size = New System.Drawing.Size(1034, 688)
        Me.tabReviewShiftRequests.TabIndex = 0
        Me.tabReviewShiftRequests.Text = "Gennemse ønsker"
        Me.tabReviewShiftRequests.UseVisualStyleBackColor = True
        '
        'ReviewShiftRequests
        '
        Me.ReviewShiftRequests.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ReviewShiftRequests.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ReviewShiftRequests.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReviewShiftRequests.Location = New System.Drawing.Point(3, 3)
        Me.ReviewShiftRequests.MinimumSize = New System.Drawing.Size(725, 540)
        Me.ReviewShiftRequests.Name = "ReviewShiftRequests"
        Me.ReviewShiftRequests.Size = New System.Drawing.Size(725, 683)
        Me.ReviewShiftRequests.TabIndex = 0
        '
        'tabManageShifts
        '
        Me.tabManageShifts.Controls.Add(Me.grpDetails)
        Me.tabManageShifts.Controls.Add(Me.ShiftManagementGrid)
        Me.tabManageShifts.Location = New System.Drawing.Point(4, 24)
        Me.tabManageShifts.Name = "tabManageShifts"
        Me.tabManageShifts.Padding = New System.Windows.Forms.Padding(3)
        Me.tabManageShifts.Size = New System.Drawing.Size(1034, 688)
        Me.tabManageShifts.TabIndex = 1
        Me.tabManageShifts.Text = "Overblik"
        Me.tabManageShifts.UseVisualStyleBackColor = True
        '
        'grpDetails
        '
        Me.grpDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpDetails.Controls.Add(Me.cmdDeleteAssignedShift)
        Me.grpDetails.Controls.Add(Me.lstAssignedShifts)
        Me.grpDetails.Controls.Add(Me.lstShiftRequests)
        Me.grpDetails.Controls.Add(Me.AssignShift)
        Me.grpDetails.Controls.Add(Me.lblDetailsRequestedShifts)
        Me.grpDetails.Controls.Add(Me.lblDetailsAssignedShifts)
        Me.grpDetails.Controls.Add(Me.txtDetailsInterval)
        Me.grpDetails.Controls.Add(Me.lblDetailsInterval)
        Me.grpDetails.Controls.Add(Me.lblDetailsDay)
        Me.grpDetails.Controls.Add(Me.txtDetailsDay)
        Me.grpDetails.Location = New System.Drawing.Point(646, 6)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(375, 680)
        Me.grpDetails.TabIndex = 1
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Detaljer:"
        '
        'cmdDeleteAssignedShift
        '
        Me.cmdDeleteAssignedShift.Location = New System.Drawing.Point(224, 283)
        Me.cmdDeleteAssignedShift.Name = "cmdDeleteAssignedShift"
        Me.cmdDeleteAssignedShift.Size = New System.Drawing.Size(100, 23)
        Me.cmdDeleteAssignedShift.TabIndex = 13
        Me.cmdDeleteAssignedShift.Text = "Slet vagt"
        Me.cmdDeleteAssignedShift.UseVisualStyleBackColor = True
        '
        'lstAssignedShifts
        '
        Me.lstAssignedShifts.FullRowSelect = True
        Me.lstAssignedShifts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstAssignedShifts.HideSelection = False
        Me.lstAssignedShifts.Location = New System.Drawing.Point(50, 102)
        Me.lstAssignedShifts.MultiSelect = False
        Me.lstAssignedShifts.Name = "lstAssignedShifts"
        Me.lstAssignedShifts.ShowGroups = False
        Me.lstAssignedShifts.Size = New System.Drawing.Size(274, 175)
        Me.lstAssignedShifts.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstAssignedShifts.TabIndex = 12
        Me.lstAssignedShifts.UseCompatibleStateImageBehavior = False
        Me.lstAssignedShifts.View = System.Windows.Forms.View.Details
        '
        'lstShiftRequests
        '
        Me.lstShiftRequests.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstShiftRequests.CheckBoxes = True
        Me.lstShiftRequests.FullRowSelect = True
        Me.lstShiftRequests.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstShiftRequests.HideSelection = False
        Me.lstShiftRequests.Location = New System.Drawing.Point(8, 339)
        Me.lstShiftRequests.MultiSelect = False
        Me.lstShiftRequests.Name = "lstShiftRequests"
        Me.lstShiftRequests.ShowGroups = False
        Me.lstShiftRequests.Size = New System.Drawing.Size(359, 181)
        Me.lstShiftRequests.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstShiftRequests.TabIndex = 11
        Me.lstShiftRequests.UseCompatibleStateImageBehavior = False
        Me.lstShiftRequests.View = System.Windows.Forms.View.Details
        '
        'AssignShift
        '
        Me.AssignShift.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.AssignShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AssignShift.Location = New System.Drawing.Point(12, 526)
        Me.AssignShift.Name = "AssignShift"
        Me.AssignShift.Size = New System.Drawing.Size(350, 155)
        Me.AssignShift.TabIndex = 10
        '
        'lblDetailsRequestedShifts
        '
        Me.lblDetailsRequestedShifts.AutoSize = True
        Me.lblDetailsRequestedShifts.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetailsRequestedShifts.Location = New System.Drawing.Point(7, 322)
        Me.lblDetailsRequestedShifts.Name = "lblDetailsRequestedShifts"
        Me.lblDetailsRequestedShifts.Size = New System.Drawing.Size(183, 14)
        Me.lblDetailsRequestedShifts.TabIndex = 7
        Me.lblDetailsRequestedShifts.Text = "Har ønsket vagt i intervallet:"
        '
        'lblDetailsAssignedShifts
        '
        Me.lblDetailsAssignedShifts.AutoSize = True
        Me.lblDetailsAssignedShifts.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetailsAssignedShifts.Location = New System.Drawing.Point(7, 85)
        Me.lblDetailsAssignedShifts.Name = "lblDetailsAssignedShifts"
        Me.lblDetailsAssignedShifts.Size = New System.Drawing.Size(99, 14)
        Me.lblDetailsAssignedShifts.TabIndex = 6
        Me.lblDetailsAssignedShifts.Text = "Tildelte vagter:"
        '
        'txtDetailsInterval
        '
        Me.txtDetailsInterval.AutoSize = True
        Me.txtDetailsInterval.Location = New System.Drawing.Point(102, 57)
        Me.txtDetailsInterval.Name = "txtDetailsInterval"
        Me.txtDetailsInterval.Size = New System.Drawing.Size(11, 14)
        Me.txtDetailsInterval.TabIndex = 3
        Me.txtDetailsInterval.Text = "-"
        '
        'lblDetailsInterval
        '
        Me.lblDetailsInterval.AutoSize = True
        Me.lblDetailsInterval.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetailsInterval.Location = New System.Drawing.Point(7, 57)
        Me.lblDetailsInterval.Name = "lblDetailsInterval"
        Me.lblDetailsInterval.Size = New System.Drawing.Size(59, 14)
        Me.lblDetailsInterval.TabIndex = 1
        Me.lblDetailsInterval.Text = "Interval:"
        '
        'lblDetailsDay
        '
        Me.lblDetailsDay.AutoSize = True
        Me.lblDetailsDay.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetailsDay.Location = New System.Drawing.Point(7, 33)
        Me.lblDetailsDay.Name = "lblDetailsDay"
        Me.lblDetailsDay.Size = New System.Drawing.Size(35, 14)
        Me.lblDetailsDay.TabIndex = 0
        Me.lblDetailsDay.Text = "Dag:"
        '
        'txtDetailsDay
        '
        Me.txtDetailsDay.AutoSize = True
        Me.txtDetailsDay.Location = New System.Drawing.Point(102, 33)
        Me.txtDetailsDay.Name = "txtDetailsDay"
        Me.txtDetailsDay.Size = New System.Drawing.Size(11, 14)
        Me.txtDetailsDay.TabIndex = 2
        Me.txtDetailsDay.Text = "-"
        '
        'ShiftManagementGrid
        '
        Me.ShiftManagementGrid.AllowUserToAddRows = False
        Me.ShiftManagementGrid.AllowUserToDeleteRows = False
        Me.ShiftManagementGrid.AllowUserToResizeColumns = False
        Me.ShiftManagementGrid.AllowUserToResizeRows = False
        Me.ShiftManagementGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ShiftManagementGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.ShiftManagementGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ShiftManagementGrid.DefaultCellStyle = DataGridViewCellStyle1
        Me.ShiftManagementGrid.Location = New System.Drawing.Point(6, 6)
        Me.ShiftManagementGrid.MultiSelect = False
        Me.ShiftManagementGrid.Name = "ShiftManagementGrid"
        Me.ShiftManagementGrid.ReadOnly = True
        Me.ShiftManagementGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.ShiftManagementGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.ShiftManagementGrid.ShowTotalTimeOnDay = True
        Me.ShiftManagementGrid.Size = New System.Drawing.Size(613, 680)
        Me.ShiftManagementGrid.TabIndex = 0
        '
        'cmdSettings
        '
        Me.cmdSettings.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdSettings.Location = New System.Drawing.Point(7, 716)
        Me.cmdSettings.Name = "cmdSettings"
        Me.cmdSettings.Size = New System.Drawing.Size(100, 23)
        Me.cmdSettings.TabIndex = 3
        Me.cmdSettings.Text = "Indstillinger"
        Me.cmdSettings.UseVisualStyleBackColor = True
        '
        'cmdClose
        '
        Me.cmdClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdClose.Location = New System.Drawing.Point(927, 839)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(100, 23)
        Me.cmdClose.TabIndex = 4
        Me.cmdClose.Text = "Luk"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdEditShiftRequests
        '
        Me.cmdEditShiftRequests.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdEditShiftRequests.Location = New System.Drawing.Point(113, 716)
        Me.cmdEditShiftRequests.Name = "cmdEditShiftRequests"
        Me.cmdEditShiftRequests.Size = New System.Drawing.Size(100, 23)
        Me.cmdEditShiftRequests.TabIndex = 5
        Me.cmdEditShiftRequests.Text = "Rediger ønsker"
        Me.cmdEditShiftRequests.UseVisualStyleBackColor = True
        '
        'ManageShiftRequestsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1042, 742)
        Me.Controls.Add(Me.cmdEditShiftRequests)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSettings)
        Me.Controls.Add(Me.MainTabControl)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1050, 706)
        Me.Name = "ManageShiftRequestsForm"
        Me.Text = "Vagt fordeling"
        Me.MainTabControl.ResumeLayout(False)
        Me.tabReviewShiftRequests.ResumeLayout(False)
        Me.tabManageShifts.ResumeLayout(False)
        Me.grpDetails.ResumeLayout(False)
        Me.grpDetails.PerformLayout()
        CType(Me.ShiftManagementGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainTabControl As System.Windows.Forms.TabControl
    Friend WithEvents tabReviewShiftRequests As System.Windows.Forms.TabPage
    Friend WithEvents tabManageShifts As System.Windows.Forms.TabPage
    Friend WithEvents ReviewShiftRequests As ShiftRequest.ReviewShiftRequests
    Friend WithEvents cmdSettings As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents ShiftManagementGrid As ShiftRequest.ShiftManagementGrid
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Friend WithEvents lblDetailsDay As System.Windows.Forms.Label
    Friend WithEvents lblDetailsInterval As System.Windows.Forms.Label
    Friend WithEvents txtDetailsDay As System.Windows.Forms.Label
    Friend WithEvents txtDetailsInterval As System.Windows.Forms.Label
    Friend WithEvents lblDetailsAssignedShifts As System.Windows.Forms.Label
    Friend WithEvents lblDetailsRequestedShifts As System.Windows.Forms.Label
    Friend WithEvents cmdEditShiftRequests As System.Windows.Forms.Button
    Friend WithEvents AssignShift As ShiftRequest.AssignShift
    Friend WithEvents lstShiftRequests As ShiftRequest.ShiftRequestsListView
    Friend WithEvents lstAssignedShifts As ShiftRequest.AssignedShiftListView
    Friend WithEvents cmdDeleteAssignedShift As System.Windows.Forms.Button
End Class
