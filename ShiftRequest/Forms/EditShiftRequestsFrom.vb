﻿Public Class EditShiftRequestsFrom

    Private SelectedAgent As Agent

    Private Sub EditShiftRequestsFromNew_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.cboAgents.UpdateAgents()

        If Me.cboAgents.Items.Count > 0 Then Me.cboAgents.SelectedIndex = 0

        Dim campaignSelectionType = My.Application.SelectedCampaign.SelectionType

        Me.primaryShiftSelection.DisplayDaysMode = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, ShiftSelection.DisplayDays.StandardShifts, ShiftSelection.DisplayDays.FreeInterval)
        Me.grpPrimaryRequests.Text = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, "Vagter", "1. Prioritet")
        Me.grpSecondaryRequests.Text = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, "Fleksibilitet", "Rådighedsrum")

        Dim workDayDefinitions = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
        workDayDefinitions.Sort()

        For i = 0 To workDayDefinitions.Count - 1
            Me.dayDescriptionLayoutPanel.RowStyles.Add(New RowStyle(SizeType.AutoSize))

            Me.dayDescriptionLayoutPanel.Controls.Add(New Label With {.Text = workDayDefinitions(i).ShiftDay.DayDescription,
                                                                      .Margin = New Padding(3, 10, 3, 9),
                                                                      .Font = New Font(Me.Font, FontStyle.Bold)},
                                                      0,
                                                      i)
        Next
    End Sub
    Private Sub cboAgents_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAgents.SelectedIndexChanged
        Me.cmdGetAgentShiftRequests.Enabled = (Me.cboAgents.SelectedAgent IsNot Nothing)
    End Sub

    Private Sub cmdGetAgentShiftRequests_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetAgentShiftRequests.Click
        Me.ResetForm()

        Me.primaryShiftSelection.Enabled = True
        Me.secondaryShiftSelection.Enabled = True

        Me.cmdSave.Enabled = True

        Me.SelectedAgent = Me.cboAgents.SelectedAgent
        Me.Text = "Rediger vagønsker for " & Me.SelectedAgent.ToString

        Dim shiftRequests = My.Application.ShiftRequestsInSelectedCampaign.FindAll(Function(r) r.AgentId = Me.SelectedAgent.Id)

        For Each r In shiftRequests
            If r.PrimaryRequest IsNot Nothing Then
                If My.Application.SelectedCampaign.SelectionType = Campaign.ShiftSelectionType.StandardShifts And r.StandardShift IsNot Nothing Then
                    Me.primaryShiftSelection.SetStandardShift(r.Day, r.StandardShift)
                Else
                    Me.primaryShiftSelection.SetShiftTime(r.Day, r.PrimaryRequest)
                End If
            End If

            If r.SecondaryRequest IsNot Nothing Then
                Me.secondaryShiftSelection.SetShiftTime(r.Day, r.SecondaryRequest)
            End If
        Next
    End Sub

    Private Sub ResetForm()
        Me.primaryShiftSelection.ClearAllComboBoxes()
        Me.secondaryShiftSelection.ClearAllComboBoxes()
    End Sub

    Private Sub primaryShiftSelection_TimeSelectedOnDay(ByVal shiftDay As ShiftDay, ByVal time As ShiftTime, ByVal shiftTimeStyle As ShiftTimeComboBox.ShiftTimeStyles) Handles primaryShiftSelection.TimeSelectedOnDay
        Me.secondaryShiftSelection.SetShiftTimeConstraints(shiftDay,
                                                           IIf(shiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift, Nothing, time),
                                                           IIf(shiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift, time, Nothing),
                                                           shiftTimeStyle)
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim workDayDefinitions = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
        Dim shiftRequests = My.Application.ShiftRequestsInSelectedCampaign.FindAll(Function(r) r.AgentId = Me.SelectedAgent.Id)

        Dim conn As New ShiftRequestConnection

        If My.Application.SelectedCampaign.SelectionType = Campaign.ShiftSelectionType.StandardShifts Then
            For Each d In workDayDefinitions
                Dim day As ShiftDay.Days = d.ShiftDay.Day

                Dim standardShiftRequest As StandardShift = Me.primaryShiftSelection.getSelectedStandardShift(d.ShiftDay)
                Dim secondaryRequest As ShiftTimeSpan = Me.secondaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)

                Dim existingRequest = shiftRequests.Find(Function(r) r.Day.Day = day)

                If standardShiftRequest IsNot Nothing Or secondaryRequest IsNot Nothing Then
                    If existingRequest IsNot Nothing Then
                        existingRequest.StandardShift = standardShiftRequest
                        existingRequest.SecondaryRequest = secondaryRequest

                        existingRequest.save(conn)
                    Else
                        Dim newRequest As New RequestedShift(standardShiftRequest,
                                                             secondaryRequest,
                                                             Me.SelectedAgent.Id,
                                                             d.ShiftDay)

                        newRequest.save(conn)

                        My.Application.ShiftRequestsInSelectedCampaign.Add(newRequest)
                    End If
                Else
                    If existingRequest IsNot Nothing Then
                        existingRequest.Delete(conn)

                        My.Application.ShiftRequestsInSelectedCampaign.Remove(existingRequest)
                    End If
                End If
            Next
        Else
            For Each d In workDayDefinitions
                Dim day As ShiftDay.Days = d.ShiftDay.Day

                Dim primaryRequest As ShiftTimeSpan = Me.primaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)
                Dim secondaryRequest As ShiftTimeSpan = Me.secondaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)

                Dim existingRequest = shiftRequests.Find(Function(r) r.Day.Day = day)

                If primaryRequest IsNot Nothing Or secondaryRequest IsNot Nothing Then
                    If existingRequest IsNot Nothing Then
                        existingRequest.PrimaryRequest = primaryRequest
                        existingRequest.SecondaryRequest = secondaryRequest

                        existingRequest.save(conn)
                    Else
                        Dim newRequest As New RequestedShift(primaryRequest,
                                                             secondaryRequest,
                                                             Me.SelectedAgent.Id,
                                                             d.ShiftDay)

                        newRequest.save(conn)

                        My.Application.ShiftRequestsInSelectedCampaign.Add(newRequest)
                    End If
                ElseIf existingRequest IsNot Nothing Then
                    existingRequest.Delete(conn)

                    My.Application.ShiftRequestsInSelectedCampaign.Remove(existingRequest)
                End If
            Next
        End If


    End Sub
End Class