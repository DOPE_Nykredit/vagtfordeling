﻿Public Class SelectCampaignForm

    Private _selectedCampaign As Campaign

    Private allTeams As List(Of Team)

    Private Property SelectedCampaign As Campaign
        Set(ByVal value As Campaign)
            _selectedCampaign = value
            RaiseEvent SelectedCampaignChanged()
        End Set
        Get
            Return _selectedCampaign
        End Get
    End Property

    Public Event SelectedCampaignChanged()

    Private Sub SelectCampaignForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboCampaignStart.Value = Now
        Me.cboCampaignEnd.Value = Now
        Me.cboStartOfSchedule.Value = Now.AddDays(1)

        Me.cboShiftSelectionType.SelectedIndex = 0

        Me.cboSelectCampaign.LoadCampaigns()

        Dim conn As New ShiftRequestConnection

        allTeams = Team.GetAllTeams(conn)

        Me.SelectCampaignForm_SelectedCampaignChanged()
    End Sub

    Private Sub cboSelectCampaign_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSelectCampaign.SelectedIndexChanged
        If Me.cboSelectCampaign.SelectedIndex = -1 Then
            Me.SelectedCampaign = Nothing
        Else
            Me.SelectedCampaign = Me.cboSelectCampaign.SelectedCampaign
        End If
    End Sub

    Private Sub SelectCampaignForm_SelectedCampaignChanged() Handles Me.SelectedCampaignChanged
        Me.lstAssignedTeams.Items.Clear()

        If Me.SelectedCampaign Is Nothing Then
            Me.txtCampaignName.Text = ""
            Me.cboCampaignStart.Value = Now
            Me.cboCampaignEnd.Value = Now
            Me.cboStartOfSchedule.Value = Now.AddDays(1)

            Me.cboShiftSelectionType.SelectedIndex = 0

            Me.lstAssignedTeams.Items.AddRange(Me.allTeams.ToArray)
        Else
            Me.txtCampaignName.Text = Me.SelectedCampaign.Name

            Me.cboCampaignStart.Value = Me.SelectedCampaign.FromDate
            Me.cboCampaignEnd.Value = Me.SelectedCampaign.ToDate

            Me.cboStartOfSchedule.Value = Me.SelectedCampaign.StartOfSchedule

            Me.cboShiftSelectionType.SelectedIndex = Me.SelectedCampaign.SelectionType

            Me.SelectedCampaign.AssignedTeams.ForEach(Sub(t) lstAssignedTeams.SetItemChecked(lstAssignedTeams.Items.Add(t), True))
        End If

        Me.txtCampaignName.ReadOnly = (Me.SelectedCampaign IsNot Nothing)
        Me.cboCampaignStart.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.cboCampaignEnd.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.cboStartOfSchedule.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.cboShiftSelectionType.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.lstAssignedTeams.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.cmdSaveCampaign.Enabled = (Me.SelectedCampaign Is Nothing)
        Me.cmdContinue.Enabled = (Me.SelectedCampaign IsNot Nothing)
    End Sub

    Private Sub cmdGem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveCampaign.Click
        If Not Me.cboCampaignEnd.Value > Me.cboCampaignStart.Value Then
            MsgBox("Startdato for kampagne skal være før slutdato.", MsgBoxStyle.OkOnly, "Bemærk")
            Exit Sub
        End If

        If Me.SelectedCampaign Is Nothing Then

            If Me.txtCampaignName.Text = "" Then
                MsgBox("Kampagnen skal have et navn.", vbOKOnly, "Bemærk")
                Exit Sub
            ElseIf Me.lstAssignedTeams.CheckedItems.Count < 1 Then
                MsgBox("Der skal minimum være et team tilknyttet kampagnen.", MsgBoxStyle.OkOnly, "Bemærk")
                Exit Sub
            End If

            Dim c As New Campaign(Me.cboCampaignStart.Value, Me.cboCampaignEnd.Value, Me.txtCampaignName.Text, Me.cboStartOfSchedule.Value, Me.cboShiftSelectionType.SelectedIndex)

            For Each t In Me.lstAssignedTeams.CheckedItems
                If TypeOf t Is Team Then c.AssignedTeams.Add(t)
            Next

            Dim conn As New ShiftRequestConnection

            c.Save(conn)

            Me.cboSelectCampaign.AddCampaign(c, True)
        Else
            Me.SelectedCampaign.Name = Me.txtCampaignName.Text

            Dim conn As New ShiftRequestConnection

            Me.SelectedCampaign.Save(conn)
        End If
    End Sub

    Private Sub cmdNewCampaign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNewCampaign.Click
        Me.cboSelectCampaign.SelectedIndex = -1
    End Sub

    Private Sub cmdContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdContinue.Click
        My.Application.SelectedCampaign = Me.SelectedCampaign

        Dim conn As New ShiftRequestConnection

        My.Application.LoadCampaignData(conn)

        My.Forms.ManageShiftRequestsForm.Show()
        Me.Close()
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub cboCampaignEnd_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCampaignEnd.ValueChanged
        Me.cboStartOfSchedule.MinDate = Me.cboCampaignEnd.Value.AddDays(1)
    End Sub
End Class