﻿Public Class SettingsForm

    Private Sub shiftRequirementsTab_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabShiftRequirements.Enter
        Me.ShiftRequirementsGrid.LoadShiftRequirements()
        Me.ShiftRequirementsGrid.AutoFill = ShiftRequirementsGrid.AutoFills.Down
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.ShiftRequirementsGrid.SaveShiftRequirements()
    End Sub

    Private Sub SettingsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.EditWorkDayDefinitions.SetupControls()

        If My.Application.SelectedCampaign.SelectionType = Campaign.ShiftSelectionType.AgentDefined Then Me.SettingsTabs.TabPages.Remove(Me.tabStandardShift)
    End Sub

    Private Sub rdbAutoFillOff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAutoFillOff.CheckedChanged,
                                                                                                                  rdbAutoFillDown.CheckedChanged,
                                                                                                                  rdbAutoFillUp.CheckedChanged

        If Me.rdbAutoFillOff.Checked Then
            Me.ShiftRequirementsGrid.AutoFill = ShiftRequirementsGrid.AutoFills.None
        ElseIf Me.rdbAutoFillDown.Checked Then
            Me.ShiftRequirementsGrid.AutoFill = ShiftRequirementsGrid.AutoFills.Down
        ElseIf Me.rdbAutoFillUp.Checked Then
            Me.ShiftRequirementsGrid.AutoFill = ShiftRequirementsGrid.AutoFills.Up
        End If
    End Sub

    Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub

    Private Sub tabStandardShift_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabStandardShift.Enter
        Me.ManageStandardShifts.SetupControls()
    End Sub

    Private Sub EditWorkDayDefinitions_WorkDayDefinitionsSaved() Handles EditWorkDayDefinitions.WorkDayDefinitionsSaved
        Me.ShiftRequirementsGrid.LoadShiftRequirements()
    End Sub
End Class