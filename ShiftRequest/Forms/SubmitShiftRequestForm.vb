﻿Public Class SubmitShiftRequestForm

    Dim standardShiftDescription As String = "Under vagter kan du angive de ugentlige vagter du ønsker. Vælg standardvagt i tidsrummet enten morgen eftermiddag aften eller weekend." + vbCrLf + vbCrLf +
                                             "Under fleksibilitet kan du angive i hvilket tidsrum du vil kunne arbejde (dvs. den tid vi kan planlægge vagter til dig i)"
    Dim freeInvervalDescription As String = "Under 1. Prioritet kan du angive de ugentlige vagter du ønsker." + vbCrLf + vbCrLf +
                                            "Under rådighedsrum kan du angive i hvilket tidsrum du vil kunne arbejde (dvs. den tid vi kan planlægge vagter til dig i)"

    Private Sub SubmitShiftRequestFormNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If My.Application.Agent Is Nothing Then
            MsgBox("Du er ikke oprettet som bruger i Nykredit Servicecenter. Kontakt HRØS hvis du er ansat men stadig får denne besked.", MsgBoxStyle.OkOnly, "Bemærk")

            Me.Close()
        End If

        Me.Text += " - " & My.Application.Agent.Initials.ToUpper

        Dim campaignSelectionType = My.Application.SelectedCampaign.SelectionType

        Me.txtDescription.Text = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, Me.standardShiftDescription, Me.freeInvervalDescription)

        Me.grpPrimaryRequests.Text = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, "Vagter", "1. Prioritet")
        Me.grpSecondaryRequests.Text = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, "Fleksibilitet", "Rådighedsrum")

        Me.primaryShiftSelection.DisplayDaysMode = IIf(campaignSelectionType = Campaign.ShiftSelectionType.StandardShifts, ShiftSelection.DisplayDays.StandardShifts, ShiftSelection.DisplayDays.FreeInterval)

        Dim workDayDefinitions = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
        workDayDefinitions.Sort()

        For i = 0 To workDayDefinitions.Count - 1
            Me.dayDescriptionLayoutPanel.RowStyles.Add(New RowStyle(SizeType.AutoSize))

            Me.dayDescriptionLayoutPanel.Controls.Add(New Label With {.Text = workDayDefinitions(i).ShiftDay.DayDescription,
                                                                      .Margin = New Padding(3, 10, 3, 9),
                                                                      .Font = New Font(Me.Font, FontStyle.Bold)},
                                                      0,
                                                      i)
        Next
    End Sub

    Private Sub primaryShiftSelection_TimeSelectedOnDay(ByVal shiftDay As ShiftDay, ByVal time As ShiftTime, ByVal shiftTimeStyle As ShiftTimeComboBox.ShiftTimeStyles) Handles primaryShiftSelection.TimeSelectedOnDay
        Me.secondaryShiftSelection.SetShiftTimeConstraints(shiftDay,
                                                           IIf(shiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift, Nothing, time),
                                                           IIf(shiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift, time, Nothing),
                                                           shiftTimeStyle)
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Dim ans = MsgBox("Dine ønsker er ikke indsendt. Er du sikker på at du vil lukke?", MsgBoxStyle.YesNo, "Bemærk")

        If ans = MsgBoxResult.Yes Then Me.Close()
    End Sub

    Private Sub cmdSendRequests_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSendRequests.Click
        Dim ans = MsgBox("Ønsker du at indsende de angivne vagtønsker?", MsgBoxStyle.YesNo, "Bekræft indsending")

        If ans = MsgBoxResult.No Then Exit Sub

        Dim conn As New ShiftRequestConnection

        If My.Application.SelectedCampaign.SelectionType = Campaign.ShiftSelectionType.StandardShifts Then
            For Each d In My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
                Dim standardShiftRequest As StandardShift = Me.primaryShiftSelection.getSelectedStandardShift(d.ShiftDay)
                Dim secondaryRequest As ShiftTimeSpan = Me.secondaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)

                If standardShiftRequest IsNot Nothing Or secondaryRequest IsNot Nothing Then
                    Dim newShiftRequest As New RequestedShift(standardShiftRequest,
                                                              secondaryRequest,
                                                              My.Application.Agent.Id,
                                                              d.ShiftDay)

                    newShiftRequest.save(conn)
                End If
            Next
        Else
            For Each d In My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
                Dim primaryRequest As ShiftTimeSpan = Me.primaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)
                Dim secondaryRequest As ShiftTimeSpan = Me.secondaryShiftSelection.getSelectedShiftTimeSpan(d.ShiftDay)

                If primaryRequest IsNot Nothing Or secondaryRequest IsNot Nothing Then
                    Dim newShiftRequest As New RequestedShift(primaryRequest,
                                                              secondaryRequest,
                                                              My.Application.Agent.Id,
                                                              d.ShiftDay)

                    newShiftRequest.save(conn)
                End If
            Next
        End If

        MsgBox("Dine ønsker er nu indsendt.", vbOKOnly, "Tak for dit svar")

        Me.Close()
    End Sub
End Class