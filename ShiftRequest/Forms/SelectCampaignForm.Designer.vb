﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectCampaignForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SelectCampaignForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpCampaignDetails = New System.Windows.Forms.GroupBox()
        Me.cboShiftSelectionType = New System.Windows.Forms.ComboBox()
        Me.lblShiftSelectionType = New System.Windows.Forms.Label()
        Me.cboStartOfSchedule = New System.Windows.Forms.DateTimePicker()
        Me.lblStartOfSchedule = New System.Windows.Forms.Label()
        Me.cmdSaveCampaign = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lstAssignedTeams = New System.Windows.Forms.CheckedListBox()
        Me.cboCampaignEnd = New System.Windows.Forms.DateTimePicker()
        Me.cboCampaignStart = New System.Windows.Forms.DateTimePicker()
        Me.txtCampaignName = New System.Windows.Forms.TextBox()
        Me.lblCampaignEnd = New System.Windows.Forms.Label()
        Me.lblCampaignStart = New System.Windows.Forms.Label()
        Me.lblCampaignName = New System.Windows.Forms.Label()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.cmdNewCampaign = New System.Windows.Forms.Button()
        Me.cboSelectCampaign = New ShiftRequest.CampaignComboBox()
        Me.grpCampaignDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(48, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Kampagne:"
        '
        'grpCampaignDetails
        '
        Me.grpCampaignDetails.Controls.Add(Me.cboShiftSelectionType)
        Me.grpCampaignDetails.Controls.Add(Me.lblShiftSelectionType)
        Me.grpCampaignDetails.Controls.Add(Me.cboStartOfSchedule)
        Me.grpCampaignDetails.Controls.Add(Me.lblStartOfSchedule)
        Me.grpCampaignDetails.Controls.Add(Me.cmdSaveCampaign)
        Me.grpCampaignDetails.Controls.Add(Me.Label2)
        Me.grpCampaignDetails.Controls.Add(Me.lstAssignedTeams)
        Me.grpCampaignDetails.Controls.Add(Me.cboCampaignEnd)
        Me.grpCampaignDetails.Controls.Add(Me.cboCampaignStart)
        Me.grpCampaignDetails.Controls.Add(Me.txtCampaignName)
        Me.grpCampaignDetails.Controls.Add(Me.lblCampaignEnd)
        Me.grpCampaignDetails.Controls.Add(Me.lblCampaignStart)
        Me.grpCampaignDetails.Controls.Add(Me.lblCampaignName)
        Me.grpCampaignDetails.Location = New System.Drawing.Point(29, 64)
        Me.grpCampaignDetails.Name = "grpCampaignDetails"
        Me.grpCampaignDetails.Size = New System.Drawing.Size(432, 328)
        Me.grpCampaignDetails.TabIndex = 2
        Me.grpCampaignDetails.TabStop = False
        Me.grpCampaignDetails.Text = "Kampagne detaljer"
        '
        'cboShiftSelectionType
        '
        Me.cboShiftSelectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftSelectionType.FormattingEnabled = True
        Me.cboShiftSelectionType.Items.AddRange(New Object() {"Standardvagter", "Valgfrit tidsrum"})
        Me.cboShiftSelectionType.Location = New System.Drawing.Point(180, 151)
        Me.cboShiftSelectionType.Name = "cboShiftSelectionType"
        Me.cboShiftSelectionType.Size = New System.Drawing.Size(151, 22)
        Me.cboShiftSelectionType.TabIndex = 11
        '
        'lblShiftSelectionType
        '
        Me.lblShiftSelectionType.AutoSize = True
        Me.lblShiftSelectionType.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftSelectionType.Location = New System.Drawing.Point(31, 154)
        Me.lblShiftSelectionType.Name = "lblShiftSelectionType"
        Me.lblShiftSelectionType.Size = New System.Drawing.Size(68, 14)
        Me.lblShiftSelectionType.TabIndex = 10
        Me.lblShiftSelectionType.Text = "Vagttype:"
        '
        'cboStartOfSchedule
        '
        Me.cboStartOfSchedule.Location = New System.Drawing.Point(180, 123)
        Me.cboStartOfSchedule.Name = "cboStartOfSchedule"
        Me.cboStartOfSchedule.Size = New System.Drawing.Size(151, 22)
        Me.cboStartOfSchedule.TabIndex = 9
        '
        'lblStartOfSchedule
        '
        Me.lblStartOfSchedule.AutoSize = True
        Me.lblStartOfSchedule.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartOfSchedule.Location = New System.Drawing.Point(31, 127)
        Me.lblStartOfSchedule.Name = "lblStartOfSchedule"
        Me.lblStartOfSchedule.Size = New System.Drawing.Size(95, 14)
        Me.lblStartOfSchedule.TabIndex = 8
        Me.lblStartOfSchedule.Text = "Træder i kraft:"
        '
        'cmdSaveCampaign
        '
        Me.cmdSaveCampaign.Enabled = False
        Me.cmdSaveCampaign.Location = New System.Drawing.Point(337, 297)
        Me.cmdSaveCampaign.Name = "cmdSaveCampaign"
        Me.cmdSaveCampaign.Size = New System.Drawing.Size(75, 23)
        Me.cmdSaveCampaign.TabIndex = 7
        Me.cmdSaveCampaign.Text = "Gem"
        Me.cmdSaveCampaign.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(31, 188)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Teams:"
        '
        'lstAssignedTeams
        '
        Me.lstAssignedTeams.FormattingEnabled = True
        Me.lstAssignedTeams.Location = New System.Drawing.Point(118, 185)
        Me.lstAssignedTeams.Name = "lstAssignedTeams"
        Me.lstAssignedTeams.Size = New System.Drawing.Size(213, 123)
        Me.lstAssignedTeams.Sorted = True
        Me.lstAssignedTeams.TabIndex = 6
        '
        'cboCampaignEnd
        '
        Me.cboCampaignEnd.Location = New System.Drawing.Point(180, 95)
        Me.cboCampaignEnd.Name = "cboCampaignEnd"
        Me.cboCampaignEnd.Size = New System.Drawing.Size(151, 22)
        Me.cboCampaignEnd.TabIndex = 5
        '
        'cboCampaignStart
        '
        Me.cboCampaignStart.Location = New System.Drawing.Point(180, 67)
        Me.cboCampaignStart.Name = "cboCampaignStart"
        Me.cboCampaignStart.Size = New System.Drawing.Size(151, 22)
        Me.cboCampaignStart.TabIndex = 4
        '
        'txtCampaignName
        '
        Me.txtCampaignName.Location = New System.Drawing.Point(101, 36)
        Me.txtCampaignName.Name = "txtCampaignName"
        Me.txtCampaignName.Size = New System.Drawing.Size(230, 22)
        Me.txtCampaignName.TabIndex = 3
        '
        'lblCampaignEnd
        '
        Me.lblCampaignEnd.AutoSize = True
        Me.lblCampaignEnd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCampaignEnd.Location = New System.Drawing.Point(31, 99)
        Me.lblCampaignEnd.Name = "lblCampaignEnd"
        Me.lblCampaignEnd.Size = New System.Drawing.Size(36, 14)
        Me.lblCampaignEnd.TabIndex = 2
        Me.lblCampaignEnd.Text = "Slut:"
        '
        'lblCampaignStart
        '
        Me.lblCampaignStart.AutoSize = True
        Me.lblCampaignStart.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCampaignStart.Location = New System.Drawing.Point(31, 71)
        Me.lblCampaignStart.Name = "lblCampaignStart"
        Me.lblCampaignStart.Size = New System.Drawing.Size(43, 14)
        Me.lblCampaignStart.TabIndex = 1
        Me.lblCampaignStart.Text = "Start:"
        '
        'lblCampaignName
        '
        Me.lblCampaignName.AutoSize = True
        Me.lblCampaignName.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCampaignName.Location = New System.Drawing.Point(31, 39)
        Me.lblCampaignName.Name = "lblCampaignName"
        Me.lblCampaignName.Size = New System.Drawing.Size(41, 14)
        Me.lblCampaignName.TabIndex = 0
        Me.lblCampaignName.Text = "Navn:"
        '
        'cmdContinue
        '
        Me.cmdContinue.Enabled = False
        Me.cmdContinue.Location = New System.Drawing.Point(337, 410)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(120, 23)
        Me.cmdContinue.TabIndex = 3
        Me.cmdContinue.Text = "Fortsæt >>"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'cmdNewCampaign
        '
        Me.cmdNewCampaign.Location = New System.Drawing.Point(401, 26)
        Me.cmdNewCampaign.Name = "cmdNewCampaign"
        Me.cmdNewCampaign.Size = New System.Drawing.Size(56, 23)
        Me.cmdNewCampaign.TabIndex = 4
        Me.cmdNewCampaign.Text = "Ny"
        Me.cmdNewCampaign.UseVisualStyleBackColor = True
        '
        'cboSelectCampaign
        '
        Me.cboSelectCampaign.DisplayMember = "Name"
        Me.cboSelectCampaign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSelectCampaign.FormattingEnabled = True
        Me.cboSelectCampaign.Location = New System.Drawing.Point(136, 27)
        Me.cboSelectCampaign.Name = "cboSelectCampaign"
        Me.cboSelectCampaign.Size = New System.Drawing.Size(259, 22)
        Me.cboSelectCampaign.TabIndex = 0
        Me.cboSelectCampaign.ValueMember = "Id"
        '
        'SelectCampaignForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 438)
        Me.Controls.Add(Me.cmdNewCampaign)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.grpCampaignDetails)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboSelectCampaign)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SelectCampaignForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vælg kampagne"
        Me.grpCampaignDetails.ResumeLayout(False)
        Me.grpCampaignDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboSelectCampaign As ShiftRequest.CampaignComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpCampaignDetails As System.Windows.Forms.GroupBox
    Friend WithEvents cboCampaignEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboCampaignStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCampaignName As System.Windows.Forms.TextBox
    Friend WithEvents lblCampaignEnd As System.Windows.Forms.Label
    Friend WithEvents lblCampaignStart As System.Windows.Forms.Label
    Friend WithEvents lblCampaignName As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lstAssignedTeams As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdSaveCampaign As System.Windows.Forms.Button
    Friend WithEvents cmdContinue As System.Windows.Forms.Button
    Friend WithEvents cmdNewCampaign As System.Windows.Forms.Button
    Friend WithEvents cboStartOfSchedule As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartOfSchedule As System.Windows.Forms.Label
    Friend WithEvents cboShiftSelectionType As System.Windows.Forms.ComboBox
    Friend WithEvents lblShiftSelectionType As System.Windows.Forms.Label
End Class
