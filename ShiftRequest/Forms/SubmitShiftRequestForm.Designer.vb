﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SubmitShiftRequestForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubmitShiftRequestForm))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.dayDescriptionLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.grpPrimaryRequests = New System.Windows.Forms.GroupBox()
        Me.primaryShiftSelection = New ShiftRequest.ShiftSelection()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.cmdSendRequests = New System.Windows.Forms.Button()
        Me.grpSecondaryRequests = New System.Windows.Forms.GroupBox()
        Me.secondaryShiftSelection = New ShiftRequest.ShiftSelection()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.generalLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.grpPrimaryRequests.SuspendLayout()
        Me.grpSecondaryRequests.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.generalLayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(8)
        Me.GroupBox1.Size = New System.Drawing.Size(642, 102)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Vagtønsker"
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Control
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Enabled = False
        Me.txtDescription.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Location = New System.Drawing.Point(8, 21)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(626, 73)
        Me.txtDescription.TabIndex = 0
        Me.txtDescription.Text = resources.GetString("txtDescription.Text")
        '
        'dayDescriptionLayoutPanel
        '
        Me.dayDescriptionLayoutPanel.AutoSize = True
        Me.dayDescriptionLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.dayDescriptionLayoutPanel.ColumnCount = 1
        Me.dayDescriptionLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.dayDescriptionLayoutPanel.Location = New System.Drawing.Point(3, 31)
        Me.dayDescriptionLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 31, 3, 3)
        Me.dayDescriptionLayoutPanel.Name = "dayDescriptionLayoutPanel"
        Me.dayDescriptionLayoutPanel.RowCount = 1
        Me.dayDescriptionLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.dayDescriptionLayoutPanel.Size = New System.Drawing.Size(0, 0)
        Me.dayDescriptionLayoutPanel.TabIndex = 3
        '
        'grpPrimaryRequests
        '
        Me.grpPrimaryRequests.AutoSize = True
        Me.grpPrimaryRequests.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpPrimaryRequests.Controls.Add(Me.primaryShiftSelection)
        Me.grpPrimaryRequests.Location = New System.Drawing.Point(16, 10)
        Me.grpPrimaryRequests.Margin = New System.Windows.Forms.Padding(10)
        Me.grpPrimaryRequests.Name = "grpPrimaryRequests"
        Me.grpPrimaryRequests.Size = New System.Drawing.Size(131, 38)
        Me.grpPrimaryRequests.TabIndex = 4
        Me.grpPrimaryRequests.TabStop = False
        Me.grpPrimaryRequests.Text = "-"
        '
        'primaryShiftSelection
        '
        Me.primaryShiftSelection.AddShiftTimeMarginToComboBoxes = True
        Me.primaryShiftSelection.AutoSize = True
        Me.primaryShiftSelection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.primaryShiftSelection.DisplayDaysMode = ShiftRequest.ShiftSelection.DisplayDays.StandardShifts
        Me.primaryShiftSelection.Location = New System.Drawing.Point(3, 17)
        Me.primaryShiftSelection.Margin = New System.Windows.Forms.Padding(0)
        Me.primaryShiftSelection.MinimumSize = New System.Drawing.Size(125, 0)
        Me.primaryShiftSelection.Name = "primaryShiftSelection"
        Me.primaryShiftSelection.Size = New System.Drawing.Size(125, 3)
        Me.primaryShiftSelection.SplitDays = False
        Me.primaryShiftSelection.TabIndex = 0
        '
        'cmdClose
        '
        Me.cmdClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdClose.Location = New System.Drawing.Point(555, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(100, 23)
        Me.cmdClose.TabIndex = 5
        Me.cmdClose.Text = "Luk"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdSendRequests
        '
        Me.cmdSendRequests.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSendRequests.Location = New System.Drawing.Point(449, 3)
        Me.cmdSendRequests.Name = "cmdSendRequests"
        Me.cmdSendRequests.Size = New System.Drawing.Size(100, 23)
        Me.cmdSendRequests.TabIndex = 6
        Me.cmdSendRequests.Text = "Send ønsker"
        Me.cmdSendRequests.UseVisualStyleBackColor = True
        '
        'grpSecondaryRequests
        '
        Me.grpSecondaryRequests.AutoSize = True
        Me.grpSecondaryRequests.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpSecondaryRequests.Controls.Add(Me.secondaryShiftSelection)
        Me.grpSecondaryRequests.Location = New System.Drawing.Point(167, 10)
        Me.grpSecondaryRequests.Margin = New System.Windows.Forms.Padding(10)
        Me.grpSecondaryRequests.Name = "grpSecondaryRequests"
        Me.grpSecondaryRequests.Size = New System.Drawing.Size(236, 38)
        Me.grpSecondaryRequests.TabIndex = 7
        Me.grpSecondaryRequests.TabStop = False
        Me.grpSecondaryRequests.Text = "-"
        '
        'secondaryShiftSelection
        '
        Me.secondaryShiftSelection.AddShiftTimeMarginToComboBoxes = False
        Me.secondaryShiftSelection.AutoSize = True
        Me.secondaryShiftSelection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.secondaryShiftSelection.DisplayDaysMode = ShiftRequest.ShiftSelection.DisplayDays.FreeInterval
        Me.secondaryShiftSelection.Location = New System.Drawing.Point(3, 17)
        Me.secondaryShiftSelection.Margin = New System.Windows.Forms.Padding(0)
        Me.secondaryShiftSelection.MinimumSize = New System.Drawing.Size(230, 0)
        Me.secondaryShiftSelection.Name = "secondaryShiftSelection"
        Me.secondaryShiftSelection.Size = New System.Drawing.Size(230, 3)
        Me.secondaryShiftSelection.SplitDays = False
        Me.secondaryShiftSelection.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmdClose)
        Me.Panel1.Controls.Add(Me.cmdSendRequests)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 181)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 40, 3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(667, 34)
        Me.Panel1.TabIndex = 8
        '
        'generalLayoutPanel
        '
        Me.generalLayoutPanel.AutoSize = True
        Me.generalLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.generalLayoutPanel.ColumnCount = 3
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.generalLayoutPanel.Controls.Add(Me.dayDescriptionLayoutPanel, 0, 0)
        Me.generalLayoutPanel.Controls.Add(Me.grpPrimaryRequests, 1, 0)
        Me.generalLayoutPanel.Controls.Add(Me.grpSecondaryRequests, 2, 0)
        Me.generalLayoutPanel.Location = New System.Drawing.Point(12, 120)
        Me.generalLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 3, 3, 40)
        Me.generalLayoutPanel.Name = "generalLayoutPanel"
        Me.generalLayoutPanel.RowCount = 1
        Me.generalLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.generalLayoutPanel.Size = New System.Drawing.Size(413, 58)
        Me.generalLayoutPanel.TabIndex = 9
        '
        'SubmitShiftRequestForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(667, 215)
        Me.Controls.Add(Me.generalLayoutPanel)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SubmitShiftRequestForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Angiv vagtønsker"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpPrimaryRequests.ResumeLayout(False)
        Me.grpPrimaryRequests.PerformLayout()
        Me.grpSecondaryRequests.ResumeLayout(False)
        Me.grpSecondaryRequests.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.generalLayoutPanel.ResumeLayout(False)
        Me.generalLayoutPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents dayDescriptionLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents grpPrimaryRequests As System.Windows.Forms.GroupBox
    Friend WithEvents primaryShiftSelection As ShiftRequest.ShiftSelection
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdSendRequests As System.Windows.Forms.Button
    Friend WithEvents grpSecondaryRequests As System.Windows.Forms.GroupBox
    Friend WithEvents secondaryShiftSelection As ShiftRequest.ShiftSelection
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents generalLayoutPanel As System.Windows.Forms.TableLayoutPanel
End Class
