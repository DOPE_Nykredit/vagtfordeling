﻿Public Class StandardShiftComboBox
    Inherits System.Windows.Forms.ComboBox

    Private _Workday As ShiftDay = Nothing

    Public Event StandardShiftSelected(ByVal sender As Object, ByVal shift As StandardShift)

    Public Property Workday As ShiftDay
        Get
            Return _Workday
        End Get
        Set(ByVal value As ShiftDay)
            If value Is Nothing Then
                Me.Items.Clear()
            Else
                If Not value.Equals(_Workday) Then
                    Me.Items.Clear()
                    Me.Items.Add("")

                    My.Application.StandardShiftsInSelectedCampaign.ForEach(Sub(s)
                                                                                If value.Day = ShiftDay.Days.SaturdayEven Or value.Day = ShiftDay.Days.SaturdayOdd Then
                                                                                    If s.AppliedDays.Exists(Function(d) d.Day = ShiftDay.Days.Saturday) Then
                                                                                        Me.Items.Add(New StandardShiftComboBoxItem(s))
                                                                                    End If
                                                                                ElseIf value.Day = ShiftDay.Days.SundayEven Or value.Day = ShiftDay.Days.SundayOdd Then
                                                                                    If s.AppliedDays.Exists(Function(d) d.Day = ShiftDay.Days.Sunday) Then
                                                                                        Me.Items.Add(New StandardShiftComboBoxItem(s))
                                                                                    End If
                                                                                Else
                                                                                    If s.AppliedDays.Exists(Function(d) d.Day = value.Day) Then
                                                                                        Me.Items.Add(New StandardShiftComboBoxItem(s))
                                                                                    End If
                                                                                End If
                                                                            End Sub)

                    If Me.Items.Count = 1 Then Me.Enabled = False
                End If
            End If

            _Workday = value
        End Set
    End Property

    Public Property SelectedShift As StandardShift
        Get
            If Me.SelectedIndex <= 0 Then Return Nothing Else Return CType(Me.SelectedItem, StandardShiftComboBoxItem).Shift
        End Get
        Set(ByVal value As StandardShift)
            If value Is Nothing Then
                Me.SelectedIndex = -1
            ElseIf Me.Items.Contains(value) Then
                Me.SelectedItem = value
            End If
        End Set
    End Property

    Public Sub New()
        Me.New(Nothing)
    End Sub
    Public Sub New(ByVal workDay As ShiftDay)
        Me.DropDownStyle = ComboBoxStyle.DropDownList

        Me.DisplayMember = "ShiftSpan"

        Me.Workday = workDay
    End Sub

    Public Sub AddStandardShift(ByVal shift As StandardShift)
        Me.Items.Add(New StandardShiftComboBoxItem(shift))
    End Sub

    Private Class StandardShiftComboBoxItem

        Private _Shift As StandardShift

        Public ReadOnly Property Shift As StandardShift
            Get
                Return _Shift
            End Get
        End Property
        Public ReadOnly Property ShiftSpan As String
            Get
                Return _Shift.ToString
            End Get
        End Property

        Public Property StandardShiftDeleted As Boolean

        Public Sub New(ByVal shift As StandardShift, Optional ByVal standardShiftDeleted As Boolean = False)
            _Shift = shift
            Me.StandardShiftDeleted = standardShiftDeleted
        End Sub

        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            If obj Is Nothing Then Return False
            If Not obj.GetType.Equals(GetType(StandardShift)) Then Return False
            Return (_Shift.TimeStart = CType(obj, StandardShift).TimeStart And _Shift.TimeEnd = CType(obj, StandardShift).TimeEnd)
        End Function
    End Class

    Private Sub StandardShiftComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        If Me.SelectedShift IsNot Nothing AndAlso CType(Me.SelectedItem, StandardShiftComboBoxItem).StandardShiftDeleted = True Then Me.BackColor = Color.Red Else Me.BackColor = System.Drawing.SystemColors.Window

        RaiseEvent StandardShiftSelected(Me, Me.SelectedShift)
    End Sub
End Class