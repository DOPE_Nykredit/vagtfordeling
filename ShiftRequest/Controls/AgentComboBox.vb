﻿Public Class AgentComboBox
    Inherits System.Windows.Forms.ComboBox

    Public Property SelectedAgent As Agent
        Get
            If Me.SelectedIndex = -1 Then
                Return Nothing
            ElseIf Me.SelectedItem.GetType.Equals(GetType(AgentComboBoxItem)) Then
                Return CType(Me.SelectedItem, AgentComboBoxItem).Agent
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Agent)
            If value Is Nothing Then
                Me.SelectedIndex = -1
            ElseIf Me.Items.Contains(value) Then
                Me.SelectedItem = value
            Else
                Me.SelectedIndex = -1
            End If
        End Set
    End Property

    Public Sub New()
        Me.DropDownStyle = ComboBoxStyle.DropDownList
    End Sub

    Public Sub UpdateAgents()
        Me.Items.Clear()

        For Each a In My.Application.AgentsInSelectedCampaign
            Me.Items.Add(New AgentComboBoxItem(a))
        Next

        Me.ValueMember = "ID"
        Me.DisplayMember = "InitialsAndName"
    End Sub

    Private Class AgentComboBoxItem
        Public ReadOnly Property Initials As String
            Get
                Return _Agent.Initials
            End Get
        End Property
        Public ReadOnly Property InitialsAndName As String
            Get
                Return _Agent.Initials & " - " & _Agent.FullName
            End Get
        End Property

        Public Property Agent As Agent

        Public Sub New(ByVal agent As Agent)
            Me.Agent = agent
        End Sub
    End Class

End Class
