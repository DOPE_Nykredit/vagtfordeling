﻿Public Class ShiftTimeComboBox
    Inherits System.Windows.Forms.ComboBox

    Public Enum ShiftTimeStyles
        None
        StartOfShift
        EndOfShift
    End Enum

    Private updatingList As Boolean

    Private _MinTime As ShiftTime = Nothing
    Private _MaxTime As ShiftTime = Nothing

    Private _Day As ShiftDay = Nothing

    Private _ShiftTimeStyle As ShiftTimeStyles = ShiftTimeStyles.None

    Private WithEvents _CboOpposite As ShiftTimeComboBox

    Public Property SelectedShiftTime As ShiftTime
        Get
            If Me.SelectedIndex >= 0 AndAlso Me.SelectedItem.GetType.Equals(GetType(ShiftTime)) Then Return Me.SelectedItem Else Return Nothing
        End Get
        Set(ByVal value As ShiftTime)
            If value IsNot Nothing Then
                If Me.Items.Contains(value) Then Me.SelectedItem = value Else Me.SelectedIndex = -1
            Else
                Me.SelectedIndex = -1
            End If
        End Set
    End Property

    Public Property MinTime As ShiftTime
        Get
            Dim returnTime As ShiftTime

            If _MinTime Is Nothing Then
                If Me.GetMyWorkDayDefinition IsNot Nothing Then
                    returnTime = Me.GetMyWorkDayDefinition.DayStart
                Else
                    returnTime = ShiftTime.MinTime
                End If
            Else
                returnTime = _MinTime
            End If

            Return returnTime
        End Get
        Set(ByVal value As ShiftTime)
            If Object.Equals(_MinTime, value) Then Exit Property

            _MinTime = value

            Me.UpdateList()
        End Set
    End Property
    Public Property MaxTime As ShiftTime
        Get
            Dim returnTime As ShiftTime

            If _MaxTime Is Nothing Then
                If Me.GetMyWorkDayDefinition IsNot Nothing Then
                    returnTime = Me.GetMyWorkDayDefinition.DayEnd
                Else
                    returnTime = ShiftTime.MaxTime
                End If
            Else
                returnTime = _MaxTime
            End If

            Return returnTime
        End Get
        Set(ByVal value As ShiftTime)
            If Object.Equals(_MaxTime, value) Then Exit Property

            _MaxTime = value

            Me.UpdateList()
        End Set
    End Property

    Public Property ShiftDay As ShiftDay
        Get
            Return _Day
        End Get
        Set(ByVal value As ShiftDay)
            If Object.Equals(_Day, value) Then Exit Property

            _Day = value

            Me.UpdateList()
        End Set
    End Property

    Public ReadOnly Property ShiftTimeStyle As ShiftTimeStyles
        Get
            Return _ShiftTimeStyle
        End Get
    End Property

    Public Property AddShiftTimeMargin As Boolean

    Public Event ShiftTimeSelected(ByVal sender As Object, ByVal time As ShiftTime)

    Public Sub New()
        Me.New(ShiftTime.MinTime, ShiftTime.MaxTime)
    End Sub
    Public Sub New(ByVal minTime As ShiftTime, ByVal maxTime As ShiftTime)
        Me.DropDownStyle = ComboBoxStyle.DropDownList
        Me.MaxDropDownItems = 16

        _MinTime = minTime
        _MaxTime = maxTime

        Me.UpdateList()
    End Sub
    Public Sub New(ByVal day As ShiftDay)
        Me.DropDownStyle = ComboBoxStyle.DropDownList
        Me.MaxDropDownItems = 16

        _Day = day

        Me.UpdateList()
    End Sub

    Public Sub setEndTimeComboBox(ByVal endTimeComboBox As ShiftTimeComboBox)
        If endTimeComboBox Is Nothing Then Throw New ArgumentNullException("EndtimeComboBox cannot be nothing.")

        _ShiftTimeStyle = ShiftTimeStyles.StartOfShift
        endTimeComboBox._ShiftTimeStyle = ShiftTimeStyles.EndOfShift

        _CboOpposite = endTimeComboBox
        endTimeComboBox._CboOpposite = Me

        endTimeComboBox.UpdateList()
        Me.UpdateList()
    End Sub

    Private Sub UpdateList()
        Dim preSelectedItem As Object = Nothing

        Me.BeginUpdate()
        Me.updatingList = True

        If Me.Items.Count > 0 And Me.SelectedItem IsNot Nothing Then
            preSelectedItem = Me.SelectedItem
        End If

        Me.Items.Clear()

        Dim shiftTimes As List(Of ShiftTime) = ShiftTime.GetAllShiftTimes

        Dim minimumTime As ShiftTime
        Dim maximumTime As ShiftTime

        If _CboOpposite Is Nothing OrElse (_CboOpposite.SelectedShiftTime Is Nothing) Then
            minimumTime = Me.MinTime
            maximumTime = Me.MaxTime
        Else
            If _ShiftTimeStyle = ShiftTimeStyles.StartOfShift Then
                minimumTime = Me.MinTime
                If _CboOpposite.SelectedShiftTime < Me.MaxTime Then maximumTime = _CboOpposite.SelectedShiftTime Else maximumTime = Me.MaxTime
            Else
                If _CboOpposite.SelectedShiftTime > Me.MinTime Then minimumTime = _CboOpposite.SelectedShiftTime Else minimumTime = Me.MinTime
                maximumTime = Me.MaxTime
            End If
        End If

        For Each t In shiftTimes
            If _ShiftTimeStyle = ShiftTimeStyles.StartOfShift Then
                If Me.AddShiftTimeMargin Then
                    If t >= minimumTime And t < maximumTime Then
                        Me.Items.Add(t)
                    End If
                Else
                    If t >= minimumTime And t <= maximumTime Then
                        Me.Items.Add(t)
                    End If
                End If
            Else
                If Me.AddShiftTimeMargin Then
                    If t > minimumTime And t <= maximumTime Then
                        Me.Items.Add(t)
                    End If
                Else
                    If t >= minimumTime And t <= maximumTime Then
                        Me.Items.Add(t)
                    End If
                End If
            End If
        Next

        If preSelectedItem IsNot Nothing Then
            If Me.Items.Contains(preSelectedItem) Then
                Me.SelectedItem = preSelectedItem
            Else
                If Me.ShiftTimeStyle = ShiftTimeStyles.StartOfShift Then
                    Me.SelectedIndex = 0
                ElseIf Me.ShiftTimeStyle = ShiftTimeStyles.EndOfShift Then
                    Me.SelectedIndex = Me.Items.Count - 1
                End If
            End If
        End If

        Me.EndUpdate()
        Me.updatingList = False
    End Sub

    Public Sub ShowAllShiftTimesUpTo(ByVal toTime As ShiftTime)
        _MinTime = ShiftTime.MinTime
        Me.MaxTime = toTime
    End Sub

    Public Sub ShowAllShiftsTimesFrom(ByVal fromTime As ShiftTime)
        _MaxTime = ShiftTime.MaxTime
        Me.MinTime = fromTime
    End Sub

    Protected Overrides Sub OnSelectedIndexChanged(ByVal e As System.EventArgs)
        MyBase.OnSelectedIndexChanged(e)

        If Me.updatingList = False Then RaiseEvent ShiftTimeSelected(Me, Me.SelectedShiftTime)
    End Sub

    Protected Overrides Sub OnEnabledChanged(ByVal e As System.EventArgs)
        If Me.Enabled = False Then Me.SelectedIndex = -1

        MyBase.OnEnabledChanged(e)
    End Sub

    Private Sub CboOpposite_ShiftTimeSelected(ByVal sender As Object, ByVal time As ShiftTime) Handles _CboOpposite.ShiftTimeSelected
        Me.UpdateList()

        If _CboOpposite.SelectedShiftTime IsNot Nothing And Me.SelectedShiftTime Is Nothing Then
            If _ShiftTimeStyle = ShiftTimeStyles.StartOfShift Then
                Me.SelectedIndex = Me.Items.Count - 1
            Else
                Me.SelectedIndex = 0
            End If
        End If
    End Sub
    Private Function GetMyWorkDayDefinition() As WorkDayDefinition
        Dim myWorkDayDefinition As WorkDayDefinition = Nothing

        If _Day IsNot Nothing Then
            myWorkDayDefinition = My.Application.WorkdayDefinitionsAll.Find(Function(d) d.ShiftDay.Equals(_Day))
        End If

        Return myWorkDayDefinition
    End Function
End Class