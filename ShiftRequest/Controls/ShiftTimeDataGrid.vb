﻿Public MustInherit Class ShiftTimeDataGrid
    Inherits System.Windows.Forms.DataGridView

    Protected SplitDays As Boolean

    Public Sub New()
        Me.AllowUserToAddRows = False
        Me.AllowUserToDeleteRows = False
        Me.AllowUserToOrderColumns = False
        Me.AllowUserToResizeColumns = False
        Me.AllowUserToResizeRows = False
        Me.MultiSelect = False

        Me.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Protected Sub ResetTableDefinition()
        Me.Columns.Clear()
        Me.Rows.Clear()

        Me.SetColumns()
        Me.SetRows()
        Me.SetupFieldProperties()
    End Sub

    Private Sub SetColumns()
        Me.Columns.Clear()

        Dim workdDayDefinitions As List(Of WorkDayDefinition) = IIf(Me.SplitDays,
                                                                    My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign,
                                                                    My.Application.WorkDayDefinitionsInSelectedCampaign)
        Me.ColumnCount = workdDayDefinitions.Count

        For i = 0 To workdDayDefinitions.Count - 1
            Me.Columns(i).HeaderText = ShiftDay.GetShiftDayDescription(workdDayDefinitions(i).ShiftDay.Day)
            Me.Columns(i).Tag = workdDayDefinitions(i).ShiftDay
            Me.Columns(i).Width = 55
        Next

        Me.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
    End Sub
    Private Sub SetRows()
        Dim earliestTime = My.Application.WorkDayDefinitionsInSelectedCampaign.Min(Function(d) d.DayStart)
        Dim latestTime = My.Application.WorkDayDefinitionsInSelectedCampaign.Max(Function(d) d.DayEnd)

        If earliestTime IsNot Nothing Then
            Dim maxTimeSpan As New ShiftTimeSpan(earliestTime, latestTime)

            Dim intervals = maxTimeSpan.GetAllIntervalsInSpan

            Me.RowCount = intervals.Count

            For i = 0 To intervals.Count - 1
                Me.Rows(i).Tag = intervals(i)
                Me.Rows(i).HeaderCell.Value = intervals(i).ToString
            Next

            Me.RowHeadersWidth = 105
        End If
    End Sub

    Private Sub SetupFieldProperties()
        Dim workDayDefinitions As List(Of WorkDayDefinition) = IIf(Me.SplitDays,
                                                                   My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign,
                                                                   My.Application.WorkDayDefinitionsInSelectedCampaign)

        For i = 0 To workDayDefinitions.Count - 1
            For Each r As DataGridViewRow In Me.Rows
                Dim currentCell = TryCast(r.Cells(i), DataGridViewTextBoxCell)

                If currentCell IsNot Nothing Then
                    Dim cellsDaySpan = workDayDefinitions(i).DaySpan

                    If cellsDaySpan IsNot Nothing AndAlso cellsDaySpan.IsInInterval(CType(r.Tag, ShiftTimeSpan).StartTime) Then
                        currentCell.Style.BackColor = Color.White
                        currentCell.Value = 0
                        currentCell.ValueType = GetType(Byte)
                        currentCell.ReadOnly = False
                        currentCell.Style.Format = "N0"
                        currentCell.MaxInputLength = 4
                    Else
                        currentCell.Style.BackColor = Color.Gray
                        currentCell.MaxInputLength = 0
                        currentCell.Value = ""
                        currentCell.ReadOnly = True
                    End If
                End If
            Next
        Next
    End Sub

    Protected Function GetCellByDayAndTimeSpan(ByVal day As ShiftDay, ByVal timeSpan As ShiftTimeSpan) As DataGridViewCell
        For Each r As DataGridViewRow In Me.Rows
            If timeSpan.Equals(r.Tag) Then
                For Each c In r.Cells
                    If c.GetType.Equals(GetType(DataGridViewTextBoxCell)) Then
                        If CType(c, DataGridViewTextBoxCell).OwningColumn.Tag.Equals(day) Then Return c
                    End If
                Next
            End If
        Next

        Return Nothing
    End Function
End Class
