﻿Public Class AssignedShiftListView
    Inherits System.Windows.Forms.ListView

    Private _Day As ShiftDay
    Private _Interval As ShiftTimeSpan

    Public Event AssignedShiftSelected(ByVal assignedShift As AssignedShift)

    Public ReadOnly Property SelectedShift As AssignedShift
        Get
            If Me.SelectedItems.Count > 0 Then
                Return CType(Me.SelectedItems(0), AssignedShiftListViewItem).AssignedShift
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Sub New()
        Me.CheckBoxes = False
        Me.HeaderStyle = ColumnHeaderStyle.Nonclickable
        Me.FullRowSelect = True
        Me.HideSelection = False
        Me.MultiSelect = False
        Me.ShowGroups = False
        Me.Sorting = SortOrder.Ascending
        Me.View = Windows.Forms.View.Details

        Me.Columns.Add("Initialer", 100, HorizontalAlignment.Left)
        Me.Columns.Add("Vagt", 150, HorizontalAlignment.Center)

        Me.Width = 274
    End Sub

    Public Sub ClearSelection()
        For Each i As ListViewItem In Me.Items
            i.Selected = False
        Next
    End Sub

    Public Sub ShowAssignedShifts(ByVal day As ShiftDay, ByVal interval As ShiftTimeSpan)
        If day Is Nothing Then Throw New ArgumentNullException("Day cannot be nothing.")
        If interval Is Nothing Then Throw New ArgumentNullException("Interval cannot be nothing.")

        _Day = day
        _Interval = interval

        Me.UpdateList()
    End Sub
    Public Sub UpdateList()
        Me.BeginUpdate()

        Me.Items.Clear()

        Dim assignedShiftInPeriod = My.Application.AssignedShiftsInSelectedCampaign.FindAll(Function(a) a.Day.Equals(_Day) And a.ShiftSpan.IsInInterval(_Interval))

        assignedShiftInPeriod.ForEach(Sub(a) Me.Items.Add(New AssignedShiftListViewItem(a)))

        Me.EndUpdate()
    End Sub

    Private Class AssignedShiftListViewItem
        Inherits System.Windows.Forms.ListViewItem

        Private WithEvents _AssignedShift As AssignedShift

        Public ReadOnly Property AssignedShift As AssignedShift
            Get
                Return _AssignedShift
            End Get
        End Property

        Public Sub New(ByVal assignedShift As AssignedShift)
            _AssignedShift = assignedShift

            Me.Text = My.Application.AgentsInSelectedCampaign.First(Function(a) a.Id = assignedShift.AgentId).Initials

            Me.SubItems.Add(assignedShift.ShiftSpan.ToString)
        End Sub

        Private Sub AssignedShiftSaved() Handles _AssignedShift.Saved
            Me.Remove()
        End Sub
        Private Sub AssignedShift_Deleted() Handles _AssignedShift.Deleted
            Me.Remove()
        End Sub
    End Class

    Private Sub AssignedShiftListView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        If Me.SelectedItems.Count > 0 Then
            RaiseEvent AssignedShiftSelected(CType(Me.SelectedItems(0), AssignedShiftListViewItem).AssignedShift)
        End If
    End Sub
End Class


