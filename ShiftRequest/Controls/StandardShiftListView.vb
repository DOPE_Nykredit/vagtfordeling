﻿Public Class StandardShiftListView
    Inherits System.Windows.Forms.ListView

    Public Event StandardShiftSelected(ByVal standardShift As StandardShift)

    Public Sub New()
        Me.Groups.Add("ActiveShifts", "Aktive standardvagter")
        Me.Activation = ItemActivation.OneClick
        Me.GridLines = True
        Me.HideSelection = False
    End Sub

    Public Sub UpdateStandardShifts()
        Dim rows As New List(Of StandardShiftListViewItem)

        For Each s In My.Application.StandardShiftsInSelectedCampaign
            For Each d In s.AppliedDays
                rows.Add(New StandardShiftListViewItem(s, d) With {.Group = Me.Groups(0)})
            Next
        Next

        rows.Sort()

        Me.Items.Clear()

        rows.ForEach(Sub(r) Me.Items.Add(r))

        If Me.Items.Count > 0 Then
            Dim lastDay As ShiftDay = CType(Me.Items(0), StandardShiftListViewItem).Day
            Dim dayCounter As Byte = 0

            For Each i As StandardShiftListViewItem In Me.Items
                If lastDay.Day <> i.Day.Day Then
                    dayCounter += 1
                    lastDay = i.Day
                End If

                If dayCounter Mod 2 <> 0 Then i.BackColor = Color.Lavender
            Next
        End If
    End Sub

    Private Sub StandardShiftListView_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ItemActivate
        If Me.SelectedItems.Count > 0 Then
            Dim selectedId = CType(Me.SelectedItems(0), StandardShiftListViewItem).StandardShift.ID

            For Each i As ListViewItem In Me.Items
                If CType(i, StandardShiftListViewItem).StandardShift.ID = selectedId Then
                    If Not i.Selected Then i.Selected = True
                End If
            Next

            RaiseEvent StandardShiftSelected(CType(Me.SelectedItems(0), StandardShiftListViewItem).StandardShift)
        End If
    End Sub

    Public Sub RemoveStandardShift(ByVal shift As StandardShift)
        For Each i As ListViewItem In Me.Items
            If CType(i, StandardShiftListViewItem).StandardShift.ID = shift.ID Then i.Remove()
        Next
    End Sub

End Class

Public Class StandardShiftListViewItem
    Inherits System.Windows.Forms.ListViewItem
    Implements IComparable(Of StandardShiftListViewItem)

    Private _Shift As StandardShift
    Private _Day As ShiftDay

    Public ReadOnly Property StandardShift As StandardShift
        Get
            Return _Shift
        End Get
    End Property
    Public ReadOnly Property Day As ShiftDay
        Get
            Return _Day
        End Get
    End Property

    Public Sub New(ByVal Shift As StandardShift, ByVal day As ShiftDay)
        _Day = day
        _Shift = Shift

        Me.Text = _Day.DayDescription
        Me.SubItems.Add(_Shift.TimeStart.ToString & " - " & _Shift.TimeEnd.ToString)
    End Sub

    Public Function CompareTo1(ByVal other As StandardShiftListViewItem) As Integer Implements System.IComparable(Of StandardShiftListViewItem).CompareTo
        Dim x1 = CType(other, StandardShiftListViewItem)

        If x1.Day.Day = Me.Day.Day Then
            Return Me.StandardShift.TimeStart.CompareTo(x1.StandardShift.TimeStart)
        Else
            Return Me.Day.Day.CompareTo(x1.Day.Day)
        End If
    End Function
End Class

