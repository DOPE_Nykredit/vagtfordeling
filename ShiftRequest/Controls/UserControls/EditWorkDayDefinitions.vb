﻿Public Class EditWorkDayDefinitions

    Public Event WorkDayDefinitionsSaved()

    Public Sub SetupControls()
        Dim workDayDefinitions As List(Of WorkDayDefinition) = My.Application.WorkDayDefinitionsInSelectedCampaign

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Monday) Then
            Dim monday As WorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Monday)

            Me.chkMondayActive.Checked = True

            Me.cboMondayStart.SelectedItem = monday.DayStart
            Me.cboMondayEnd.SelectedItem = monday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtMondayRequiredHours, monday.RequiredHours)
        End If

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Tuesday) Then
            Dim tuesday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Tuesday)

            Me.chkTuesdayActive.Checked = True

            Me.cboTuesdayStart.SelectedItem = tuesday.DayStart
            Me.cboTuesdayEnd.SelectedItem = tuesday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtTuesdayRequiredHours, tuesday.RequiredHours)
        End If

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Wednesday) Then
            Dim wednesday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Wednesday)

            Me.chkWednesdayActive.Checked = True

            Me.cboWednesdayStart.SelectedItem = wednesday.DayStart
            Me.cboWednesdayEnd.SelectedItem = wednesday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtWednesdayRequiredHours, wednesday.RequiredHours)
        End If

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Thursday) Then
            Dim thursday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Thursday)

            Me.chkThursdayActive.Checked = True

            Me.cboThursdayStart.SelectedItem = thursday.DayStart
            Me.cboThursdayEnd.SelectedItem = thursday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtThursdayRequiredHours, thursday.RequiredHours)
        End If

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Friday) Then
            Dim friday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Friday)

            Me.chkFridayActive.Checked = True

            Me.cboFridayStart.SelectedItem = friday.DayStart
            Me.cboFridayEnd.SelectedItem = friday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtFridayRequiredHours, friday.RequiredHours)
        End If

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Saturday) Then
            Dim saturday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday)

            Me.chkSaturdayActive.Checked = True

            Me.cboSaturdayStart.SelectedItem = saturday.DayStart
            Me.cboSaturdayEnd.SelectedItem = saturday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtSaturdayRequiredHours, saturday.RequiredHours)
        End If

        Me.cboSundayStart.setEndTimeComboBox(Me.cboSundayEnd)

        If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = ShiftDay.Days.Sunday) Then
            Dim sunday = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday)

            Me.chkSundayActive.Checked = True

            Me.cboSundayStart.SelectedItem = sunday.DayStart
            Me.cboSundayEnd.SelectedItem = sunday.DayEnd

            Me.InsertTimeInRequiredHoursTextBox(txtSundayRequiredHours, sunday.RequiredHours)
        End If
    End Sub

    Private Sub InsertTimeInRequiredHoursTextBox(ByVal textbox As MaskedTextBox, ByVal time As Decimal)
        Dim hours As Integer = time
        Dim minutes = time - hours

        Dim hoursString As String = hours
        Dim minutesString As String = minutes.ToString.Replace("0,", "")

        Do Until hoursString.Length >= 3
            hoursString = hoursString.Insert(0, "0")
        Loop

        textbox.Text = hoursString & ":" & minutesString
    End Sub

    Private Sub chkMondayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMondayActive.CheckedChanged
        Me.cboMondayStart.Enabled = Me.chkMondayActive.Checked
        Me.cboMondayEnd.Enabled = Me.chkMondayActive.Checked
        Me.txtMondayRequiredHours.Enabled = Me.chkMondayActive.Checked

        If Me.chkMondayActive.Checked Then
            Me.cboMondayStart.SelectedIndex = 0
            Me.cboMondayEnd.SelectedIndex = Me.cboMondayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkTuesdayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTuesdayActive.CheckedChanged
        Me.cboTuesdayStart.Enabled = Me.chkTuesdayActive.Checked
        Me.cboTuesdayEnd.Enabled = Me.chkTuesdayActive.Checked
        Me.txtTuesdayRequiredHours.Enabled = Me.chkTuesdayActive.Checked

        If Me.chkTuesdayActive.Checked Then
            Me.cboTuesdayStart.SelectedIndex = 0
            Me.cboTuesdayEnd.SelectedIndex = Me.cboTuesdayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkWednesdayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkWednesdayActive.CheckedChanged
        Me.cboWednesdayStart.Enabled = Me.chkWednesdayActive.Checked
        Me.cboWednesdayEnd.Enabled = Me.chkWednesdayActive.Checked
        Me.txtWednesdayRequiredHours.Enabled = Me.chkWednesdayActive.Checked

        If Me.chkWednesdayActive.Checked Then
            Me.cboWednesdayStart.SelectedIndex = 0
            Me.cboWednesdayEnd.SelectedIndex = Me.cboWednesdayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkThursdayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkThursdayActive.CheckedChanged
        Me.cboThursdayStart.Enabled = Me.chkThursdayActive.Checked
        Me.cboThursdayEnd.Enabled = Me.chkThursdayActive.Checked
        Me.txtThursdayRequiredHours.Enabled = Me.chkThursdayActive.Checked

        If Me.chkThursdayActive.Checked Then
            Me.cboThursdayStart.SelectedIndex = 0
            Me.cboThursdayEnd.SelectedIndex = Me.cboThursdayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkFridayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFridayActive.CheckedChanged
        Me.cboFridayStart.Enabled = Me.chkFridayActive.Checked
        Me.cboFridayEnd.Enabled = Me.chkFridayActive.Checked
        Me.txtFridayRequiredHours.Enabled = Me.chkFridayActive.Checked

        If Me.chkFridayActive.Checked Then
            Me.cboFridayStart.SelectedIndex = 0
            Me.cboFridayEnd.SelectedIndex = Me.cboFridayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkSaturdayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSaturdayActive.CheckedChanged
        Me.cboSaturdayStart.Enabled = Me.chkSaturdayActive.Checked
        Me.cboSaturdayEnd.Enabled = Me.chkSaturdayActive.Checked
        Me.txtSaturdayRequiredHours.Enabled = Me.chkSaturdayActive.Checked

        If chkSaturdayActive.Checked Then
            Me.cboSaturdayStart.SelectedIndex = 0
            Me.cboSaturdayEnd.SelectedIndex = Me.cboSaturdayEnd.Items.Count - 1
        End If
    End Sub
    Private Sub chkSundayActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSundayActive.CheckedChanged
        Me.cboSundayStart.Enabled = Me.chkSundayActive.Checked
        Me.cboSundayEnd.Enabled = Me.chkSundayActive.Checked
        Me.txtSundayRequiredHours.Enabled = Me.chkSundayActive.Checked

        If chkSundayActive.Checked Then
            Me.cboSundayStart.SelectedIndex = 0
            Me.cboSundayEnd.SelectedIndex = Me.cboSundayEnd.Items.Count - 1
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Me.Cursor = Cursors.WaitCursor

        Try
            Me.SaveWorkDayDefinitions()

            RaiseEvent WorkDayDefinitionsSaved()
        Catch ex As OracleClient.OracleException
            Throw
        Finally
            Me.Cursor = Cursors.Arrow
        End Try
    End Sub

    Private Sub SaveWorkDayDefinitions()
        Dim conn As New ShiftRequestConnection

        Dim workDayDefinitions = My.Application.WorkDayDefinitionsInSelectedCampaign

        If chkMondayActive.Checked Then
            Dim mondayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Monday) Then
                mondayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Monday)

                mondayWorkDayDefinition.DayStart = Me.cboMondayStart.SelectedShiftTime
                mondayWorkDayDefinition.DayEnd = Me.cboMondayEnd.SelectedShiftTime

                mondayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtMondayRequiredHours)
            Else
                mondayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Monday,
                                                                Me.cboMondayStart.SelectedShiftTime,
                                                                Me.cboMondayEnd.SelectedShiftTime,
                                                                Me.GetTimeFromRequiredHoursTextbox(Me.txtMondayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(mondayWorkDayDefinition)
            End If

            mondayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Monday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Monday)
        End If

        If chkTuesdayActive.Checked Then
            Dim tuesdayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Tuesday) Then
                tuesdayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Tuesday)

                tuesdayWorkDayDefinition.DayStart = Me.cboTuesdayStart.SelectedShiftTime
                tuesdayWorkDayDefinition.DayEnd = Me.cboTuesdayEnd.SelectedShiftTime

                tuesdayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtTuesdayRequiredHours)
            Else
                tuesdayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Tuesday,
                                                                 Me.cboTuesdayStart.SelectedShiftTime,
                                                                 Me.cboTuesdayEnd.SelectedShiftTime,
                                                                 Me.GetTimeFromRequiredHoursTextbox(Me.txtTuesdayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(tuesdayWorkDayDefinition)
            End If

            tuesdayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Tuesday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Tuesday)
        End If

        If chkWednesdayActive.Checked Then
            Dim wednesdayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Wednesday) Then
                wednesdayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Wednesday)

                wednesdayWorkDayDefinition.DayStart = Me.cboWednesdayStart.SelectedShiftTime
                wednesdayWorkDayDefinition.DayEnd = Me.cboWednesdayEnd.SelectedShiftTime

                wednesdayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtWednesdayRequiredHours)
            Else
                wednesdayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Wednesday,
                                                                   Me.cboWednesdayStart.SelectedShiftTime,
                                                                   Me.cboWednesdayEnd.SelectedShiftTime,
                                                                   Me.GetTimeFromRequiredHoursTextbox(Me.txtWednesdayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(wednesdayWorkDayDefinition)
            End If

            wednesdayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Wednesday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Wednesday)
        End If

        If chkThursdayActive.Checked Then
            Dim thursdayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Thursday) Then
                thursdayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Thursday)

                thursdayWorkDayDefinition.DayStart = Me.cboThursdayStart.SelectedShiftTime
                thursdayWorkDayDefinition.DayEnd = Me.cboThursdayEnd.SelectedShiftTime

                thursdayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtThursdayRequiredHours)
            Else
                thursdayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Thursday,
                                                                  Me.cboThursdayStart.SelectedShiftTime,
                                                                  Me.cboThursdayEnd.SelectedShiftTime,
                                                                  Me.GetTimeFromRequiredHoursTextbox(Me.txtThursdayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(thursdayWorkDayDefinition)
            End If

            thursdayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Thursday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Thursday)
        End If

        If chkFridayActive.Checked Then
            Dim fridayWorkdayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Friday) Then
                fridayWorkdayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Friday)

                fridayWorkdayDefinition.DayStart = Me.cboFridayStart.SelectedShiftTime
                fridayWorkdayDefinition.DayEnd = Me.cboFridayEnd.SelectedShiftTime

                fridayWorkdayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtFridayRequiredHours)
            Else
                fridayWorkdayDefinition = New WorkDayDefinition(ShiftDay.Days.Friday,
                                                                Me.cboFridayStart.SelectedShiftTime,
                                                                Me.cboFridayEnd.SelectedShiftTime,
                                                                Me.GetTimeFromRequiredHoursTextbox(Me.txtFridayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(fridayWorkdayDefinition)
            End If

            fridayWorkdayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Friday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Friday)
        End If

        If chkSaturdayActive.Checked Then
            Dim saturdayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday) Then
                saturdayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday)

                saturdayWorkDayDefinition.DayStart = Me.cboSaturdayStart.SelectedShiftTime
                saturdayWorkDayDefinition.DayEnd = Me.cboSaturdayEnd.SelectedShiftTime

                saturdayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtSaturdayRequiredHours)
            Else
                saturdayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Saturday,
                                                                  Me.cboSaturdayStart.SelectedShiftTime,
                                                                  Me.cboSaturdayEnd.SelectedShiftTime,
                                                                  Me.GetTimeFromRequiredHoursTextbox(Me.txtSaturdayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(saturdayWorkDayDefinition)
            End If

            saturdayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Saturday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Saturday)
        End If

        If chkSundayActive.Checked Then
            Dim sundayWorkDayDefinition As WorkDayDefinition

            If workDayDefinitions.Exists(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday) Then
                sundayWorkDayDefinition = workDayDefinitions.First(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday)

                sundayWorkDayDefinition.DayStart = Me.cboSundayStart.SelectedShiftTime
                sundayWorkDayDefinition.DayEnd = Me.cboSundayEnd.SelectedShiftTime

                sundayWorkDayDefinition.RequiredHours = Me.GetTimeFromRequiredHoursTextbox(Me.txtSundayRequiredHours)
            Else
                sundayWorkDayDefinition = New WorkDayDefinition(ShiftDay.Days.Sunday,
                                                                Me.cboSundayStart.SelectedShiftTime,
                                                                Me.cboSundayEnd.SelectedShiftTime,
                                                                Me.GetTimeFromRequiredHoursTextbox(Me.txtSundayRequiredHours))

                My.Application.WorkDayDefinitionsInSelectedCampaign.Add(sundayWorkDayDefinition)
            End If

            sundayWorkDayDefinition.Save(conn)
        Else
            WorkDayDefinition.DeleteWorkDayDefinitionInSelectedCampaign(conn, ShiftDay.Days.Sunday)

            My.Application.WorkDayDefinitionsInSelectedCampaign.RemoveAll(Function(d) d.ShiftDay.Day = ShiftDay.Days.Sunday)
        End If

            conn.CloseConnection()
    End Sub

    Private Function GetTimeFromRequiredHoursTextbox(ByVal textBox As MaskedTextBox)
        Dim input = textBox.Text

        Dim elementsInInput = input.Split(New String({":"}))

        Dim hours = elementsInInput(0).TrimStart.Replace(" ", "0")
        Dim minutes = elementsInInput(1).Replace(" ", "0")

        If minutes = "" Then minutes = "0"
        If hours = "" Then hours = "0"

        Return Decimal.Parse(hours & "," & minutes)
    End Function

End Class
