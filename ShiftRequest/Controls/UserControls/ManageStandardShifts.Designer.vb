﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManageStandardShifts
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ListViewGroup1 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup2 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup3 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup4 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup5 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup6 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup7 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup8 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup9 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup10 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup11 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup12 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup13 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup14 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup15 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup16 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Aktive standardvagter", System.Windows.Forms.HorizontalAlignment.Left)
        Me.grpDays = New System.Windows.Forms.GroupBox()
        Me.chkSunday = New System.Windows.Forms.CheckBox()
        Me.chkSaturday = New System.Windows.Forms.CheckBox()
        Me.chkFriday = New System.Windows.Forms.CheckBox()
        Me.chkThursday = New System.Windows.Forms.CheckBox()
        Me.chkWednesday = New System.Windows.Forms.CheckBox()
        Me.chkTuesday = New System.Windows.Forms.CheckBox()
        Me.chkMonday = New System.Windows.Forms.CheckBox()
        Me.lblShiftStart = New System.Windows.Forms.Label()
        Me.lblShiftEnd = New System.Windows.Forms.Label()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmdNew = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.StandardShiftListView = New ShiftRequest.StandardShiftListView()
        Me.Day = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Shift = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cboShiftEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboShiftStart = New ShiftRequest.ShiftTimeComboBox()
        Me.grpDays.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpDays
        '
        Me.grpDays.Controls.Add(Me.chkSunday)
        Me.grpDays.Controls.Add(Me.chkSaturday)
        Me.grpDays.Controls.Add(Me.chkFriday)
        Me.grpDays.Controls.Add(Me.chkThursday)
        Me.grpDays.Controls.Add(Me.chkWednesday)
        Me.grpDays.Controls.Add(Me.chkTuesday)
        Me.grpDays.Controls.Add(Me.chkMonday)
        Me.grpDays.Location = New System.Drawing.Point(260, 109)
        Me.grpDays.Name = "grpDays"
        Me.grpDays.Size = New System.Drawing.Size(166, 211)
        Me.grpDays.TabIndex = 0
        Me.grpDays.TabStop = False
        Me.grpDays.Text = "Gælder for:"
        '
        'chkSunday
        '
        Me.chkSunday.AutoSize = True
        Me.chkSunday.Location = New System.Drawing.Point(28, 177)
        Me.chkSunday.Name = "chkSunday"
        Me.chkSunday.Size = New System.Drawing.Size(68, 18)
        Me.chkSunday.TabIndex = 7
        Me.chkSunday.Tag = "6"
        Me.chkSunday.Text = "Søndag"
        Me.chkSunday.UseVisualStyleBackColor = True
        '
        'chkSaturday
        '
        Me.chkSaturday.AutoSize = True
        Me.chkSaturday.Location = New System.Drawing.Point(28, 152)
        Me.chkSaturday.Name = "chkSaturday"
        Me.chkSaturday.Size = New System.Drawing.Size(64, 18)
        Me.chkSaturday.TabIndex = 5
        Me.chkSaturday.Tag = "5"
        Me.chkSaturday.Text = "Lørdag"
        Me.chkSaturday.UseVisualStyleBackColor = True
        '
        'chkFriday
        '
        Me.chkFriday.AutoSize = True
        Me.chkFriday.Location = New System.Drawing.Point(28, 127)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Size = New System.Drawing.Size(63, 18)
        Me.chkFriday.TabIndex = 4
        Me.chkFriday.Tag = "4"
        Me.chkFriday.Text = "Fredag"
        Me.chkFriday.UseVisualStyleBackColor = True
        '
        'chkThursday
        '
        Me.chkThursday.AutoSize = True
        Me.chkThursday.Location = New System.Drawing.Point(28, 102)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Size = New System.Drawing.Size(70, 18)
        Me.chkThursday.TabIndex = 3
        Me.chkThursday.Tag = "3"
        Me.chkThursday.Text = "Torsdag"
        Me.chkThursday.UseVisualStyleBackColor = True
        '
        'chkWednesday
        '
        Me.chkWednesday.AutoSize = True
        Me.chkWednesday.Location = New System.Drawing.Point(28, 78)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(67, 18)
        Me.chkWednesday.TabIndex = 2
        Me.chkWednesday.Tag = "2"
        Me.chkWednesday.Text = "Onsdag"
        Me.chkWednesday.UseVisualStyleBackColor = True
        '
        'chkTuesday
        '
        Me.chkTuesday.AutoSize = True
        Me.chkTuesday.Location = New System.Drawing.Point(28, 53)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(65, 18)
        Me.chkTuesday.TabIndex = 1
        Me.chkTuesday.Tag = "1"
        Me.chkTuesday.Text = "Tirsdag"
        Me.chkTuesday.UseVisualStyleBackColor = True
        '
        'chkMonday
        '
        Me.chkMonday.AutoSize = True
        Me.chkMonday.Location = New System.Drawing.Point(28, 28)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Size = New System.Drawing.Size(68, 18)
        Me.chkMonday.TabIndex = 0
        Me.chkMonday.Tag = "0"
        Me.chkMonday.Text = "Mandag"
        Me.chkMonday.UseVisualStyleBackColor = True
        '
        'lblShiftStart
        '
        Me.lblShiftStart.AutoSize = True
        Me.lblShiftStart.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftStart.Location = New System.Drawing.Point(257, 4)
        Me.lblShiftStart.Name = "lblShiftStart"
        Me.lblShiftStart.Size = New System.Drawing.Size(43, 14)
        Me.lblShiftStart.TabIndex = 2
        Me.lblShiftStart.Text = "Start:"
        '
        'lblShiftEnd
        '
        Me.lblShiftEnd.AutoSize = True
        Me.lblShiftEnd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftEnd.Location = New System.Drawing.Point(257, 62)
        Me.lblShiftEnd.Name = "lblShiftEnd"
        Me.lblShiftEnd.Size = New System.Drawing.Size(36, 14)
        Me.lblShiftEnd.TabIndex = 3
        Me.lblShiftEnd.Text = "Slut:"
        '
        'cmdSave
        '
        Me.cmdSave.Enabled = False
        Me.cmdSave.Location = New System.Drawing.Point(260, 326)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(100, 25)
        Me.cmdSave.TabIndex = 9
        Me.cmdSave.Text = "Gem"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdNew
        '
        Me.cmdNew.Location = New System.Drawing.Point(3, 428)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.Size = New System.Drawing.Size(100, 25)
        Me.cmdNew.TabIndex = 11
        Me.cmdNew.Text = "Ny"
        Me.cmdNew.UseVisualStyleBackColor = True
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Location = New System.Drawing.Point(109, 428)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(100, 25)
        Me.cmdDelete.TabIndex = 10
        Me.cmdDelete.Text = "Slet"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'StandardShiftListView
        '
        Me.StandardShiftListView.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.StandardShiftListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Day, Me.Shift})
        Me.StandardShiftListView.FullRowSelect = True
        Me.StandardShiftListView.GridLines = True
        ListViewGroup1.Header = "Aktive standardvagter"
        ListViewGroup1.Name = "ActiveShifts"
        ListViewGroup2.Header = "Aktive standardvagter"
        ListViewGroup2.Name = "ActiveShifts"
        ListViewGroup3.Header = "Aktive standardvagter"
        ListViewGroup3.Name = "ActiveShifts"
        ListViewGroup4.Header = "Aktive standardvagter"
        ListViewGroup4.Name = "ActiveShifts"
        ListViewGroup5.Header = "Aktive standardvagter"
        ListViewGroup5.Name = "ActiveShifts"
        ListViewGroup6.Header = "Aktive standardvagter"
        ListViewGroup6.Name = "ActiveShifts"
        ListViewGroup7.Header = "Aktive standardvagter"
        ListViewGroup7.Name = "ActiveShifts"
        ListViewGroup8.Header = "Aktive standardvagter"
        ListViewGroup8.Name = "ActiveShifts"
        ListViewGroup9.Header = "Aktive standardvagter"
        ListViewGroup9.Name = "ActiveShifts"
        ListViewGroup10.Header = "Aktive standardvagter"
        ListViewGroup10.Name = "ActiveShifts"
        ListViewGroup11.Header = "Aktive standardvagter"
        ListViewGroup11.Name = "ActiveShifts"
        ListViewGroup12.Header = "Aktive standardvagter"
        ListViewGroup12.Name = "ActiveShifts"
        ListViewGroup13.Header = "Aktive standardvagter"
        ListViewGroup13.Name = "ActiveShifts"
        ListViewGroup14.Header = "Aktive standardvagter"
        ListViewGroup14.Name = "ActiveShifts"
        ListViewGroup15.Header = "Aktive standardvagter"
        ListViewGroup15.Name = "ActiveShifts"
        ListViewGroup16.Header = "Aktive standardvagter"
        ListViewGroup16.Name = "ActiveShifts"
        Me.StandardShiftListView.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup1, ListViewGroup2, ListViewGroup3, ListViewGroup4, ListViewGroup5, ListViewGroup6, ListViewGroup7, ListViewGroup8, ListViewGroup9, ListViewGroup10, ListViewGroup11, ListViewGroup12, ListViewGroup13, ListViewGroup14, ListViewGroup15, ListViewGroup16})
        Me.StandardShiftListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.StandardShiftListView.HideSelection = False
        Me.StandardShiftListView.Location = New System.Drawing.Point(3, 3)
        Me.StandardShiftListView.Name = "StandardShiftListView"
        Me.StandardShiftListView.Size = New System.Drawing.Size(249, 418)
        Me.StandardShiftListView.TabIndex = 12
        Me.StandardShiftListView.UseCompatibleStateImageBehavior = False
        Me.StandardShiftListView.View = System.Windows.Forms.View.Details
        '
        'Day
        '
        Me.Day.Text = "Dag"
        '
        'Shift
        '
        Me.Shift.Text = "Vagt"
        Me.Shift.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Shift.Width = 120
        '
        'cboShiftEnd
        '
        Me.cboShiftEnd.AddShiftTimeMargin = True
        Me.cboShiftEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftEnd.Enabled = False
        Me.cboShiftEnd.FormattingEnabled = True
        Me.cboShiftEnd.Location = New System.Drawing.Point(260, 80)
        Me.cboShiftEnd.Name = "cboShiftEnd"
        Me.cboShiftEnd.SelectedShiftTime = Nothing
        Me.cboShiftEnd.Size = New System.Drawing.Size(107, 22)
        Me.cboShiftEnd.TabIndex = 4
        '
        'cboShiftStart
        '
        Me.cboShiftStart.AddShiftTimeMargin = True
        Me.cboShiftStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftStart.Enabled = False
        Me.cboShiftStart.FormattingEnabled = True
        Me.cboShiftStart.Location = New System.Drawing.Point(260, 22)
        Me.cboShiftStart.Name = "cboShiftStart"
        Me.cboShiftStart.SelectedShiftTime = Nothing
        Me.cboShiftStart.Size = New System.Drawing.Size(107, 22)
        Me.cboShiftStart.TabIndex = 1
        '
        'ManageStandardShifts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StandardShiftListView)
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cboShiftEnd)
        Me.Controls.Add(Me.lblShiftEnd)
        Me.Controls.Add(Me.lblShiftStart)
        Me.Controls.Add(Me.cboShiftStart)
        Me.Controls.Add(Me.grpDays)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ManageStandardShifts"
        Me.Size = New System.Drawing.Size(479, 458)
        Me.grpDays.ResumeLayout(False)
        Me.grpDays.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grpDays As System.Windows.Forms.GroupBox
    Friend WithEvents chkSunday As System.Windows.Forms.CheckBox
    Friend WithEvents chkSaturday As System.Windows.Forms.CheckBox
    Friend WithEvents chkFriday As System.Windows.Forms.CheckBox
    Friend WithEvents chkThursday As System.Windows.Forms.CheckBox
    Friend WithEvents chkWednesday As System.Windows.Forms.CheckBox
    Friend WithEvents chkTuesday As System.Windows.Forms.CheckBox
    Friend WithEvents chkMonday As System.Windows.Forms.CheckBox
    Friend WithEvents cboShiftStart As ShiftTimeComboBox
    Friend WithEvents lblShiftStart As System.Windows.Forms.Label
    Friend WithEvents lblShiftEnd As System.Windows.Forms.Label
    Friend WithEvents cboShiftEnd As ShiftTimeComboBox
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents StandardShiftListView As StandardShiftListView
    Friend WithEvents Day As System.Windows.Forms.ColumnHeader
    Friend WithEvents Shift As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdNew As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button

End Class
