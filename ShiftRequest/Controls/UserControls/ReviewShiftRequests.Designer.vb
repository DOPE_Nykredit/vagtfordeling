﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReviewShiftRequests
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.chkGroupByTeams = New System.Windows.Forms.CheckBox()
        Me.grpAgentFilter = New System.Windows.Forms.GroupBox()
        Me.rdoAgentWithoutRequests = New System.Windows.Forms.RadioButton()
        Me.rdoAgentsWithRequests = New System.Windows.Forms.RadioButton()
        Me.rdoAllAgents = New System.Windows.Forms.RadioButton()
        Me.grpReviewShiftRequestsOnAgent = New System.Windows.Forms.GroupBox()
        Me.AssignShift = New ShiftRequest.AssignShift()
        Me.cmdDeleteAssignedShift = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lstAssignedShifts = New System.Windows.Forms.ListView()
        Me.AssignedShiftDayColumn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AssignedShiftStartColumn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.AssignedShiftEndColumn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lstShiftRequests = New System.Windows.Forms.ListView()
        Me.cboDateSubmitted = New System.Windows.Forms.DateTimePicker()
        Me.lblRequestsSubmitted = New System.Windows.Forms.Label()
        Me.AssignShiftErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdUpdateAgents = New System.Windows.Forms.Button()
        Me.lstAgents = New ShiftRequest.AgentListView()
        Me.grpAgentFilter.SuspendLayout()
        Me.grpReviewShiftRequestsOnAgent.SuspendLayout()
        CType(Me.AssignShiftErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkGroupByTeams
        '
        Me.chkGroupByTeams.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkGroupByTeams.AutoSize = True
        Me.chkGroupByTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGroupByTeams.Location = New System.Drawing.Point(3, 667)
        Me.chkGroupByTeams.Name = "chkGroupByTeams"
        Me.chkGroupByTeams.Size = New System.Drawing.Size(98, 17)
        Me.chkGroupByTeams.TabIndex = 1
        Me.chkGroupByTeams.Text = "Guppér i teams"
        Me.chkGroupByTeams.UseVisualStyleBackColor = True
        '
        'grpAgentFilter
        '
        Me.grpAgentFilter.Controls.Add(Me.rdoAgentWithoutRequests)
        Me.grpAgentFilter.Controls.Add(Me.rdoAgentsWithRequests)
        Me.grpAgentFilter.Controls.Add(Me.rdoAllAgents)
        Me.grpAgentFilter.Location = New System.Drawing.Point(3, 5)
        Me.grpAgentFilter.Name = "grpAgentFilter"
        Me.grpAgentFilter.Size = New System.Drawing.Size(279, 98)
        Me.grpAgentFilter.TabIndex = 2
        Me.grpAgentFilter.TabStop = False
        Me.grpAgentFilter.Text = "Filter"
        '
        'rdoAgentWithoutRequests
        '
        Me.rdoAgentWithoutRequests.AutoSize = True
        Me.rdoAgentWithoutRequests.Location = New System.Drawing.Point(8, 70)
        Me.rdoAgentWithoutRequests.Name = "rdoAgentWithoutRequests"
        Me.rdoAgentWithoutRequests.Size = New System.Drawing.Size(191, 17)
        Me.rdoAgentWithoutRequests.TabIndex = 3
        Me.rdoAgentWithoutRequests.Text = "Vis medarbejdere der ikke har valgt"
        Me.rdoAgentWithoutRequests.UseVisualStyleBackColor = True
        '
        'rdoAgentsWithRequests
        '
        Me.rdoAgentsWithRequests.AutoSize = True
        Me.rdoAgentsWithRequests.Location = New System.Drawing.Point(8, 45)
        Me.rdoAgentsWithRequests.Name = "rdoAgentsWithRequests"
        Me.rdoAgentsWithRequests.Size = New System.Drawing.Size(168, 17)
        Me.rdoAgentsWithRequests.TabIndex = 3
        Me.rdoAgentsWithRequests.Text = "Vis medarbejdere der har valgt"
        Me.rdoAgentsWithRequests.UseVisualStyleBackColor = True
        '
        'rdoAllAgents
        '
        Me.rdoAllAgents.AutoSize = True
        Me.rdoAllAgents.Checked = True
        Me.rdoAllAgents.Location = New System.Drawing.Point(8, 20)
        Me.rdoAllAgents.Name = "rdoAllAgents"
        Me.rdoAllAgents.Size = New System.Drawing.Size(58, 17)
        Me.rdoAllAgents.TabIndex = 3
        Me.rdoAllAgents.TabStop = True
        Me.rdoAllAgents.Text = "Vis alle"
        Me.rdoAllAgents.UseVisualStyleBackColor = True
        '
        'grpReviewShiftRequestsOnAgent
        '
        Me.grpReviewShiftRequestsOnAgent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grpReviewShiftRequestsOnAgent.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.AssignShift)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.cmdDeleteAssignedShift)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.Label1)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.lstAssignedShifts)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.lstShiftRequests)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.cboDateSubmitted)
        Me.grpReviewShiftRequestsOnAgent.Controls.Add(Me.lblRequestsSubmitted)
        Me.grpReviewShiftRequestsOnAgent.Location = New System.Drawing.Point(304, 5)
        Me.grpReviewShiftRequestsOnAgent.Name = "grpReviewShiftRequestsOnAgent"
        Me.grpReviewShiftRequestsOnAgent.Size = New System.Drawing.Size(418, 689)
        Me.grpReviewShiftRequestsOnAgent.TabIndex = 3
        Me.grpReviewShiftRequestsOnAgent.TabStop = False
        Me.grpReviewShiftRequestsOnAgent.Text = "Vagtønsker"
        '
        'AssignShift
        '
        Me.AssignShift.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AssignShift.Location = New System.Drawing.Point(26, 318)
        Me.AssignShift.Name = "AssignShift"
        Me.AssignShift.Size = New System.Drawing.Size(350, 155)
        Me.AssignShift.TabIndex = 9
        '
        'cmdDeleteAssignedShift
        '
        Me.cmdDeleteAssignedShift.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDeleteAssignedShift.Enabled = False
        Me.cmdDeleteAssignedShift.Location = New System.Drawing.Point(241, 647)
        Me.cmdDeleteAssignedShift.Name = "cmdDeleteAssignedShift"
        Me.cmdDeleteAssignedShift.Size = New System.Drawing.Size(100, 23)
        Me.cmdDeleteAssignedShift.TabIndex = 8
        Me.cmdDeleteAssignedShift.Text = "Slet"
        Me.cmdDeleteAssignedShift.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(74, 489)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 14)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Tildelte vagter:"
        '
        'lstAssignedShifts
        '
        Me.lstAssignedShifts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstAssignedShifts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.AssignedShiftDayColumn, Me.AssignedShiftStartColumn, Me.AssignedShiftEndColumn})
        Me.lstAssignedShifts.FullRowSelect = True
        Me.lstAssignedShifts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstAssignedShifts.HideSelection = False
        Me.lstAssignedShifts.Location = New System.Drawing.Point(77, 506)
        Me.lstAssignedShifts.MultiSelect = False
        Me.lstAssignedShifts.Name = "lstAssignedShifts"
        Me.lstAssignedShifts.ShowGroups = False
        Me.lstAssignedShifts.Size = New System.Drawing.Size(264, 135)
        Me.lstAssignedShifts.TabIndex = 6
        Me.lstAssignedShifts.UseCompatibleStateImageBehavior = False
        Me.lstAssignedShifts.View = System.Windows.Forms.View.Details
        '
        'AssignedShiftDayColumn
        '
        Me.AssignedShiftDayColumn.Text = "Dag"
        Me.AssignedShiftDayColumn.Width = 120
        '
        'AssignedShiftStartColumn
        '
        Me.AssignedShiftStartColumn.Text = "Start"
        Me.AssignedShiftStartColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.AssignedShiftStartColumn.Width = 70
        '
        'AssignedShiftEndColumn
        '
        Me.AssignedShiftEndColumn.Text = "Slut"
        Me.AssignedShiftEndColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.AssignedShiftEndColumn.Width = 70
        '
        'lstShiftRequests
        '
        Me.lstShiftRequests.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstShiftRequests.FullRowSelect = True
        Me.lstShiftRequests.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstShiftRequests.HideSelection = False
        Me.lstShiftRequests.Location = New System.Drawing.Point(26, 53)
        Me.lstShiftRequests.MultiSelect = False
        Me.lstShiftRequests.Name = "lstShiftRequests"
        Me.lstShiftRequests.ShowGroups = False
        Me.lstShiftRequests.Size = New System.Drawing.Size(367, 259)
        Me.lstShiftRequests.TabIndex = 2
        Me.lstShiftRequests.UseCompatibleStateImageBehavior = False
        Me.lstShiftRequests.View = System.Windows.Forms.View.Details
        '
        'cboDateSubmitted
        '
        Me.cboDateSubmitted.Enabled = False
        Me.cboDateSubmitted.Location = New System.Drawing.Point(211, 24)
        Me.cboDateSubmitted.Name = "cboDateSubmitted"
        Me.cboDateSubmitted.Size = New System.Drawing.Size(149, 22)
        Me.cboDateSubmitted.TabIndex = 1
        Me.cboDateSubmitted.Visible = False
        '
        'lblRequestsSubmitted
        '
        Me.lblRequestsSubmitted.AutoSize = True
        Me.lblRequestsSubmitted.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequestsSubmitted.Location = New System.Drawing.Point(22, 28)
        Me.lblRequestsSubmitted.Name = "lblRequestsSubmitted"
        Me.lblRequestsSubmitted.Size = New System.Drawing.Size(111, 14)
        Me.lblRequestsSubmitted.TabIndex = 0
        Me.lblRequestsSubmitted.Text = "Ønsker indsendt:"
        '
        'AssignShiftErrorProvider
        '
        Me.AssignShiftErrorProvider.ContainerControl = Me
        '
        'cmdUpdateAgents
        '
        Me.cmdUpdateAgents.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdUpdateAgents.Location = New System.Drawing.Point(182, 667)
        Me.cmdUpdateAgents.Name = "cmdUpdateAgents"
        Me.cmdUpdateAgents.Size = New System.Drawing.Size(100, 23)
        Me.cmdUpdateAgents.TabIndex = 4
        Me.cmdUpdateAgents.Text = "Opdater"
        Me.cmdUpdateAgents.UseVisualStyleBackColor = True
        '
        'lstAgents
        '
        Me.lstAgents.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstAgents.CheckBoxes = True
        Me.lstAgents.Filter = ShiftRequest.AgentListView.Filters.All
        Me.lstAgents.FullRowSelect = True
        Me.lstAgents.GroupByTeam = False
        Me.lstAgents.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstAgents.HideSelection = False
        Me.lstAgents.Location = New System.Drawing.Point(3, 110)
        Me.lstAgents.MultiSelect = False
        Me.lstAgents.Name = "lstAgents"
        Me.lstAgents.Size = New System.Drawing.Size(278, 551)
        Me.lstAgents.TabIndex = 0
        Me.lstAgents.UseCompatibleStateImageBehavior = False
        Me.lstAgents.View = System.Windows.Forms.View.Details
        '
        'ReviewShiftRequests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.cmdUpdateAgents)
        Me.Controls.Add(Me.grpReviewShiftRequestsOnAgent)
        Me.Controls.Add(Me.grpAgentFilter)
        Me.Controls.Add(Me.chkGroupByTeams)
        Me.Controls.Add(Me.lstAgents)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimumSize = New System.Drawing.Size(725, 540)
        Me.Name = "ReviewShiftRequests"
        Me.Size = New System.Drawing.Size(725, 697)
        Me.grpAgentFilter.ResumeLayout(False)
        Me.grpAgentFilter.PerformLayout()
        Me.grpReviewShiftRequestsOnAgent.ResumeLayout(False)
        Me.grpReviewShiftRequestsOnAgent.PerformLayout()
        CType(Me.AssignShiftErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstAgents As ShiftRequest.AgentListView
    Friend WithEvents chkGroupByTeams As System.Windows.Forms.CheckBox
    Friend WithEvents grpAgentFilter As System.Windows.Forms.GroupBox
    Friend WithEvents rdoAgentWithoutRequests As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAgentsWithRequests As System.Windows.Forms.RadioButton
    Friend WithEvents rdoAllAgents As System.Windows.Forms.RadioButton
    Friend WithEvents grpReviewShiftRequestsOnAgent As System.Windows.Forms.GroupBox
    Friend WithEvents lstShiftRequests As System.Windows.Forms.ListView
    Friend WithEvents cboDateSubmitted As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRequestsSubmitted As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lstAssignedShifts As System.Windows.Forms.ListView
    Friend WithEvents AssignedShiftDayColumn As System.Windows.Forms.ColumnHeader
    Friend WithEvents AssignedShiftStartColumn As System.Windows.Forms.ColumnHeader
    Friend WithEvents AssignedShiftEndColumn As System.Windows.Forms.ColumnHeader
    Friend WithEvents AssignShiftErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdDeleteAssignedShift As System.Windows.Forms.Button
    Friend WithEvents cmdUpdateAgents As System.Windows.Forms.Button
    Friend WithEvents AssignShift As ShiftRequest.AssignShift

End Class
