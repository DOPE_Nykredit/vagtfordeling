﻿Public Class ShiftSelection

    Public Enum DisplayDays
        StandardShifts
        FreeInterval
    End Enum

    Private tableLayout As TableLayoutPanel

    Private _DisplayDaysMode = DisplayDays.FreeInterval

    Public Property AddShiftTimeMarginToComboBoxes As Boolean

    Public Property DisplayDaysMode As DisplayDays
        Get
            Return _DisplayDaysMode
        End Get
        Set(ByVal value As DisplayDays)
            If value <> _DisplayDaysMode Then
                _DisplayDaysMode = value
                Me.SetupControl()
            End If
        End Set
    End Property

    Public Property SplitDays As Boolean

    Public Event TimeSelectedOnDay(ByVal shiftDay As ShiftDay, ByVal time As ShiftTime, ByVal shiftTimeStyle As ShiftTimeComboBox.ShiftTimeStyles)

    Private Sub WorkDayTimeSelection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SetupControl()
    End Sub

    Private Sub SetupControl()
        Dim workDayDefinitions As List(Of WorkDayDefinition) = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign

        workDayDefinitions.Sort()

        Me.Controls.Clear()

        Me.tableLayout = New TableLayoutPanel() With {.Padding = New Padding(0),
                                                      .AutoSize = True,
                                                      .AutoSizeMode = Windows.Forms.AutoSizeMode.GrowAndShrink}

        If _DisplayDaysMode = DisplayDays.StandardShifts Then
            Me.tableLayout.ColumnCount = 1

            Me.tableLayout.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 100))
        ElseIf _DisplayDaysMode = DisplayDays.FreeInterval Then
            Me.tableLayout.ColumnCount = 4

            Me.tableLayout.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
            Me.tableLayout.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
            Me.tableLayout.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
            Me.tableLayout.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
        End If

        If workDayDefinitions.Count > 0 Then
            For i = 0 To workDayDefinitions.Count - 1
                Me.addRowToTableLayout(workDayDefinitions(i), i)
            Next
        End If

        Me.Controls.Add(Me.tableLayout)
    End Sub

    Private Sub addRowToTableLayout(ByVal workDayDefinition As WorkDayDefinition, ByVal dayIndex As Integer)
        If _DisplayDaysMode = DisplayDays.StandardShifts Then
            Dim cboStandardShift As New StandardShiftComboBox(workDayDefinition.ShiftDay) With {.Margin = New Padding(25, 10, 25, 11),
                                                                                                .Width = 100,
                                                                                                .Anchor = CType((AnchorStyles.Bottom Or
                                                                                                                 AnchorStyles.Right Or
                                                                                                                 AnchorStyles.Left Or
                                                                                                                 AnchorStyles.Top), System.Windows.Forms.AnchorStyles)}

            AddHandler cboStandardShift.StandardShiftSelected, AddressOf Me.cboStandardShift_StandardShiftSelected

            Me.tableLayout.Controls.Add(cboStandardShift, 0, dayIndex)
        ElseIf _DisplayDaysMode = DisplayDays.FreeInterval Then
            Dim cboTimeStart As New ShiftTimeComboBox(workDayDefinition.ShiftDay) With {.Size = New Size(80, 22),
                                                                                        .Margin = New Padding(10, 10, 5, 10),
                                                                                        .AddShiftTimeMargin = Me.AddShiftTimeMarginToComboBoxes}

            Dim cboTimeEnd As New ShiftTimeComboBox(workDayDefinition.ShiftDay) With {.Size = New Size(80, 22),
                                                                                      .Margin = New Padding(5, 10, 10, 10),
                                                                                      .AddShiftTimeMargin = Me.AddShiftTimeMarginToComboBoxes}

            cboTimeStart.setEndTimeComboBox(cboTimeEnd)

            Dim lblDash As New Label() With {.Text = "til",
                                             .Margin = New Padding(3, 14, 3, 10),
                                             .AutoSize = True,
                                             .Tag = workDayDefinition.ShiftDay}

            Dim cmdClear As New Button() With {.Image = My.Resources._Error,
                                               .Margin = New Padding(9),
                                               .Size = New Size(24, 24),
                                               .Tag = workDayDefinition.ShiftDay}

            AddHandler cmdClear.Click, AddressOf Me.cmdClear_Click

            AddHandler cboTimeStart.ShiftTimeSelected, AddressOf Me.cboShiftTime_ShiftTimeSelected
            AddHandler cboTimeEnd.ShiftTimeSelected, AddressOf Me.cboShiftTime_ShiftTimeSelected

            Me.tableLayout.Controls.Add(cboTimeStart, 0, dayIndex)
            Me.tableLayout.Controls.Add(lblDash, 1, dayIndex)
            Me.tableLayout.Controls.Add(cboTimeEnd, 2, dayIndex)
            Me.tableLayout.Controls.Add(cmdClear, 3, dayIndex)
        End If
    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each c In Me.tableLayout.Controls
            If TypeOf c Is ShiftTimeComboBox AndAlso CType(c, ShiftTimeComboBox).ShiftDay.Equals(CType(sender, Button).Tag) Then
                CType(c, ShiftTimeComboBox).SelectedIndex = -1
            End If
        Next
    End Sub
    Public Sub SetShiftTime(ByVal day As ShiftDay, ByVal timeSpan As ShiftTimeSpan)
        For Each c In Me.tableLayout.Controls
            If TypeOf c Is ShiftTimeComboBox AndAlso CType(c, ShiftTimeComboBox).ShiftDay.Equals(day) Then
                Dim cboShiftTime As ShiftTimeComboBox = CType(c, ShiftTimeComboBox)

                If cboShiftTime.ShiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift Then
                    cboShiftTime.SelectedShiftTime = timeSpan.StartTime
                Else
                    cboShiftTime.SelectedShiftTime = timeSpan.EndTime
                End If
            End If
        Next
    End Sub
    Public Sub SetStandardShift(ByVal day As ShiftDay, ByVal shift As StandardShift)
        For Each c In Me.tableLayout.Controls
            If TypeOf c Is StandardShiftComboBox AndAlso CType(c, StandardShiftComboBox).Workday.Equals(day) Then
                CType(c, StandardShiftComboBox).SelectedShift = shift
            End If
        Next
    End Sub

    Public Sub ClearAllComboBoxes()
        For Each c In Me.tableLayout.Controls
            If TypeOf c Is ComboBox Then
                CType(c, ComboBox).SelectedIndex = -1
            End If
        Next
    End Sub

    Private Sub cboShiftTime_ShiftTimeSelected(ByVal sender As Object, ByVal time As ShiftTime)
        Dim cboShiftTime = CType(sender, ShiftTimeComboBox)

        RaiseEvent TimeSelectedOnDay(cboShiftTime.ShiftDay, cboShiftTime.SelectedShiftTime, cboShiftTime.ShiftTimeStyle)
    End Sub
    Private Sub cboStandardShift_StandardShiftSelected(ByVal sender As Object, ByVal shift As StandardShift)
        Dim cboStandardShift = CType(sender, StandardShiftComboBox)

        If shift Is Nothing Then
            RaiseEvent TimeSelectedOnDay(cboStandardShift.Workday, Nothing, ShiftTimeComboBox.ShiftTimeStyles.StartOfShift)
            RaiseEvent TimeSelectedOnDay(cboStandardShift.Workday, Nothing, ShiftTimeComboBox.ShiftTimeStyles.EndOfShift)
        Else
            RaiseEvent TimeSelectedOnDay(cboStandardShift.Workday, shift.TimeStart, ShiftTimeComboBox.ShiftTimeStyles.StartOfShift)
            RaiseEvent TimeSelectedOnDay(cboStandardShift.Workday, shift.TimeEnd, ShiftTimeComboBox.ShiftTimeStyles.EndOfShift)
        End If
    End Sub

    Public Sub SetShiftTimeConstraints(ByVal shiftDay As ShiftDay, ByVal minTime As ShiftTime, ByVal maxTime As ShiftTime, ByVal shiftTimeStyle As ShiftTimeComboBox.ShiftTimeStyles)
        For Each c In Me.tableLayout.Controls
            If TypeOf c Is ShiftTimeComboBox Then
                Dim cboShiftTime = CType(c, ShiftTimeComboBox)

                If cboShiftTime.ShiftTimeStyle = shiftTimeStyle And cboShiftTime.ShiftDay.Equals(shiftDay) Then
                    cboShiftTime.MinTime = minTime
                    cboShiftTime.MaxTime = maxTime
                End If
            End If
        Next
    End Sub

    Public Function getSelectedStandardShift(ByVal day As ShiftDay) As StandardShift
        Dim selectedStandardShift As StandardShift = Nothing

        For Each c In Me.tableLayout.Controls
            If TypeOf c Is StandardShiftComboBox AndAlso CType(c, StandardShiftComboBox).Workday.Equals(day) Then
                selectedStandardShift = CType(c, StandardShiftComboBox).SelectedShift
            End If
        Next

        Return selectedStandardShift
    End Function
    Public Function getSelectedShiftTimeSpan(ByVal day As ShiftDay) As ShiftTimeSpan
        Dim startTime As ShiftTime = Nothing
        Dim endTime As ShiftTime = Nothing

        For Each c In Me.tableLayout.Controls
            If TypeOf c Is ShiftTimeComboBox Then
                Dim cboShiftTime = CType(c, ShiftTimeComboBox)

                If cboShiftTime.ShiftDay.Equals(day) Then
                    If cboShiftTime.ShiftTimeStyle = ShiftTimeComboBox.ShiftTimeStyles.StartOfShift Then
                        startTime = cboShiftTime.SelectedShiftTime
                    Else
                        endTime = cboShiftTime.SelectedShiftTime
                    End If
                End If
            End If
        Next

        If startTime Is Nothing Or endTime Is Nothing Then
            Return Nothing
        Else
            Return New ShiftTimeSpan(startTime, endTime)
        End If
    End Function
End Class
