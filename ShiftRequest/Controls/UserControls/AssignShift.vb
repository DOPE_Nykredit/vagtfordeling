﻿Public Class AssignShift

    Private _Agent As Agent
    Private _RequestedShift As RequestedShift
    Private _AssignedShift As AssignedShift

    Private isUpdatingControl As Boolean

    Public Event NewShiftAssigned(ByVal shift As AssignedShift)
    Public Event AssignedShiftUpdated(ByVal assignedShift As AssignedShift)

    Public Sub UpdateCboAssignShiftDay()
        Me.cboAssignShiftDay.Items.Clear()

        Me.cboAssignShiftDay.DisplayMember = "DayDescription"
        Me.cboAssignShiftDay.ValueMember = "Day"

        For Each w In My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
            If w.DayStart IsNot Nothing Then Me.cboAssignShiftDay.Items.Add(w.ShiftDay)
        Next
    End Sub

    Private Sub cboAssignShiftDay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssignShiftDay.SelectedIndexChanged
        Me.cboAssignShiftStart.ShiftDay = Me.cboAssignShiftDay.SelectedItem
        Me.cboAssignShiftEnd.ShiftDay = Me.cboAssignShiftDay.SelectedItem

        If Not isUpdatingControl Then
            If Me.cboAssignShiftDay.SelectedIndex >= 0 Then
                Me.UpdateCboShiftTimeLimits()

                Me.SetCmdSaveAssignedShiftEnabledStatus()
            Else
                Me.cboAssignShiftStart.Items.Clear()
                Me.cboAssignShiftEnd.Items.Clear()

                Me.cmdSaveAssignedShift.Enabled = False
            End If
        End If
    End Sub
    Private Sub cboAssignShiftStart_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssignShiftStart.SelectedIndexChanged
        If Not isUpdatingControl Then Me.SetCmdSaveAssignedShiftEnabledStatus()
    End Sub
    Private Sub cboAssignShiftEnd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssignShiftEnd.SelectedIndexChanged
        If Not isUpdatingControl Then Me.SetCmdSaveAssignedShiftEnabledStatus()
    End Sub
    Private Function ValidateShiftAssignment() As Boolean
        Dim validation As Boolean = True

        Me.AssignShiftErrorProvider.Clear()

        If Me.cboAssignShiftDay.SelectedIndex = -1 Then
            Me.AssignShiftErrorProvider.SetError(Me.cboAssignShiftDay, "Vælg en dag.")
            validation = False
        End If

        If Me.cboAssignShiftStart.SelectedIndex = -1 Then
            Me.AssignShiftErrorProvider.SetError(Me.cboAssignShiftStart, "Vælg starttid.")
            validation = False
        End If

        If Me.cboAssignShiftEnd.SelectedIndex = -1 Then
            Me.AssignShiftErrorProvider.SetError(Me.cboAssignShiftEnd, "Vælg sluttid.")
            validation = False
        End If

        If Me.cboAssignShiftStart.SelectedIndex > -1 And Me.cboAssignShiftEnd.SelectedIndex > -1 Then
            If Me.cboAssignShiftStart.SelectedShiftTime >= Me.cboAssignShiftEnd.SelectedShiftTime Then
                Me.AssignShiftErrorProvider.SetError(Me.cboAssignShiftEnd, "Sluttid skal være senere end starttid.")
                validation = False
            End If
        End If

        Return validation
    End Function
    Private Sub SetCmdSaveAssignedShiftEnabledStatus()
        Dim existingAssignedShift As AssignedShift = My.Application.AssignedShiftsInSelectedCampaign.Find(Function(a) a.AgentId = _Agent.Id And a.Day.Equals(Me.cboAssignShiftDay.SelectedItem))

        If _AssignedShift IsNot Nothing Or existingAssignedShift Is Nothing Then
            If cboAssignShiftDay.SelectedIndex >= 0 And
               cboAssignShiftStart.SelectedIndex >= 0 And
               cboAssignShiftEnd.SelectedIndex >= 0 Then
                Me.cmdSaveAssignedShift.Enabled = True
            Else
                Me.cmdSaveAssignedShift.Enabled = False
            End If
        Else
            Me.cmdSaveAssignedShift.Enabled = False
        End If
    End Sub
    Private Sub cmdSaveAssignedShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveAssignedShift.Click
        If Me.ValidateShiftAssignment() = False Then Exit Sub

        Dim conn As New ShiftRequestConnection

        If _RequestedShift IsNot Nothing Then
            Dim newShift As New AssignedShift(_Agent.Id,
                                              CType(Me.cboAssignShiftDay.SelectedItem, ShiftDay).Day,
                                              New ShiftTimeSpan(Me.cboAssignShiftStart.SelectedShiftTime,
                                                                Me.cboAssignShiftEnd.SelectedShiftTime))

            Try
                newShift.save(conn)

                RaiseEvent NewShiftAssigned(newShift)
            Catch ex As InvalidOperationException
                MsgBox("Der findes allerede en vagt for " & _Agent.Initials & " på den pågældende dag i tidsrummet.", MsgBoxStyle.OkOnly, "Bemærk")
            End Try
        Else
            _AssignedShift.Day = Me.cboAssignShiftDay.SelectedItem
            _AssignedShift.ShiftSpan = New ShiftTimeSpan(Me.cboAssignShiftStart.SelectedShiftTime,
                                                         Me.cboAssignShiftEnd.SelectedShiftTime)

            _AssignedShift.save(conn)

            RaiseEvent AssignedShiftUpdated(_AssignedShift)
        End If

        conn.CloseConnection()

        Me.Clear()
    End Sub

    Public Sub Clear()
        _RequestedShift = Nothing
        _AssignedShift = Nothing

        isUpdatingControl = True

        Me.grpAssignShift.Enabled = False

        _Agent = Nothing

        Me.grpAssignShift.Text = "Tildel vagt"

        Me.chkAssignFromShiftRequest.Checked = False

        Me.cboAssignShiftDay.SelectedIndex = -1

        Me.cboAssignShiftStart.Items.Clear()
        Me.cboAssignShiftEnd.Items.Clear()

        isUpdatingControl = False
    End Sub
    Public Sub DisplayRequestedShift(ByVal reqShift As RequestedShift)
        If reqShift Is Nothing Then Throw New ArgumentNullException("Requested shift cannot be nothing.")

        _RequestedShift = reqShift
        _AssignedShift = Nothing

        _Agent = My.Application.AgentsInSelectedCampaign.First(Function(a) a.Id = reqShift.AgentId)

        isUpdatingControl = True

        Me.grpAssignShift.Enabled = True
        Me.grpAssignShift.Text = "Tildel vagt - " & _Agent.Initials

        Me.chkAssignFromShiftRequest.Checked = True

        Me.cboAssignShiftDay.SelectedItem = reqShift.Day
        Me.cboAssignShiftDay.Enabled = False

        Me.UpdateCboShiftTimeLimits()

        If reqShift.PrimaryRequest IsNot Nothing Then
            Me.cboAssignShiftStart.SelectedShiftTime = reqShift.PrimaryRequest.StartTime
            Me.cboAssignShiftEnd.SelectedShiftTime = reqShift.PrimaryRequest.EndTime
        Else
            Me.cboAssignShiftStart.SelectedShiftTime = reqShift.SecondaryRequest.StartTime
            Me.cboAssignShiftEnd.SelectedShiftTime = reqShift.SecondaryRequest.EndTime
        End If

        Me.SetCmdSaveAssignedShiftEnabledStatus()

        isUpdatingControl = False
    End Sub
    Public Sub DisplayAssignedShift(ByVal assignedShift As AssignedShift)
        If assignedShift Is Nothing Then Throw New ArgumentNullException("AssignedShift cannot be nothing.")

        _RequestedShift = Nothing
        _AssignedShift = assignedShift

        _Agent = My.Application.AgentsInSelectedCampaign.First(Function(a) a.Id = assignedShift.AgentId)

        isUpdatingControl = True

        Me.grpAssignShift.Enabled = True
        Me.grpAssignShift.Text = "Rediger vagt - " & _Agent.Initials

        Dim shiftRequest = My.Application.ShiftRequestsInSelectedCampaign.Find(Function(r) r.AgentId = _Agent.Id And r.Day.Equals(assignedShift.Day))

        If shiftRequest Is Nothing OrElse shiftRequest.MaxTimeSpan.IsInInterval(assignedShift.ShiftSpan) = False Then
            Me.chkAssignFromShiftRequest.Checked = False
            Me.cboAssignShiftDay.Enabled = True
        Else
            Me.chkAssignFromShiftRequest.Checked = True
            Me.cboAssignShiftDay.Enabled = False
        End If

        Me.cboAssignShiftDay.SelectedItem = assignedShift.Day

        Me.UpdateCboShiftTimeLimits()

        Me.cboAssignShiftStart.SelectedShiftTime = assignedShift.ShiftSpan.StartTime
        Me.cboAssignShiftEnd.SelectedShiftTime = assignedShift.ShiftSpan.EndTime

        Me.SetCmdSaveAssignedShiftEnabledStatus()

        isUpdatingControl = False
    End Sub

    Private Sub chkAssignShiftRequest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAssignFromShiftRequest.CheckedChanged
        If Not isUpdatingControl Then
            If Me.chkAssignFromShiftRequest.Checked Then
                Me.cboAssignShiftDay.Enabled = False

                If _AssignedShift IsNot Nothing Then
                    Me.DisplayAssignedShift(_AssignedShift)
                Else
                    Me.DisplayRequestedShift(_RequestedShift)
                End If
            Else
                Me.cboAssignShiftDay.Enabled = True
            End If

            Me.UpdateCboShiftTimeLimits()
        End If
    End Sub
    Private Sub UpdateCboShiftTimeLimits()
        If Me.cboAssignShiftDay.SelectedIndex >= 0 Then
            If Me.chkAssignFromShiftRequest.Checked Then
                Dim shiftRequest = My.Application.ShiftRequestsInSelectedCampaign.Find(Function(r) r.AgentId = _Agent.Id And r.Day.Equals(Me.cboAssignShiftDay.SelectedItem))

                If shiftRequest IsNot Nothing Then
                    Me.cboAssignShiftStart.Enabled = True
                    Me.cboAssignShiftEnd.Enabled = True

                    Me.cboAssignShiftStart.MinTime = shiftRequest.MaxTimeSpan.StartTime
                    Me.cboAssignShiftStart.MaxTime = shiftRequest.MaxTimeSpan.EndTime

                    Me.cboAssignShiftEnd.MinTime = shiftRequest.MaxTimeSpan.StartTime
                    Me.cboAssignShiftEnd.MaxTime = shiftRequest.MaxTimeSpan.EndTime
                Else
                    Me.cboAssignShiftStart.SelectedIndex = -1
                    Me.cboAssignShiftStart.Enabled = False

                    Me.cboAssignShiftEnd.SelectedIndex = -1
                    Me.cboAssignShiftEnd.Enabled = False
                End If
            Else
                Me.cboAssignShiftStart.Enabled = True
                Me.cboAssignShiftEnd.Enabled = True

                Me.cboAssignShiftStart.MinTime = Nothing
                Me.cboAssignShiftStart.MaxTime = Nothing

                Me.cboAssignShiftEnd.MinTime = Nothing
                Me.cboAssignShiftEnd.MaxTime = Nothing
            End If
        Else
            Me.cboAssignShiftStart.SelectedIndex = -1
            Me.cboAssignShiftStart.Enabled = False

            Me.cboAssignShiftEnd.SelectedIndex = -1
            Me.cboAssignShiftEnd.Enabled = False
        End If
    End Sub

    Private Sub AssignShift_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.UpdateCboAssignShiftDay()
        Me.cboAssignShiftStart.setEndTimeComboBox(Me.cboAssignShiftEnd)
    End Sub
End Class
