﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AssignShift
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grpAssignShift = New System.Windows.Forms.GroupBox()
        Me.cmdSaveAssignedShift = New System.Windows.Forms.Button()
        Me.cboAssignShiftDay = New System.Windows.Forms.ComboBox()
        Me.lblAssignShiftDay = New System.Windows.Forms.Label()
        Me.lblAssignedShiftEnd = New System.Windows.Forms.Label()
        Me.lblAssignShiftStart = New System.Windows.Forms.Label()
        Me.chkAssignFromShiftRequest = New System.Windows.Forms.CheckBox()
        Me.AssignShiftErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cboAssignShiftEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboAssignShiftStart = New ShiftRequest.ShiftTimeComboBox()
        Me.grpAssignShift.SuspendLayout()
        CType(Me.AssignShiftErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpAssignShift
        '
        Me.grpAssignShift.Controls.Add(Me.cmdSaveAssignedShift)
        Me.grpAssignShift.Controls.Add(Me.cboAssignShiftDay)
        Me.grpAssignShift.Controls.Add(Me.lblAssignShiftDay)
        Me.grpAssignShift.Controls.Add(Me.cboAssignShiftEnd)
        Me.grpAssignShift.Controls.Add(Me.lblAssignedShiftEnd)
        Me.grpAssignShift.Controls.Add(Me.lblAssignShiftStart)
        Me.grpAssignShift.Controls.Add(Me.chkAssignFromShiftRequest)
        Me.grpAssignShift.Controls.Add(Me.cboAssignShiftStart)
        Me.grpAssignShift.Enabled = False
        Me.grpAssignShift.Location = New System.Drawing.Point(3, 3)
        Me.grpAssignShift.Name = "grpAssignShift"
        Me.grpAssignShift.Size = New System.Drawing.Size(345, 149)
        Me.grpAssignShift.TabIndex = 6
        Me.grpAssignShift.TabStop = False
        Me.grpAssignShift.Text = "Tildel vagt"
        '
        'cmdSaveAssignedShift
        '
        Me.cmdSaveAssignedShift.Enabled = False
        Me.cmdSaveAssignedShift.Location = New System.Drawing.Point(233, 117)
        Me.cmdSaveAssignedShift.Name = "cmdSaveAssignedShift"
        Me.cmdSaveAssignedShift.Size = New System.Drawing.Size(100, 23)
        Me.cmdSaveAssignedShift.TabIndex = 11
        Me.cmdSaveAssignedShift.Text = "Gem"
        Me.cmdSaveAssignedShift.UseVisualStyleBackColor = True
        '
        'cboAssignShiftDay
        '
        Me.cboAssignShiftDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssignShiftDay.FormattingEnabled = True
        Me.cboAssignShiftDay.Location = New System.Drawing.Point(110, 51)
        Me.cboAssignShiftDay.Name = "cboAssignShiftDay"
        Me.cboAssignShiftDay.Size = New System.Drawing.Size(121, 21)
        Me.cboAssignShiftDay.TabIndex = 10
        '
        'lblAssignShiftDay
        '
        Me.lblAssignShiftDay.AutoSize = True
        Me.lblAssignShiftDay.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssignShiftDay.Location = New System.Drawing.Point(44, 54)
        Me.lblAssignShiftDay.Name = "lblAssignShiftDay"
        Me.lblAssignShiftDay.Size = New System.Drawing.Size(35, 14)
        Me.lblAssignShiftDay.TabIndex = 9
        Me.lblAssignShiftDay.Text = "Dag:"
        '
        'lblAssignedShiftEnd
        '
        Me.lblAssignedShiftEnd.AutoSize = True
        Me.lblAssignedShiftEnd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssignedShiftEnd.Location = New System.Drawing.Point(44, 112)
        Me.lblAssignedShiftEnd.Name = "lblAssignedShiftEnd"
        Me.lblAssignedShiftEnd.Size = New System.Drawing.Size(36, 14)
        Me.lblAssignedShiftEnd.TabIndex = 7
        Me.lblAssignedShiftEnd.Text = "Slut:"
        '
        'lblAssignShiftStart
        '
        Me.lblAssignShiftStart.AutoSize = True
        Me.lblAssignShiftStart.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssignShiftStart.Location = New System.Drawing.Point(44, 84)
        Me.lblAssignShiftStart.Name = "lblAssignShiftStart"
        Me.lblAssignShiftStart.Size = New System.Drawing.Size(43, 14)
        Me.lblAssignShiftStart.TabIndex = 6
        Me.lblAssignShiftStart.Text = "Start:"
        '
        'chkAssignFromShiftRequest
        '
        Me.chkAssignFromShiftRequest.AutoSize = True
        Me.chkAssignFromShiftRequest.Location = New System.Drawing.Point(33, 24)
        Me.chkAssignFromShiftRequest.Name = "chkAssignFromShiftRequest"
        Me.chkAssignFromShiftRequest.Size = New System.Drawing.Size(87, 17)
        Me.chkAssignFromShiftRequest.TabIndex = 5
        Me.chkAssignFromShiftRequest.Text = "Ud fra ønske"
        Me.chkAssignFromShiftRequest.UseVisualStyleBackColor = True
        '
        'AssignShiftErrorProvider
        '
        Me.AssignShiftErrorProvider.ContainerControl = Me
        '
        'cboAssignShiftEnd
        '
        Me.cboAssignShiftEnd.AddShiftTimeMargin = True
        Me.cboAssignShiftEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssignShiftEnd.FormattingEnabled = True
        Me.cboAssignShiftEnd.Location = New System.Drawing.Point(110, 109)
        Me.cboAssignShiftEnd.MaxDropDownItems = 16
        Me.cboAssignShiftEnd.Name = "cboAssignShiftEnd"
        Me.cboAssignShiftEnd.SelectedShiftTime = Nothing
        Me.cboAssignShiftEnd.ShiftDay = Nothing
        Me.cboAssignShiftEnd.Size = New System.Drawing.Size(80, 21)
        Me.cboAssignShiftEnd.TabIndex = 8
        '
        'cboAssignShiftStart
        '
        Me.cboAssignShiftStart.AddShiftTimeMargin = True
        Me.cboAssignShiftStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssignShiftStart.FormattingEnabled = True
        Me.cboAssignShiftStart.Location = New System.Drawing.Point(110, 81)
        Me.cboAssignShiftStart.MaxDropDownItems = 16
        Me.cboAssignShiftStart.Name = "cboAssignShiftStart"
        Me.cboAssignShiftStart.SelectedShiftTime = Nothing
        Me.cboAssignShiftStart.ShiftDay = Nothing
        Me.cboAssignShiftStart.Size = New System.Drawing.Size(80, 21)
        Me.cboAssignShiftStart.TabIndex = 4
        '
        'AssignShift
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.grpAssignShift)
        Me.Name = "AssignShift"
        Me.Size = New System.Drawing.Size(350, 155)
        Me.grpAssignShift.ResumeLayout(False)
        Me.grpAssignShift.PerformLayout()
        CType(Me.AssignShiftErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpAssignShift As System.Windows.Forms.GroupBox
    Friend WithEvents cmdSaveAssignedShift As System.Windows.Forms.Button
    Friend WithEvents cboAssignShiftDay As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssignShiftDay As System.Windows.Forms.Label
    Friend WithEvents cboAssignShiftEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblAssignedShiftEnd As System.Windows.Forms.Label
    Friend WithEvents lblAssignShiftStart As System.Windows.Forms.Label
    Friend WithEvents chkAssignFromShiftRequest As System.Windows.Forms.CheckBox
    Friend WithEvents cboAssignShiftStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents AssignShiftErrorProvider As System.Windows.Forms.ErrorProvider

End Class
