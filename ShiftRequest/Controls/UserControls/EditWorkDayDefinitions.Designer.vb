﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditWorkDayDefinitions
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.chkSundayActive = New System.Windows.Forms.CheckBox()
        Me.chkSaturdayActive = New System.Windows.Forms.CheckBox()
        Me.chkFridayActive = New System.Windows.Forms.CheckBox()
        Me.chkThursdayActive = New System.Windows.Forms.CheckBox()
        Me.chkWednesdayActive = New System.Windows.Forms.CheckBox()
        Me.chkTuesdayActive = New System.Windows.Forms.CheckBox()
        Me.chkMondayActive = New System.Windows.Forms.CheckBox()
        Me.lblSundayDash = New System.Windows.Forms.Label()
        Me.lblSaturdayDash = New System.Windows.Forms.Label()
        Me.lblFridayDash = New System.Windows.Forms.Label()
        Me.lblThursdayDash = New System.Windows.Forms.Label()
        Me.lblWednesdayDash = New System.Windows.Forms.Label()
        Me.lbTuesdayDash = New System.Windows.Forms.Label()
        Me.lblMondayDash = New System.Windows.Forms.Label()
        Me.lblSunday = New System.Windows.Forms.Label()
        Me.lblSaturday = New System.Windows.Forms.Label()
        Me.lblFriday = New System.Windows.Forms.Label()
        Me.lblThursday = New System.Windows.Forms.Label()
        Me.lblWednesday = New System.Windows.Forms.Label()
        Me.lblTuesday = New System.Windows.Forms.Label()
        Me.lblMonday = New System.Windows.Forms.Label()
        Me.WorkdayIntervalErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtMondayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtTuesdayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtWednesdayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtThursdayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtFridayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtSaturdayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.txtSundayRequiredHours = New System.Windows.Forms.MaskedTextBox()
        Me.lblRequiredHours = New System.Windows.Forms.Label()
        Me.lblWorkdayStart = New System.Windows.Forms.Label()
        Me.lblWorkdayEnd = New System.Windows.Forms.Label()
        Me.cboSundayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboSundayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboSaturdayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboSaturdayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboFridayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboFridayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboThursdayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboThursdayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboWednesdayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboWednesdayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboTuesdayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboTuesdayStart = New ShiftRequest.ShiftTimeComboBox()
        Me.cboMondayEnd = New ShiftRequest.ShiftTimeComboBox()
        Me.cboMondayStart = New ShiftRequest.ShiftTimeComboBox()
        CType(Me.WorkdayIntervalErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(403, 300)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(117, 25)
        Me.cmdSave.TabIndex = 38
        Me.cmdSave.Text = "Gem"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'chkSundayActive
        '
        Me.chkSundayActive.AutoSize = True
        Me.chkSundayActive.Location = New System.Drawing.Point(324, 268)
        Me.chkSundayActive.Name = "chkSundayActive"
        Me.chkSundayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkSundayActive.TabIndex = 36
        Me.chkSundayActive.Text = "Aktiv"
        Me.chkSundayActive.UseVisualStyleBackColor = True
        '
        'chkSaturdayActive
        '
        Me.chkSaturdayActive.AutoSize = True
        Me.chkSaturdayActive.Location = New System.Drawing.Point(324, 231)
        Me.chkSaturdayActive.Name = "chkSaturdayActive"
        Me.chkSaturdayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkSaturdayActive.TabIndex = 32
        Me.chkSaturdayActive.Text = "Aktiv"
        Me.chkSaturdayActive.UseVisualStyleBackColor = True
        '
        'chkFridayActive
        '
        Me.chkFridayActive.AutoSize = True
        Me.chkFridayActive.Location = New System.Drawing.Point(324, 195)
        Me.chkFridayActive.Name = "chkFridayActive"
        Me.chkFridayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkFridayActive.TabIndex = 28
        Me.chkFridayActive.Text = "Aktiv"
        Me.chkFridayActive.UseVisualStyleBackColor = True
        '
        'chkThursdayActive
        '
        Me.chkThursdayActive.AutoSize = True
        Me.chkThursdayActive.Location = New System.Drawing.Point(324, 158)
        Me.chkThursdayActive.Name = "chkThursdayActive"
        Me.chkThursdayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkThursdayActive.TabIndex = 22
        Me.chkThursdayActive.Text = "Aktiv"
        Me.chkThursdayActive.UseVisualStyleBackColor = True
        '
        'chkWednesdayActive
        '
        Me.chkWednesdayActive.AutoSize = True
        Me.chkWednesdayActive.Location = New System.Drawing.Point(324, 122)
        Me.chkWednesdayActive.Name = "chkWednesdayActive"
        Me.chkWednesdayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkWednesdayActive.TabIndex = 16
        Me.chkWednesdayActive.Text = "Aktiv"
        Me.chkWednesdayActive.UseVisualStyleBackColor = True
        '
        'chkTuesdayActive
        '
        Me.chkTuesdayActive.AutoSize = True
        Me.chkTuesdayActive.Location = New System.Drawing.Point(324, 85)
        Me.chkTuesdayActive.Name = "chkTuesdayActive"
        Me.chkTuesdayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkTuesdayActive.TabIndex = 10
        Me.chkTuesdayActive.Text = "Aktiv"
        Me.chkTuesdayActive.UseVisualStyleBackColor = True
        '
        'chkMondayActive
        '
        Me.chkMondayActive.AutoSize = True
        Me.chkMondayActive.Location = New System.Drawing.Point(324, 48)
        Me.chkMondayActive.Name = "chkMondayActive"
        Me.chkMondayActive.Size = New System.Drawing.Size(53, 18)
        Me.chkMondayActive.TabIndex = 4
        Me.chkMondayActive.Text = "Aktiv"
        Me.chkMondayActive.UseVisualStyleBackColor = True
        '
        'lblSundayDash
        '
        Me.lblSundayDash.AutoSize = True
        Me.lblSundayDash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSundayDash.Location = New System.Drawing.Point(172, 269)
        Me.lblSundayDash.Name = "lblSundayDash"
        Me.lblSundayDash.Size = New System.Drawing.Size(18, 13)
        Me.lblSundayDash.TabIndex = 98
        Me.lblSundayDash.Text = "til"
        '
        'lblSaturdayDash
        '
        Me.lblSaturdayDash.AutoSize = True
        Me.lblSaturdayDash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaturdayDash.Location = New System.Drawing.Point(172, 233)
        Me.lblSaturdayDash.Name = "lblSaturdayDash"
        Me.lblSaturdayDash.Size = New System.Drawing.Size(18, 13)
        Me.lblSaturdayDash.TabIndex = 95
        Me.lblSaturdayDash.Text = "til"
        '
        'lblFridayDash
        '
        Me.lblFridayDash.AutoSize = True
        Me.lblFridayDash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFridayDash.Location = New System.Drawing.Point(172, 196)
        Me.lblFridayDash.Name = "lblFridayDash"
        Me.lblFridayDash.Size = New System.Drawing.Size(18, 13)
        Me.lblFridayDash.TabIndex = 26
        Me.lblFridayDash.Text = "til"
        '
        'lblThursdayDash
        '
        Me.lblThursdayDash.AutoSize = True
        Me.lblThursdayDash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblThursdayDash.Location = New System.Drawing.Point(172, 159)
        Me.lblThursdayDash.Name = "lblThursdayDash"
        Me.lblThursdayDash.Size = New System.Drawing.Size(18, 13)
        Me.lblThursdayDash.TabIndex = 20
        Me.lblThursdayDash.Text = "til"
        '
        'lblWednesdayDash
        '
        Me.lblWednesdayDash.AutoSize = True
        Me.lblWednesdayDash.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWednesdayDash.Location = New System.Drawing.Point(172, 123)
        Me.lblWednesdayDash.Name = "lblWednesdayDash"
        Me.lblWednesdayDash.Size = New System.Drawing.Size(18, 13)
        Me.lblWednesdayDash.TabIndex = 14
        Me.lblWednesdayDash.Text = "til"
        '
        'lbTuesdayDash
        '
        Me.lbTuesdayDash.AutoSize = True
        Me.lbTuesdayDash.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbTuesdayDash.Location = New System.Drawing.Point(172, 86)
        Me.lbTuesdayDash.Name = "lbTuesdayDash"
        Me.lbTuesdayDash.Size = New System.Drawing.Size(16, 14)
        Me.lbTuesdayDash.TabIndex = 8
        Me.lbTuesdayDash.Text = "til"
        '
        'lblMondayDash
        '
        Me.lblMondayDash.AutoSize = True
        Me.lblMondayDash.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMondayDash.Location = New System.Drawing.Point(172, 49)
        Me.lblMondayDash.Name = "lblMondayDash"
        Me.lblMondayDash.Size = New System.Drawing.Size(16, 14)
        Me.lblMondayDash.TabIndex = 2
        Me.lblMondayDash.Text = "til"
        '
        'lblSunday
        '
        Me.lblSunday.AutoSize = True
        Me.lblSunday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSunday.Location = New System.Drawing.Point(7, 269)
        Me.lblSunday.Name = "lblSunday"
        Me.lblSunday.Size = New System.Drawing.Size(54, 14)
        Me.lblSunday.TabIndex = 78
        Me.lblSunday.Text = "Søndag"
        '
        'lblSaturday
        '
        Me.lblSaturday.AutoSize = True
        Me.lblSaturday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaturday.Location = New System.Drawing.Point(7, 233)
        Me.lblSaturday.Name = "lblSaturday"
        Me.lblSaturday.Size = New System.Drawing.Size(50, 14)
        Me.lblSaturday.TabIndex = 77
        Me.lblSaturday.Text = "Lørdag"
        '
        'lblFriday
        '
        Me.lblFriday.AutoSize = True
        Me.lblFriday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFriday.Location = New System.Drawing.Point(7, 196)
        Me.lblFriday.Name = "lblFriday"
        Me.lblFriday.Size = New System.Drawing.Size(48, 14)
        Me.lblFriday.TabIndex = 24
        Me.lblFriday.Text = "Fredag"
        '
        'lblThursday
        '
        Me.lblThursday.AutoSize = True
        Me.lblThursday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblThursday.Location = New System.Drawing.Point(7, 159)
        Me.lblThursday.Name = "lblThursday"
        Me.lblThursday.Size = New System.Drawing.Size(56, 14)
        Me.lblThursday.TabIndex = 18
        Me.lblThursday.Text = "Torsdag"
        '
        'lblWednesday
        '
        Me.lblWednesday.AutoSize = True
        Me.lblWednesday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWednesday.Location = New System.Drawing.Point(7, 123)
        Me.lblWednesday.Name = "lblWednesday"
        Me.lblWednesday.Size = New System.Drawing.Size(53, 14)
        Me.lblWednesday.TabIndex = 12
        Me.lblWednesday.Text = "Onsdag"
        '
        'lblTuesday
        '
        Me.lblTuesday.AutoSize = True
        Me.lblTuesday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTuesday.Location = New System.Drawing.Point(7, 86)
        Me.lblTuesday.Name = "lblTuesday"
        Me.lblTuesday.Size = New System.Drawing.Size(51, 14)
        Me.lblTuesday.TabIndex = 6
        Me.lblTuesday.Text = "Tirsdag"
        '
        'lblMonday
        '
        Me.lblMonday.AutoSize = True
        Me.lblMonday.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonday.Location = New System.Drawing.Point(7, 49)
        Me.lblMonday.Name = "lblMonday"
        Me.lblMonday.Size = New System.Drawing.Size(60, 14)
        Me.lblMonday.TabIndex = 0
        Me.lblMonday.Text = "Mandag:"
        '
        'WorkdayIntervalErrorProvider
        '
        Me.WorkdayIntervalErrorProvider.ContainerControl = Me
        '
        'txtMondayRequiredHours
        '
        Me.txtMondayRequiredHours.AllowPromptAsInput = False
        Me.txtMondayRequiredHours.AsciiOnly = True
        Me.txtMondayRequiredHours.Enabled = False
        Me.txtMondayRequiredHours.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite
        Me.txtMondayRequiredHours.Location = New System.Drawing.Point(442, 46)
        Me.txtMondayRequiredHours.Mask = "000:00"
        Me.txtMondayRequiredHours.Name = "txtMondayRequiredHours"
        Me.txtMondayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtMondayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtMondayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtMondayRequiredHours.TabIndex = 5
        '
        'txtTuesdayRequiredHours
        '
        Me.txtTuesdayRequiredHours.Enabled = False
        Me.txtTuesdayRequiredHours.Location = New System.Drawing.Point(442, 83)
        Me.txtTuesdayRequiredHours.Mask = "000:00"
        Me.txtTuesdayRequiredHours.Name = "txtTuesdayRequiredHours"
        Me.txtTuesdayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtTuesdayRequiredHours.RejectInputOnFirstFailure = True
        Me.txtTuesdayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtTuesdayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtTuesdayRequiredHours.TabIndex = 11
        '
        'txtWednesdayRequiredHours
        '
        Me.txtWednesdayRequiredHours.Enabled = False
        Me.txtWednesdayRequiredHours.Location = New System.Drawing.Point(442, 120)
        Me.txtWednesdayRequiredHours.Mask = "000:00"
        Me.txtWednesdayRequiredHours.Name = "txtWednesdayRequiredHours"
        Me.txtWednesdayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtWednesdayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtWednesdayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtWednesdayRequiredHours.TabIndex = 17
        '
        'txtThursdayRequiredHours
        '
        Me.txtThursdayRequiredHours.Enabled = False
        Me.txtThursdayRequiredHours.Location = New System.Drawing.Point(442, 156)
        Me.txtThursdayRequiredHours.Mask = "000:00"
        Me.txtThursdayRequiredHours.Name = "txtThursdayRequiredHours"
        Me.txtThursdayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtThursdayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtThursdayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtThursdayRequiredHours.TabIndex = 23
        '
        'txtFridayRequiredHours
        '
        Me.txtFridayRequiredHours.Enabled = False
        Me.txtFridayRequiredHours.Location = New System.Drawing.Point(442, 193)
        Me.txtFridayRequiredHours.Mask = "000:00"
        Me.txtFridayRequiredHours.Name = "txtFridayRequiredHours"
        Me.txtFridayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtFridayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtFridayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtFridayRequiredHours.TabIndex = 29
        '
        'txtSaturdayRequiredHours
        '
        Me.txtSaturdayRequiredHours.Enabled = False
        Me.txtSaturdayRequiredHours.Location = New System.Drawing.Point(442, 229)
        Me.txtSaturdayRequiredHours.Mask = "000:00"
        Me.txtSaturdayRequiredHours.Name = "txtSaturdayRequiredHours"
        Me.txtSaturdayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtSaturdayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtSaturdayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtSaturdayRequiredHours.TabIndex = 33
        '
        'txtSundayRequiredHours
        '
        Me.txtSundayRequiredHours.Enabled = False
        Me.txtSundayRequiredHours.Location = New System.Drawing.Point(442, 266)
        Me.txtSundayRequiredHours.Mask = "000:00"
        Me.txtSundayRequiredHours.Name = "txtSundayRequiredHours"
        Me.txtSundayRequiredHours.PromptChar = Global.Microsoft.VisualBasic.ChrW(48)
        Me.txtSundayRequiredHours.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtSundayRequiredHours.Size = New System.Drawing.Size(49, 22)
        Me.txtSundayRequiredHours.TabIndex = 37
        '
        'lblRequiredHours
        '
        Me.lblRequiredHours.AutoSize = True
        Me.lblRequiredHours.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequiredHours.Location = New System.Drawing.Point(403, 20)
        Me.lblRequiredHours.Name = "lblRequiredHours"
        Me.lblRequiredHours.Size = New System.Drawing.Size(116, 14)
        Me.lblRequiredHours.TabIndex = 115
        Me.lblRequiredHours.Text = "Krav til timeantal:"
        '
        'lblWorkdayStart
        '
        Me.lblWorkdayStart.AutoSize = True
        Me.lblWorkdayStart.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkdayStart.Location = New System.Drawing.Point(105, 20)
        Me.lblWorkdayStart.Name = "lblWorkdayStart"
        Me.lblWorkdayStart.Size = New System.Drawing.Size(39, 14)
        Me.lblWorkdayStart.TabIndex = 116
        Me.lblWorkdayStart.Text = "Start"
        '
        'lblWorkdayEnd
        '
        Me.lblWorkdayEnd.AutoSize = True
        Me.lblWorkdayEnd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkdayEnd.Location = New System.Drawing.Point(228, 20)
        Me.lblWorkdayEnd.Name = "lblWorkdayEnd"
        Me.lblWorkdayEnd.Size = New System.Drawing.Size(32, 14)
        Me.lblWorkdayEnd.TabIndex = 117
        Me.lblWorkdayEnd.Text = "Slut"
        '
        'cboSundayEnd
        '
        Me.cboSundayEnd.AddShiftTimeMargin = True
        Me.cboSundayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSundayEnd.Enabled = False
        Me.cboSundayEnd.FormattingEnabled = True
        Me.cboSundayEnd.Location = New System.Drawing.Point(200, 266)
        Me.cboSundayEnd.Name = "cboSundayEnd"
        Me.cboSundayEnd.SelectedShiftTime = Nothing
        Me.cboSundayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboSundayEnd.TabIndex = 35
        '
        'cboSundayStart
        '
        Me.cboSundayStart.AddShiftTimeMargin = True
        Me.cboSundayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSundayStart.Enabled = False
        Me.cboSundayStart.FormattingEnabled = True
        Me.cboSundayStart.Location = New System.Drawing.Point(79, 266)
        Me.cboSundayStart.Name = "cboSundayStart"
        Me.cboSundayStart.SelectedShiftTime = Nothing
        Me.cboSundayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboSundayStart.TabIndex = 34
        '
        'cboSaturdayEnd
        '
        Me.cboSaturdayEnd.AddShiftTimeMargin = True
        Me.cboSaturdayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSaturdayEnd.Enabled = False
        Me.cboSaturdayEnd.FormattingEnabled = True
        Me.cboSaturdayEnd.Location = New System.Drawing.Point(200, 229)
        Me.cboSaturdayEnd.Name = "cboSaturdayEnd"
        Me.cboSaturdayEnd.SelectedShiftTime = Nothing
        Me.cboSaturdayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboSaturdayEnd.TabIndex = 31
        '
        'cboSaturdayStart
        '
        Me.cboSaturdayStart.AddShiftTimeMargin = True
        Me.cboSaturdayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSaturdayStart.Enabled = False
        Me.cboSaturdayStart.FormattingEnabled = True
        Me.cboSaturdayStart.Location = New System.Drawing.Point(79, 229)
        Me.cboSaturdayStart.Name = "cboSaturdayStart"
        Me.cboSaturdayStart.SelectedShiftTime = Nothing
        Me.cboSaturdayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboSaturdayStart.TabIndex = 30
        '
        'cboFridayEnd
        '
        Me.cboFridayEnd.AddShiftTimeMargin = True
        Me.cboFridayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFridayEnd.Enabled = False
        Me.cboFridayEnd.FormattingEnabled = True
        Me.cboFridayEnd.Location = New System.Drawing.Point(200, 193)
        Me.cboFridayEnd.Name = "cboFridayEnd"
        Me.cboFridayEnd.SelectedShiftTime = Nothing
        Me.cboFridayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboFridayEnd.TabIndex = 27
        '
        'cboFridayStart
        '
        Me.cboFridayStart.AddShiftTimeMargin = True
        Me.cboFridayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFridayStart.Enabled = False
        Me.cboFridayStart.FormattingEnabled = True
        Me.cboFridayStart.Location = New System.Drawing.Point(79, 193)
        Me.cboFridayStart.Name = "cboFridayStart"
        Me.cboFridayStart.SelectedShiftTime = Nothing
        Me.cboFridayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboFridayStart.TabIndex = 25
        '
        'cboThursdayEnd
        '
        Me.cboThursdayEnd.AddShiftTimeMargin = True
        Me.cboThursdayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboThursdayEnd.Enabled = False
        Me.cboThursdayEnd.FormattingEnabled = True
        Me.cboThursdayEnd.Location = New System.Drawing.Point(200, 156)
        Me.cboThursdayEnd.Name = "cboThursdayEnd"
        Me.cboThursdayEnd.SelectedShiftTime = Nothing
        Me.cboThursdayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboThursdayEnd.TabIndex = 21
        '
        'cboThursdayStart
        '
        Me.cboThursdayStart.AddShiftTimeMargin = True
        Me.cboThursdayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboThursdayStart.Enabled = False
        Me.cboThursdayStart.FormattingEnabled = True
        Me.cboThursdayStart.Location = New System.Drawing.Point(79, 156)
        Me.cboThursdayStart.Name = "cboThursdayStart"
        Me.cboThursdayStart.SelectedShiftTime = Nothing
        Me.cboThursdayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboThursdayStart.TabIndex = 19
        '
        'cboWednesdayEnd
        '
        Me.cboWednesdayEnd.AddShiftTimeMargin = True
        Me.cboWednesdayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWednesdayEnd.Enabled = False
        Me.cboWednesdayEnd.FormattingEnabled = True
        Me.cboWednesdayEnd.Location = New System.Drawing.Point(200, 119)
        Me.cboWednesdayEnd.Name = "cboWednesdayEnd"
        Me.cboWednesdayEnd.SelectedShiftTime = Nothing
        Me.cboWednesdayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboWednesdayEnd.TabIndex = 15
        '
        'cboWednesdayStart
        '
        Me.cboWednesdayStart.AddShiftTimeMargin = True
        Me.cboWednesdayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWednesdayStart.Enabled = False
        Me.cboWednesdayStart.FormattingEnabled = True
        Me.cboWednesdayStart.Location = New System.Drawing.Point(79, 119)
        Me.cboWednesdayStart.Name = "cboWednesdayStart"
        Me.cboWednesdayStart.SelectedShiftTime = Nothing
        Me.cboWednesdayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboWednesdayStart.TabIndex = 13
        '
        'cboTuesdayEnd
        '
        Me.cboTuesdayEnd.AddShiftTimeMargin = True
        Me.cboTuesdayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTuesdayEnd.Enabled = False
        Me.cboTuesdayEnd.FormattingEnabled = True
        Me.cboTuesdayEnd.Location = New System.Drawing.Point(200, 83)
        Me.cboTuesdayEnd.Name = "cboTuesdayEnd"
        Me.cboTuesdayEnd.SelectedShiftTime = Nothing
        Me.cboTuesdayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboTuesdayEnd.TabIndex = 9
        '
        'cboTuesdayStart
        '
        Me.cboTuesdayStart.AddShiftTimeMargin = True
        Me.cboTuesdayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTuesdayStart.Enabled = False
        Me.cboTuesdayStart.FormattingEnabled = True
        Me.cboTuesdayStart.Location = New System.Drawing.Point(79, 83)
        Me.cboTuesdayStart.Name = "cboTuesdayStart"
        Me.cboTuesdayStart.SelectedShiftTime = Nothing
        Me.cboTuesdayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboTuesdayStart.TabIndex = 7
        '
        'cboMondayEnd
        '
        Me.cboMondayEnd.AddShiftTimeMargin = True
        Me.cboMondayEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMondayEnd.Enabled = False
        Me.cboMondayEnd.FormattingEnabled = True
        Me.cboMondayEnd.Location = New System.Drawing.Point(200, 46)
        Me.cboMondayEnd.Name = "cboMondayEnd"
        Me.cboMondayEnd.SelectedShiftTime = Nothing
        Me.cboMondayEnd.Size = New System.Drawing.Size(80, 22)
        Me.cboMondayEnd.TabIndex = 3
        '
        'cboMondayStart
        '
        Me.cboMondayStart.AddShiftTimeMargin = True
        Me.cboMondayStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMondayStart.Enabled = False
        Me.cboMondayStart.FormattingEnabled = True
        Me.cboMondayStart.Location = New System.Drawing.Point(79, 46)
        Me.cboMondayStart.Name = "cboMondayStart"
        Me.cboMondayStart.SelectedShiftTime = Nothing
        Me.cboMondayStart.Size = New System.Drawing.Size(80, 22)
        Me.cboMondayStart.TabIndex = 1
        '
        'EditWorkDayDefinitions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblWorkdayEnd)
        Me.Controls.Add(Me.lblWorkdayStart)
        Me.Controls.Add(Me.lblRequiredHours)
        Me.Controls.Add(Me.txtSundayRequiredHours)
        Me.Controls.Add(Me.txtSaturdayRequiredHours)
        Me.Controls.Add(Me.txtFridayRequiredHours)
        Me.Controls.Add(Me.txtThursdayRequiredHours)
        Me.Controls.Add(Me.txtWednesdayRequiredHours)
        Me.Controls.Add(Me.txtTuesdayRequiredHours)
        Me.Controls.Add(Me.txtMondayRequiredHours)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.chkSundayActive)
        Me.Controls.Add(Me.chkSaturdayActive)
        Me.Controls.Add(Me.chkFridayActive)
        Me.Controls.Add(Me.chkThursdayActive)
        Me.Controls.Add(Me.chkWednesdayActive)
        Me.Controls.Add(Me.chkTuesdayActive)
        Me.Controls.Add(Me.chkMondayActive)
        Me.Controls.Add(Me.cboSundayEnd)
        Me.Controls.Add(Me.lblSundayDash)
        Me.Controls.Add(Me.cboSundayStart)
        Me.Controls.Add(Me.cboSaturdayEnd)
        Me.Controls.Add(Me.lblSaturdayDash)
        Me.Controls.Add(Me.cboSaturdayStart)
        Me.Controls.Add(Me.cboFridayEnd)
        Me.Controls.Add(Me.lblFridayDash)
        Me.Controls.Add(Me.cboFridayStart)
        Me.Controls.Add(Me.cboThursdayEnd)
        Me.Controls.Add(Me.lblThursdayDash)
        Me.Controls.Add(Me.cboThursdayStart)
        Me.Controls.Add(Me.cboWednesdayEnd)
        Me.Controls.Add(Me.lblWednesdayDash)
        Me.Controls.Add(Me.cboWednesdayStart)
        Me.Controls.Add(Me.cboTuesdayEnd)
        Me.Controls.Add(Me.lbTuesdayDash)
        Me.Controls.Add(Me.cboTuesdayStart)
        Me.Controls.Add(Me.cboMondayEnd)
        Me.Controls.Add(Me.lblMondayDash)
        Me.Controls.Add(Me.cboMondayStart)
        Me.Controls.Add(Me.lblSunday)
        Me.Controls.Add(Me.lblSaturday)
        Me.Controls.Add(Me.lblFriday)
        Me.Controls.Add(Me.lblThursday)
        Me.Controls.Add(Me.lblWednesday)
        Me.Controls.Add(Me.lblTuesday)
        Me.Controls.Add(Me.lblMonday)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "EditWorkDayDefinitions"
        Me.Size = New System.Drawing.Size(535, 334)
        CType(Me.WorkdayIntervalErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents chkSundayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkSaturdayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkFridayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkThursdayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkWednesdayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkTuesdayActive As System.Windows.Forms.CheckBox
    Friend WithEvents chkMondayActive As System.Windows.Forms.CheckBox
    Friend WithEvents cboSundayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblSundayDash As System.Windows.Forms.Label
    Friend WithEvents cboSundayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboSaturdayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblSaturdayDash As System.Windows.Forms.Label
    Friend WithEvents cboSaturdayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboFridayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblFridayDash As System.Windows.Forms.Label
    Friend WithEvents cboFridayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboThursdayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblThursdayDash As System.Windows.Forms.Label
    Friend WithEvents cboThursdayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboWednesdayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblWednesdayDash As System.Windows.Forms.Label
    Friend WithEvents cboWednesdayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboTuesdayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lbTuesdayDash As System.Windows.Forms.Label
    Friend WithEvents cboTuesdayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents cboMondayEnd As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblMondayDash As System.Windows.Forms.Label
    Friend WithEvents cboMondayStart As ShiftRequest.ShiftTimeComboBox
    Friend WithEvents lblSunday As System.Windows.Forms.Label
    Friend WithEvents lblSaturday As System.Windows.Forms.Label
    Friend WithEvents lblFriday As System.Windows.Forms.Label
    Friend WithEvents lblThursday As System.Windows.Forms.Label
    Friend WithEvents lblWednesday As System.Windows.Forms.Label
    Friend WithEvents lblTuesday As System.Windows.Forms.Label
    Friend WithEvents lblMonday As System.Windows.Forms.Label
    Friend WithEvents WorkdayIntervalErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblWorkdayEnd As System.Windows.Forms.Label
    Friend WithEvents lblWorkdayStart As System.Windows.Forms.Label
    Friend WithEvents lblRequiredHours As System.Windows.Forms.Label
    Friend WithEvents txtSundayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSaturdayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtFridayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtThursdayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtWednesdayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtTuesdayRequiredHours As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtMondayRequiredHours As System.Windows.Forms.MaskedTextBox

End Class
