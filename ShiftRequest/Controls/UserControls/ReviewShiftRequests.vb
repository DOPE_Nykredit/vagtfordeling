﻿Public Class ReviewShiftRequests

    Private DisplayedAgent As Agent

    Private Sub ReviewShiftRequests_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.UpdateRequestData()

        If System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Runtime Then
            Dim selectionType = My.Application.SelectedCampaign.SelectionType

            Me.lstShiftRequests.Columns.Add(New ColumnHeader() With {.Text = "Dag",
                                                                     .Width = 123})
            Me.lstShiftRequests.Columns.Add(New ColumnHeader() With {.Text = IIf(selectionType = Campaign.ShiftSelectionType.StandardShifts, "Standardvagt", "1. Prioritet"),
                                                                     .Width = 120,
                                                                     .TextAlign = HorizontalAlignment.Center})
            Me.lstShiftRequests.Columns.Add(New ColumnHeader() With {.Text = IIf(selectionType = Campaign.ShiftSelectionType.StandardShifts, "Fleksibilitet", "Rådighedsrum"),
                                                                     .Width = 120,
                                                                     .TextAlign = HorizontalAlignment.Center})
        End If
    End Sub
    Public Sub UpdateRequestData()
        Me.lstAgents.FilterAndDisplayAgents()
    End Sub

    Private Sub rdoAgents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAllAgents.CheckedChanged,
                                                                                                             rdoAgentsWithRequests.CheckedChanged,
                                                                                                             rdoAgentWithoutRequests.CheckedChanged
        If Me.rdoAllAgents.Checked Then lstAgents.Filter = AgentListView.Filters.All
        If Me.rdoAgentsWithRequests.Checked Then lstAgents.Filter = AgentListView.Filters.HasRequested
        If Me.rdoAgentWithoutRequests.Checked Then lstAgents.Filter = AgentListView.Filters.NoRequests
    End Sub
    Private Sub chkGroupByTeams_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupByTeams.CheckedChanged
        Me.lstAgents.GroupByTeam = (Me.chkGroupByTeams.Checked)
    End Sub
    Private Sub lstAgents_AgentSelected(ByVal sender As Object, ByVal selectedAgent As Agent) Handles lstAgents.AgentSelected
        Me.lstShiftRequests.Items.Clear()
        Me.lstAssignedShifts.Items.Clear()
        Me.AssignShift.Clear()

        If selectedAgent IsNot Nothing Then
            Me.grpReviewShiftRequestsOnAgent.Text = "Vagtønsker for " & selectedAgent.Initials & " - " & selectedAgent.FirstName & " " & selectedAgent.LastName

            Dim shiftRequests = My.Application.ShiftRequestsInSelectedCampaign.FindAll(Function(r) r.AgentId = selectedAgent.Id)
            Dim assignedShifts = My.Application.AssignedShiftsInSelectedCampaign.FindAll(Function(a) a.AgentId = selectedAgent.Id)

            shiftRequests.Sort()
            assignedShifts.Sort()

            Me.DisplayedAgent = selectedAgent

            shiftRequests.ForEach(Sub(r) Me.lstShiftRequests.Items.Add(New ShiftRequestListViewItem(r)))
            assignedShifts.ForEach(Sub(a) Me.lstAssignedShifts.Items.Add(New AssignedShiftListViewItem(a)))

            If Me.lstShiftRequests.Items.Count = 0 Then
                Me.lblRequestsSubmitted.Text = "Ønsker ikke indsendt."
                Me.cboDateSubmitted.Visible = False
            Else
                Me.lblRequestsSubmitted.Text = "Ønsker indsendt:"
                Me.cboDateSubmitted.Visible = True
                Me.cboDateSubmitted.Value = shiftRequests.First(Function(r) r.AgentId = selectedAgent.Id).Submitted
            End If
        End If


    End Sub
    Private Sub cmdUpdateAgents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdateAgents.Click
        Dim conn As New ShiftRequestConnection

        My.Application.LoadCampaignData(conn)
    End Sub

    Private Sub lstShiftRequests_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstShiftRequests.SelectedIndexChanged
        If lstShiftRequests.SelectedItems.Count > 0 Then
            Me.AssignShift.DisplayRequestedShift(CType(Me.lstShiftRequests.SelectedItems(0), ShiftRequestListViewItem).ShiftRequest)
        End If
    End Sub

    Public Sub ClearSelections()
        For Each i As ListViewItem In Me.lstAgents.SelectedItems
            i.Selected = False
        Next
    End Sub

    Private Sub UpdateLstAssignedShifts(ByVal agent As Agent)
        Me.lstAssignedShifts.Items.Clear()

        My.Application.AssignedShiftsInSelectedCampaign.ForEach(Sub(a)
                                                                    If a.AgentId = agent.Id Then
                                                                        Me.lstAssignedShifts.Items.Add(New AssignedShiftListViewItem(a))
                                                                    End If
                                                                End Sub)
    End Sub


    Private Sub lstAssignedShifts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstAssignedShifts.SelectedIndexChanged
        If Me.lstAssignedShifts.SelectedItems.Count > 0 Then
            Me.AssignShift.DisplayAssignedShift(CType(Me.lstAssignedShifts.SelectedItems(0), AssignedShiftListViewItem).Shift)
        End If

        Me.cmdDeleteAssignedShift.Enabled = (lstAssignedShifts.SelectedItems.Count > 0)
    End Sub
    Private Sub cmdDeleteAssignedShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteAssignedShift.Click
        If Me.lstAssignedShifts.SelectedItems.Count > 0 Then
            Dim conn As New ShiftRequestConnection

            CType(Me.lstAssignedShifts.SelectedItems(0), AssignedShiftListViewItem).Shift.delete(conn)
            Me.lstAssignedShifts.SelectedItems(0).Remove()
        End If
    End Sub

    Private Class AssignedShiftListViewItem
        Inherits ListViewItem

        Private _Shift As AssignedShift

        Public ReadOnly Property Shift As AssignedShift
            Get
                Return _Shift
            End Get
        End Property

        Public Sub New(ByVal shift As AssignedShift)
            _Shift = shift

            Me.Text = shift.Day.ToString

            Me.SubItems.Add(shift.ShiftSpan.StartTime.ToString)
            Me.SubItems.Add(shift.ShiftSpan.EndTime.ToString)
        End Sub
    End Class
    Private Class ShiftRequestListViewItem
        Inherits ListViewItem

        Private _ShiftRequest As RequestedShift

        Public ReadOnly Property ShiftRequest As RequestedShift
            Get
                Return _ShiftRequest
            End Get
        End Property

        Public Sub New(ByVal request As RequestedShift)
            _ShiftRequest = request

            Me.Text = ShiftDay.GetShiftDayDescription(request.Day.Day)

            If request.PrimaryRequest IsNot Nothing Then
                Me.SubItems.Add(ShiftRequest.PrimaryRequest.ToString)
            Else
                Me.SubItems.Add("-")
            End If

            If ShiftRequest.SecondaryRequest IsNot Nothing AndAlso Not ShiftRequest.FlexibilityEqualsShift Then
                Me.SubItems.Add(ShiftRequest.SecondaryRequest.ToString)
            Else
                Me.SubItems.Add("-")
            End If
        End Sub
    End Class
End Class
