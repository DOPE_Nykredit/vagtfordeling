﻿Public Class ManageStandardShifts

    Private _SelectedStandardShift As StandardShift

    Public Sub SetupControls()
        Dim workDayDefinitions = My.Application.WorkDayDefinitionsInSelectedCampaign

        For Each c In Me.grpDays.Controls
            Dim chk As CheckBox = CType(c, CheckBox)

            If workDayDefinitions.Exists(Function(w) w.ShiftDay.Day = chk.Tag) Then
                chk.Enabled = (workDayDefinitions.First(Function(w) w.ShiftDay.Day = chk.Tag).DayStart IsNot Nothing)
            End If
        Next

        Me.cboShiftStart.setEndTimeComboBox(Me.cboShiftEnd)

        Me.StandardShiftListView.UpdateStandardShifts()
    End Sub

    Private Function GetCheckedDays() As List(Of ShiftDay)
        Dim selectedDays As New List(Of ShiftDay)

        For Each c In grpDays.Controls
            If CType(c, CheckBox).Checked Then selectedDays.Add(New ShiftDay(CType(c, CheckBox).Tag))
        Next

        Return selectedDays
    End Function
    Private Sub SetCheckedDays(ByVal days As List(Of ShiftDay))
        For Each c In grpDays.Controls
            Dim chk As CheckBox = CType(c, CheckBox)
            If days.Exists(Function(d) d.Day = chk.Tag) Then chk.Checked = True Else chk.Checked = False
        Next
    End Sub

    Private Sub StandardShiftListView_StandardShiftSelected(ByVal standardShift As StandardShift) Handles StandardShiftListView.StandardShiftSelected
        _SelectedStandardShift = standardShift

        Me.cmdDelete.Enabled = True
        Me.cmdNew.Enabled = True

        Me.SetCheckedDays(standardShift.AppliedDays)
    End Sub

    Private Sub cmdNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNew.Click
        Me.cmdDelete.Enabled = False
        Me.grpDays.Enabled = True

        For Each i In Me.StandardShiftListView.SelectedItems
            CType(i, ListViewItem).Selected = False
        Next

        Me.SetCheckedDays(New List(Of ShiftDay))

        _SelectedStandardShift = Nothing
    End Sub
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim conn As New ShiftRequestConnection

        If _SelectedStandardShift Is Nothing Then
            Dim newShift As New StandardShift(New ShiftTimeSpan(CType(Me.cboShiftStart.SelectedItem, ShiftTime),
                                                                CType(Me.cboShiftEnd.SelectedItem, ShiftTime))) With {.AppliedDays = Me.GetCheckedDays}

            newShift.Save(conn)

            My.Application.StandardShiftsInSelectedCampaign.Add(newShift)
        Else
            _SelectedStandardShift.TimeSpan = New ShiftTimeSpan(Me.cboShiftStart.SelectedShiftTime,
                                                                Me.cboShiftEnd.SelectedShiftTime)

            _SelectedStandardShift.AppliedDays = Me.GetCheckedDays

            _SelectedStandardShift.Save(conn)
        End If

        conn.CloseConnection()

        Me.StandardShiftListView.UpdateStandardShifts()
    End Sub
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        _SelectedStandardShift.Delete()

        My.Application.StandardShiftsInSelectedCampaign.Remove(_SelectedStandardShift)

        Me.StandardShiftListView.UpdateStandardShifts()

        _SelectedStandardShift = Nothing

        Me.cmdNew_Click(Nothing, Nothing)
    End Sub

    Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMonday.CheckedChanged,
                                                                                                       chkTuesday.CheckedChanged,
                                                                                                       chkWednesday.CheckedChanged,
                                                                                                       chkThursday.CheckedChanged,
                                                                                                       chkFriday.CheckedChanged,
                                                                                                       chkSaturday.CheckedChanged,
                                                                                                       chkSunday.CheckedChanged
        Dim checkedDays As List(Of ShiftDay) = Me.GetCheckedDays

        If checkedDays.Count > 0 Then
            Dim minTime As ShiftTime = ShiftTime.MinTime
            Dim maxTime As ShiftTime = ShiftTime.MaxTime

            Dim workDayDefinitions = My.Application.WorkDayDefinitionsInSelectedCampaign

            For Each d In checkedDays
                Dim day = d.Day

                Dim wd As WorkDayDefinition = workDayDefinitions.Find(Function(w) w.ShiftDay.Day = day)

                If wd IsNot Nothing Then
                    If minTime < wd.DayStart Then minTime = wd.DayStart
                    If maxTime > wd.DayEnd Then maxTime = wd.DayEnd
                End If
            Next

            Me.cboShiftStart.MinTime = minTime
            Me.cboShiftStart.MaxTime = maxTime

            Me.cboShiftEnd.MinTime = minTime
            Me.cboShiftEnd.MaxTime = maxTime

            If _SelectedStandardShift IsNot Nothing Then
                Me.cboShiftStart.SelectedShiftTime = _SelectedStandardShift.TimeStart
                Me.cboShiftEnd.SelectedShiftTime = _SelectedStandardShift.TimeEnd
            End If

            If Me.cboShiftStart.SelectedIndex = -1 Then
                Me.cboShiftStart.SelectedIndex = 0
                Me.cboShiftEnd.SelectedIndex = Me.cboShiftEnd.Items.Count - 1
            End If
        ElseIf _SelectedStandardShift IsNot Nothing Then

        End If

        Me.cboShiftStart.Enabled = (checkedDays.Count > 0)
        Me.cboShiftEnd.Enabled = (checkedDays.Count > 0)

        Me.cmdSave.Enabled = (checkedDays.Count > 0)
    End Sub
End Class
