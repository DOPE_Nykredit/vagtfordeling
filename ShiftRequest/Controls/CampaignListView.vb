﻿Public Class CampaignListView
    Inherits System.Windows.Forms.ListView

    Public Sub New()
        Me.MultiSelect = False
        Me.FullRowSelect = True
        Me.View = Windows.Forms.View.Details
        Me.HeaderStyle = ColumnHeaderStyle.Nonclickable
        Me.HideSelection = False

        Me.SetupColumns()

        Me.Groups.Add("ActiveCampaigns", "Aktive kampagner")
        Me.Groups.Add("InactiveCampaigns", "Udløbne kampagner")
    End Sub

    Public Sub LoadCampaigns()
        Me.SetCampaigns(Campaign.GetAllCampaigns)
    End Sub

    Private Sub SetupColumns()
        Me.Columns.Add("Kampagne", 150)
        Me.Columns.Add("Start", 100)
        Me.Columns.Add("Slut", 100)
        Me.Columns.Add("Teams", 150)
    End Sub

    Private Sub SetCampaigns(ByVal campaigns As List(Of Campaign))
        Me.Items.Clear()

        For Each c In campaigns
            If Now.Date > c.ToDate Or Now.Date < c.FromDate Then
                Me.Items.Add(New CampaignListViewItem(c) With {.Group = Me.Groups("InactiveCampaigns")})
            Else
                Me.Items.Add(New CampaignListViewItem(c) With {.Group = Me.Groups("ActiveCampaigns")})
            End If
        Next
    End Sub

    Private Class CampaignListViewItem
        Inherits System.Windows.Forms.ListViewItem

        Private WithEvents _Campaign As Campaign

        Public ReadOnly Property Campaign As Campaign
            Get
                Return _Campaign
            End Get
        End Property

        Public Sub New(ByVal campaign As Campaign)
            _Campaign = campaign

            Me.Text = _Campaign.Name
            Me.SubItems.Add(campaign.FromDate)
            Me.SubItems.Add(campaign.ToDate)

            Dim assignedTeams As String = ""

            For Each t In campaign.AssignedTeams
                assignedTeams += t.Code & ", "
            Next

            assignedTeams = assignedTeams.Remove(assignedTeams.LastIndexOf(", "))

            Me.SubItems.Add(assignedTeams)
        End Sub

        Private Sub _Campaign_Saved() Handles _Campaign.Saved
            Me.Text = _Campaign.Name
        End Sub
    End Class
End Class
