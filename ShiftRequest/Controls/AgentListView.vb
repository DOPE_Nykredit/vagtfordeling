﻿Public Class AgentListView
    Inherits System.Windows.Forms.ListView

#Region "Enums"

    Public Enum Filters
        All
        HasRequested
        NoRequests
    End Enum

#End Region

#Region "Variables"

    Private CheckboxesLocked As Boolean
    Private _GroupByTeam As Boolean
    Private _Filter As Filters = Filters.All

    Private _FilteredItems As New List(Of AgentListViewItem)

#End Region

#Region "Properties"

    Public Property GroupByTeam As Boolean
        Get
            Return _GroupByTeam
        End Get
        Set(ByVal value As Boolean)
            If value <> _GroupByTeam Then
                _GroupByTeam = value
                Me.FilterAndDisplayAgents()
            End If
        End Set
    End Property

    Public Property Filter As Filters
        Get
            Return _Filter
        End Get
        Set(ByVal value As Filters)
            If value <> _Filter Then
                _Filter = value

                If _Filter = Filters.All Then
                    Me.CheckBoxes = True
                ElseIf _Filter = Filters.NoRequests Then
                    Me.CheckBoxes = False
                ElseIf _Filter = Filters.HasRequested Then
                    Me.CheckBoxes = False
                End If

                Me.FilterAndDisplayAgents()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedAgent As Agent
        Get
            If Me.SelectedItems.Count > 0 Then
                Return CType(Me.SelectedItems(0), AgentListViewItem).ShiftRequestDetails.Agent
            Else
                Return Nothing
            End If
        End Get
    End Property

#End Region

#Region "Events"

    Public Event AgentSelected(ByVal sender As Object, ByVal selectedAgent As Agent)

#End Region
    
    Public Sub New()
        Me.View = Windows.Forms.View.Details
        Me.CheckBoxes = True
        Me.MultiSelect = False
        Me.HeaderStyle = ColumnHeaderStyle.Nonclickable
    End Sub

    Private Sub SetupListView()
        Me.Columns.Clear()

        If Not _GroupByTeam Then
            Dim allAgentsGroup = New ListViewGroup("AllAgents", "Agenter")

            For Each i As ListViewItem In Me.Items
                i.Remove()
                i.Group = allAgentsGroup

                If i.GetType.Equals(GetType(AgentListViewItem)) Then CType(i, AgentListViewItem).ShowTeam = False
            Next

            Me.Groups.Clear()

            Me.Groups.Add(allAgentsGroup)

            For Each i In allAgentsGroup.Items
                Me.Items.Add(i)
            Next
        Else
            Dim teamGroups As New List(Of ListViewGroup)

            For Each i As ListViewItem In Me.Items
                Dim agentItem = TryCast(i, AgentListViewItem)

                If agentItem IsNot Nothing Then
                    agentItem.Remove()
                    If Not teamGroups.Exists(Function(g) g.Name = agentItem.ShiftRequestDetails.Agent.Team.Code) Then teamGroups.Add(New ListViewGroup(agentItem.ShiftRequestDetails.Agent.Team.Code,
                                                                                                                                                       agentItem.ShiftRequestDetails.Agent.Team.Name))
                    i.Group = teamGroups.First(Function(g) g.Name = agentItem.ShiftRequestDetails.Agent.Team.Code)
                    Me.Items.Add(agentItem)
                End If
            Next

            Me.Groups.Clear()

            teamGroups.OrderBy(Function(n) n.Name)
            teamGroups.ForEach(Sub(g) Me.Groups.Add(g))
        End If
    End Sub

    Public Sub FilterAndDisplayAgents()
        Me.Clear()

        Me.Columns.Add("Initialer", 70)
        Me.Columns.Add("Navn", 150)

        Me.CheckboxesLocked = False

        Dim agents = My.Application.AgentsInSelectedCampaign
        Dim shiftRequests = My.Application.ShiftRequestsInSelectedCampaign

        Dim shiftRequestDetails As New List(Of AgentShiftRequestdetails)

        agents.ForEach(Sub(a)
                           Dim allShiftRequestsByAgent = shiftRequests.FindAll(Function(r) r.AgentId = a.Id)
                           Dim dateSubmitted As Date? = Nothing

                           If allShiftRequestsByAgent.Count > 0 Then dateSubmitted = allShiftRequestsByAgent(0).Submitted

                           shiftRequestDetails.Add(New AgentShiftRequestdetails(a,
                                                                                allShiftRequestsByAgent.Count,
                                                                                dateSubmitted))
                       End Sub)

        If _GroupByTeam Then
            Dim teams As New List(Of Team)

            Me.Columns(1).Width = 180

            agents.ForEach(Sub(a) If Not teams.Contains(a.Team) Then teams.Add(a.Team))

            teams.OrderBy(Function(t) t.Name)

            teams.ForEach(Sub(t) Me.Groups.Add(t.Code, t.Name))

            For Each d In shiftRequestDetails
                If Not Me.IsAgentFiltered(d) Then
                    Dim newAgentItem As New AgentListViewItem(d) With {.Group = Me.Groups(d.Agent.Team.Code)}

                    If _Filter = Filters.All Then
                        newAgentItem.Checked = (d.ShiftRequestCount > 0)
                    ElseIf _Filter = Filters.HasRequested Then
                        newAgentItem.Checked = (d.ConfirmationSent IsNot Nothing)
                    End If

                    Me.Items.Add(newAgentItem)
                End If
            Next
        Else
            Me.Columns.Add("Team", 30)

            Me.Groups.Add("AllAgents", "Agenter")

            For Each d In shiftRequestDetails
                If Not Me.IsAgentFiltered(d) Then
                    Dim newAgentItem As New AgentListViewItem(d) With {.Group = Me.Groups("AllAgents"),
                                                                       .ShowTeam = True}

                    If _Filter = Filters.All Then
                        newAgentItem.Checked = (d.ShiftRequestCount > 0)
                    ElseIf _Filter = Filters.HasRequested Then
                        newAgentItem.Checked = (d.ConfirmationSent IsNot Nothing)
                    End If

                    Me.Items.Add(newAgentItem)
                End If
            Next
        End If

        Me.CheckboxesLocked = True
    End Sub

    Private Function IsAgentFiltered(ByVal shiftRequestDetials As AgentShiftRequestdetails) As Boolean
        Select Case _Filter
            Case Filters.All
                Return False
            Case Filters.HasRequested
                Return (shiftRequestDetials.ShiftRequestCount = 0)
            Case Filters.NoRequests
                Return (shiftRequestDetials.ShiftRequestCount > 0)
            Case Else
                Return False
        End Select
    End Function

    Private Sub ApplyFilter()
        For Each i In Me.Items
            If i.GetType.Equals(GetType(AgentListViewItem)) Then _FilteredItems.Add(i)
        Next

        Me.Clear()

        Dim newfilteredItems As New List(Of AgentListViewItem)

        Me.CheckboxesLocked = False

        For Each i In _FilteredItems
            Select Case _Filter
                Case Filters.All

                    Me.Items.Add(i)
                Case Filters.HasRequested
                    If i.ShiftRequestDetails.ShiftRequestCount > 0 Then Me.Items.Add(i) Else newfilteredItems.Add(i)
                Case Filters.NoRequests
                    If i.ShiftRequestDetails.ShiftRequestCount = 0 Then Me.Items.Add(i) Else newfilteredItems.Add(i)
            End Select
        Next

        _FilteredItems = newfilteredItems

        Me.SetupListView()

        Me.CheckboxesLocked = False
    End Sub

    Private Class AgentShiftRequestdetails
        Public Property Agent As Agent
        Public Property ShiftRequestCount As Integer
        Public Property ConfirmationSent As Date?

        Public Sub New(ByVal agent As Agent, ByVal requestCount As Integer, ByVal confirmation As Date?)
            Me.Agent = agent
            Me.ShiftRequestCount = requestCount
            Me.ConfirmationSent = confirmation
        End Sub
    End Class

    Private Class AgentListViewItem
        Inherits System.Windows.Forms.ListViewItem

        Private _ShiftRequestDetails As AgentShiftRequestdetails
        Private _ShowTeam As Boolean

        Public ReadOnly Property ShiftRequestDetails As AgentShiftRequestdetails
            Get
                Return _ShiftRequestDetails
            End Get
        End Property

        Public Property ShowTeam As Boolean
            Get
                Return _ShowTeam
            End Get
            Set(ByVal value As Boolean)
                If value <> _ShowTeam Then
                    _ShowTeam = value

                    If _ShowTeam Then
                        Me.SubItems.Add(New ListViewSubItem() With {.Name = "Team",
                                                                    .Text = _ShiftRequestDetails.Agent.Team.Code})
                    Else
                        Me.SubItems.RemoveByKey("Team")
                    End If
                End If
            End Set
        End Property

        Public Sub New(ByVal shiftRequestsDetails As AgentShiftRequestdetails)
            If shiftRequestsDetails Is Nothing Then Throw New ArgumentNullException("Agent cannot me Nothing")

            _ShiftRequestDetails = shiftRequestsDetails

            Me.Text = _ShiftRequestDetails.Agent.Initials
            Me.SubItems.Add(_ShiftRequestDetails.Agent.FirstName & " " & _ShiftRequestDetails.Agent.LastName)

            If ShiftRequestDetails.ShiftRequestCount > 0 Then Me.Checked = True
        End Sub

    End Class

    Private Sub AgentListView_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles Me.ItemCheck
        If Me.CheckboxesLocked Then e.NewValue = e.CurrentValue
    End Sub

    Private Sub AgentListView_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles Me.ItemSelectionChanged
        If Me.SelectedItems.Count > 0 AndAlso Me.SelectedItems(0).GetType.Equals(GetType(AgentListViewItem)) Then
            RaiseEvent AgentSelected(Me, CType(Me.SelectedItems(0), AgentListViewItem).ShiftRequestDetails.Agent)
        Else
            RaiseEvent AgentSelected(Me, Nothing)
        End If

    End Sub
End Class
