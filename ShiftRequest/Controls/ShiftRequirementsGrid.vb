﻿Public Class ShiftRequirementsGrid
    Inherits ShiftTimeDataGrid

    Public Enum AutoFills
        None
        Up
        Down
    End Enum

    Public Property AutoFill As AutoFills

    Public Sub New()
        MyBase.new()
    End Sub

    Public Sub LoadShiftRequirements()
        Me.ResetTableDefinition()

        Dim workDayDefinitions As New List(Of WorkDayDefinition)(My.Application.WorkDayDefinitionsInSelectedCampaign)

        workDayDefinitions.OrderBy(Function(d) d.ShiftDay)

        For Each d In workDayDefinitions
            For Each r In d.ShiftCountRequirements
                MyBase.GetCellByDayAndTimeSpan(d.ShiftDay, New ShiftTimeSpan(r.IntervalStart, r.IntervalEnd)).Value = r.RequiredAgents
            Next
        Next
    End Sub

    Private Function ValidateShiftRequirements() As Boolean
        Dim validated As Boolean = True

        For Each r As DataGridViewRow In Me.Rows
            For Each c As DataGridViewCell In r.Cells
                If Not c.ReadOnly AndAlso Not IsNumeric(c.Value) Then
                    validated = False
                End If
            Next
        Next

        Return validated
    End Function

    Private Sub ShiftRequirementsGrid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Me.CellValueChanged
        If e.RowIndex = -1 Or e.ColumnIndex = -1 Then Exit Sub

        Dim changedCell = Me.Rows(e.RowIndex).Cells(e.ColumnIndex)

        If changedCell.Value Is Nothing Then changedCell.Value = 0

        If Me.AutoFill = AutoFills.Down Then
            If changedCell.RowIndex <> Me.RowCount - 1 Then
                Dim nextCell = Me.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
                If Not nextCell.ReadOnly Then nextCell.Value = changedCell.Value
            End If
        ElseIf Me.AutoFill = AutoFills.Up Then
            If changedCell.RowIndex <> 0 Then
                Dim previousCell = Me.Rows(e.RowIndex - 1).Cells(e.ColumnIndex)
                If Not previousCell.ReadOnly Then previousCell.Value = changedCell.Value
            End If
        End If
    End Sub

    Public Sub SaveShiftRequirements()
        Dim inserts As New List(Of SqlInsert)

        If Not Me.ValidateShiftRequirements Then
            MsgBox("Alle felter skal indeholde nummeriske værdier.", MsgBoxStyle.OkOnly, "Bemærk!")
            Exit Sub
        End If

        For y = 0 To Me.ColumnCount - 1
            For i = 0 To Me.RowCount - 1
                Dim currentCell = Me.Rows(i).Cells(y)

                Dim day = CType(currentCell.OwningColumn.Tag, ShiftDay).Day
                Dim startTime = CType(currentCell.OwningRow.Tag, ShiftTimeSpan).StartTime

                If Not currentCell.ReadOnly Then
                    inserts.Add(New SqlInsert(New ShiftRequirementsTable,
                                              {New SqlNumberColumnValuePair(ShiftRequirementsTable.Columns.Day, day),
                                               New SqlDecimalColumnValuePair(ShiftRequirementsTable.Columns.IntervalStart, startTime.AsDecimal),
                                               New SqlNumberColumnValuePair(ShiftRequirementsTable.Columns.AgentsRequired, CType(currentCell.Value, Integer)),
                                               New SqlNumberColumnValuePair(ShiftRequirementsTable.Columns.CampaignId, My.Application.SelectedCampaign.Id)}))
                End If
            Next
        Next

        Dim db As New ShiftRequestConnection

        db.ExecuteSQLStatement(New SqlDelete(New ShiftRequirementsTable,
                                             {New SqlNumberColumnValuePair(ShiftRequestTable.Columns.CampaignId, My.Application.SelectedCampaign.Id)}))

        For Each i In inserts
            db.ExecuteSQLStatement(i)
        Next

        db.CloseConnection()
    End Sub
End Class