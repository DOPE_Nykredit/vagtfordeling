﻿Public Class ShiftRequestsListView
    Inherits System.Windows.Forms.ListView

    Private lockCheckboxes As Boolean

    Private _Day As ShiftDay
    Private _Interval As ShiftTimeSpan

    Public ReadOnly Property SelectedRequestedShift As RequestedShift
        Get
            If Me.SelectedIndices.Count > 0 AndAlso TypeOf Me.SelectedItems(0) Is ShiftRequestListViewItem Then
                Return CType(Me.SelectedItems(0), ShiftRequestListViewItem).RequestedShift
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Event RequestedShiftSelected(ByVal requestedShift As RequestedShift)

    Public Sub New()
        Me.CheckBoxes = True
        Me.HeaderStyle = ColumnHeaderStyle.Nonclickable
        Me.FullRowSelect = True
        Me.HideSelection = False
        Me.MultiSelect = False
        Me.ShowGroups = False
        Me.Sorting = SortOrder.Ascending
        Me.View = Windows.Forms.View.Details

        Me.Columns.Add("Initialer", 120, HorizontalAlignment.Left)

        If System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Runtime Then
            If My.Application.SelectedCampaign.SelectionType = Campaign.ShiftSelectionType.StandardShifts Then
                Me.Columns.Add("Standardvagt", 109, HorizontalAlignment.Center)
                Me.Columns.Add("Fleksibilitet", 109, HorizontalAlignment.Center)
            Else
                Me.Columns.Add("1. Prioritet", 109, HorizontalAlignment.Center)
                Me.Columns.Add("Rådighedsrum", 109, HorizontalAlignment.Center)
            End If
        End If

        Me.Width = 345
    End Sub

    Public Sub ClearSelection()
        For Each i As ListViewItem In Me.Items
            i.Selected = False
        Next
    End Sub

    Public Sub ShowRequestedShifts(ByVal day As ShiftDay, ByVal interval As ShiftTimeSpan)
        If day Is Nothing Then Throw New ArgumentNullException("Day cannot be nothing.")
        If interval Is Nothing Then Throw New ArgumentNullException("Interval cannot be nothing.")

        _Day = day
        _Interval = interval

        Me.UpdateList()
    End Sub
    Public Sub UpdateList()
        Me.lockCheckboxes = False

        Me.BeginUpdate()

        Me.Items.Clear()

        Dim requestedShiftsInPeriod = My.Application.ShiftRequestsInSelectedCampaign.FindAll(Function(r) r.Day.Equals(_Day) And r.MaxTimeSpan.IsInInterval(_Interval))

        requestedShiftsInPeriod.ForEach(Sub(r) Me.Items.Add(New ShiftRequestListViewItem(r)))

        Me.EndUpdate()

        Me.lockCheckboxes = True
    End Sub
    Public Sub UpdateItems()
        Me.lockCheckboxes = False

        For Each i As ShiftRequestListViewItem In Me.Items
            i.Update()
        Next

        Me.lockCheckboxes = True
    End Sub
    Private Sub ShiftRequestsListView_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles Me.ItemCheck
        If Me.lockCheckboxes Then
            e.NewValue = e.CurrentValue
        End If
    End Sub
    Private Sub ShiftRequestsListView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        If Me.SelectedIndices.Count > 0 AndAlso TypeOf Me.SelectedItems(0) Is ShiftRequestListViewItem Then
            RaiseEvent RequestedShiftSelected(CType(Me.SelectedItems(0), ShiftRequestListViewItem).RequestedShift)
        End If
    End Sub

    Private Class ShiftRequestListViewItem
        Inherits System.Windows.Forms.ListViewItem

        Private _RequestedShift As RequestedShift

        Public ReadOnly Property RequestedShift As RequestedShift
            Get
                Return _RequestedShift
            End Get
        End Property

        Public Sub New(ByVal requestedShift As RequestedShift)
            _RequestedShift = requestedShift

            Dim initials = My.Application.AgentsInSelectedCampaign.Find(Function(a) a.Id = requestedShift.AgentId).Initials

            Dim assignedShifts = My.Application.AssignedShiftsInSelectedCampaign.FindAll(Function(a) a.AgentId = requestedShift.AgentId)

            Dim assignedCount = assignedShifts.Count
            Dim assignedHours = New ShiftTime(assignedShifts.Sum(Function(a) a.ShiftSpan.Duration.AsDecimal), False)

            Me.Text = initials + " (" + assignedHours.ToString + "/" + assignedCount.ToString + ")"

            If requestedShift.PrimaryRequest Is Nothing Then
                Me.SubItems.Add("-")
            Else
                Me.SubItems.Add(requestedShift.PrimaryRequest.ToString)
            End If

            If requestedShift.SecondaryRequest Is Nothing Then
                Me.SubItems.Add("-")
            Else
                Me.SubItems.Add(requestedShift.SecondaryRequest.ToString)
            End If

            Me.Checked = My.Application.AssignedShiftsInSelectedCampaign.Exists(Function(a) a.AgentId = requestedShift.AgentId And a.Day.Equals(requestedShift.Day))
        End Sub

        Public Sub Update()
            Dim initials = My.Application.AgentsInSelectedCampaign.Find(Function(a) a.Id = RequestedShift.AgentId).Initials

            Dim assignedShifts = My.Application.AssignedShiftsInSelectedCampaign.FindAll(Function(a) a.AgentId = RequestedShift.AgentId)

            Dim assignedCount = assignedShifts.Count
            Dim assignedHours = New ShiftTime(assignedShifts.Sum(Function(a) a.ShiftSpan.Duration.AsDecimal), False)

            Me.Text = initials + " (" + assignedHours.ToString + "/" + assignedCount.ToString + ")"

            If RequestedShift.PrimaryRequest Is Nothing Then
                Me.SubItems(1).Text = "-"
            Else
                Me.SubItems(1).Text = _RequestedShift.PrimaryRequest.ToString
            End If

            If RequestedShift.SecondaryRequest Is Nothing Then
                Me.SubItems(2).Text = "-"
            Else
                Me.SubItems(2).Text = _RequestedShift.SecondaryRequest.ToString
            End If

            Me.Checked = My.Application.AssignedShiftsInSelectedCampaign.Exists(Function(a) a.AgentId = RequestedShift.AgentId And a.Day.Equals(RequestedShift.Day))
        End Sub
    End Class
End Class