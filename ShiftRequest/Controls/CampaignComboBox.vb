﻿Public Class CampaignComboBox
    Inherits System.Windows.Forms.ComboBox

    Public ReadOnly Property SelectedCampaign As Campaign
        Get
            Return CType(Me.SelectedItem, Campaign)
        End Get
    End Property

    Public Sub New()
        Me.DropDownStyle = ComboBoxStyle.DropDownList

        Me.ValueMember = "Id"
        Me.DisplayMember = "Name"
    End Sub

    Public Sub LoadCampaigns()
        Dim campaigns As List(Of Campaign) = Campaign.GetAllCampaigns

        Me.SetCampaigns(campaigns)
    End Sub

    Private Sub SetCampaigns(ByVal campaigns As List(Of Campaign))
        For Each c In campaigns
            Me.Items.Add(c)
        Next

        If Me.Items.Count > 0 Then Me.SelectedIndex = 0
    End Sub

    Public Sub AddCampaign(ByVal campaign As Campaign, Optional ByVal setAsSelected As Boolean = False)
        Me.Items.Insert(0, campaign)
        Me.SelectedIndex = 0
    End Sub

End Class
