﻿Public Class ShiftManagementGrid
    Inherits ShiftTimeDataGrid

    Private _ShowTotalTimeOnDay As Boolean = True

    Public Event IntervalSelected(ByVal day As ShiftDay, ByVal interval As ShiftTimeSpan, ByVal requiredAgents As Integer)

    Public Property ShowTotalTimeOnDay As Boolean
        Get
            Return _ShowTotalTimeOnDay
        End Get
        Set(ByVal value As Boolean)
            If value <> _ShowTotalTimeOnDay Then
                _ShowTotalTimeOnDay = value

                Me.Rows(0).Visible = value
            End If
        End Set
    End Property

    Public Sub New()
        MyBase.new()

        MyBase.SplitDays = True

        If System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Runtime Then
            Me.ReadOnly = True
            MyBase.ResetTableDefinition()

            If Me.Rows.Count > 0 Then
                Me.Rows.Insert(0, 1)
                Dim totalTimeRow = Me.Rows(0)

                totalTimeRow.Frozen = True
                totalTimeRow.HeaderCell.Value = "Total"
                totalTimeRow.Tag = "TotalTime"
                totalTimeRow.Visible = _ShowTotalTimeOnDay
            End If
        End If
    End Sub

    Public Sub RefreshShiftCoverage()
        Me.ResetCellValues()
        Me.SetCellValues()
        Me.updateTotalTimeOnDay()
    End Sub

    Private Sub ResetCellValues()
        For Each r As DataGridViewRow In Me.Rows
            For Each c As DataGridViewCell In r.Cells
                If c.Style.BackColor = Color.Gray Then c.Value = ""
            Next
        Next
    End Sub
    Private Sub SetCellValues()
        Dim assignedShifts = My.Application.AssignedShiftsInSelectedCampaign
        Dim workDayDefinitions = My.Application.WorkdayDefinitionsAll

        For Each r As DataGridViewRow In Me.Rows
            For Each c As DataGridViewTextBoxCell In r.Cells
                Dim cell = TryCast(c, DataGridViewTextBoxCell)

                If cell IsNot Nothing Then
                    If TypeOf cell.OwningRow.Tag Is ShiftTimeSpan And cell.MaxInputLength > 0 Then
                        Dim requiredAgents As Integer = 0
                        Dim assignedAgents As Integer = 0

                        Dim wd = workDayDefinitions.Find(Function(d) d.ShiftDay.Equals(cell.OwningColumn.Tag))

                        If wd.ShiftCountRequirements.Exists(Function(re) re.IntervalSpan.Equals(cell.OwningRow.Tag)) Then
                            requiredAgents = wd.ShiftCountRequirements.Find(Function(re) re.IntervalSpan.Equals(cell.OwningRow.Tag)).RequiredAgents()
                        End If

                        If TypeOf cell.OwningRow.Tag Is ShiftTimeSpan Then
                            Dim currentCellTimeSpan = CType(cell.OwningRow.Tag, ShiftTimeSpan)

                            assignedAgents = assignedShifts.LongCount(Function(a) a.Day.Equals(cell.OwningColumn.Tag) And a.ShiftSpan.IsInInterval(currentCellTimeSpan))

                            cell.Value = assignedAgents

                            cell.ToolTipText = "Krav for interval: " & requiredAgents & vbLf &
                                               "Tildelte vagter i interval: " & assignedAgents
                        End If

                        If assignedAgents < requiredAgents Then
                            Dim colValue As Integer = 0

                            If assignedAgents = 0 Then
                                colValue = 0
                            ElseIf requiredAgents > 0 And assignedAgents > 0 Then
                                colValue = 255 / requiredAgents * assignedAgents
                            End If

                            cell.Style.BackColor = Color.FromArgb(255, colValue, colValue)
                        ElseIf assignedAgents = requiredAgents Then
                            cell.Style.BackColor = Color.White
                        ElseIf assignedAgents > requiredAgents Then
                            cell.Style.BackColor = Color.LightGreen
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Private Sub updateTotalTimeOnDay()
        If Me.Rows.Count > 0 Then
            Dim workDayDefinitions = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign
            Dim assignedShifts = My.Application.AssignedShiftsInSelectedCampaign

            For Each c As DataGridViewCell In Me.Rows(0).Cells
                Dim day As ShiftDay = c.OwningColumn.Tag

                If workDayDefinitions.Find(Function(w) w.ShiftDay.Day = day.Day).DaySpan IsNot Nothing Then
                    Dim totalTime As Decimal = 0

                    assignedShifts.FindAll(Function(a) a.Day.Day = day.Day).ForEach(Sub(a) totalTime += a.ShiftSpan.Duration.AsDecimal)

                    Dim totalShiftTime As New ShiftTime(totalTime, False)

                    c.Value = totalShiftTime.ToString

                    If day.Day = ShiftDay.Days.Sunday Then day.Day = ShiftDay.Days.SundayEven

                    Dim requiredTime = workDayDefinitions.Find(Function(w) w.ShiftDay.Day = day.Day).RequiredHours

                    If requiredTime > 0 Then
                        If requiredTime > totalTime Then
                            c.Style.BackColor = Color.Red
                        ElseIf requiredTime = totalTime Then
                            c.Style.BackColor = Color.White
                        Else
                            c.Style.BackColor = Color.Green
                        End If
                    Else
                        c.Style.BackColor = Color.LightGray
                    End If
                Else
                    c.Style.BackColor = Color.Gray
                End If
            Next
        End If
    End Sub

    Private Sub ShiftManagementGrid_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Me.CellEnter
        If Me.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.BackColor <> Color.Gray And e.RowIndex > 0 Then
            Dim interval As ShiftTimeSpan = Me.Rows(e.RowIndex).Tag
            Dim day As ShiftDay = Me.Columns(e.ColumnIndex).Tag
            Dim requiredAgentsCount As Integer

            Dim wd = My.Application.WorkDayDefinitionsWithSplitInSelectedCampaign.Find(Function(d) d.ShiftDay.Equals(Me.Columns(e.ColumnIndex).Tag))

            If wd IsNot Nothing Then
                Dim requiredAgents = wd.ShiftCountRequirements.Find(Function(re) re.IntervalSpan.Equals(Me.Rows(e.RowIndex).Tag))

                If requiredAgents IsNot Nothing Then
                    requiredAgentsCount = requiredAgents.RequiredAgents
                End If
            End If

            RaiseEvent IntervalSelected(day, interval, requiredAgentsCount)
        End If
    End Sub
End Class
